﻿using Jasamarga.DA.UserManagement.Databases.Identity;
using Jasamarga.DA.UserManagement.Databases.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "{0} Tidak boleh kosong")]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "{0} Tidak boleh kosong")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
        public string DeviceId { get; set; }
        //public string DeviceType { get; set; }
    }
    public class ResetPasswordViewModel
    {
        public string UserName { get; set; }
        [Required(ErrorMessage = "{0} Tidak boleh kosong")]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }
        [Required(ErrorMessage = "{0} Tidak boleh kosong")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "{0} Tidak boleh kosong")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string ConfirmNewPassword { get; set; }
    }
    public class RegisterViewModel
    {
        public string Id { get; set; }
        //public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        //public long? PersonNumber { get; set; }
        //public string PersonName { get; set; }
        //public string EmployeeNumber { get; set; }
        public double? RecentPositionId { get; set; }
        //public string PositionName { get; set; }
        //public long? UnitKerjaId { get; set; }
        //public string UnitKerja { get; set; }
        //public string UrlImage { get; set; }
        //public string ImageFileName { get; set; }
        public bool isActive { get; set; }
        public bool IsUnitKearsipan { get; set; }
        public string RoleId { get; set; }
        //public string RoleName { get; set; }

        public IFormFile ImageAttachment { get; set; }
    }
    public class RegisterViewModelV2
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string PersonName { get; set; }
        public string EmployeeNumber { get; set; }
        public string Golongan { get; set; }
        public double? RecentPositionId { get; set; }
        public string PositionName { get; set; }
        public double? OrganizationId { get; set; }
        public string OrgName { get; set; }
        public double? OrgIdParent { get; set; }
        public long? UnitKerjaId { get; set; }
        public string UnitKerja { get; set; }
        public bool isActive { get; set; }
        public bool IsUnitKearsipan { get; set; }
        public string RoleId { get; set; }

        public IFormFile ImageAttachment { get; set; }
    }
    public class PositionViewModels
    {
        public double? RecentPositionId { get; set; }
        public string PositionName { get; set; }
    }
    public class RoleViewModel : BaseViewModels
    {
        public RoleViewModel()
        {
            SetSearchField();
        }
        [Required]
        public string Name { get; set; }
        public override void SetSearchField()
        {
            var searchField = new List<string>() { "Name" };

            this.SearchField = searchField;

        }
        public virtual ICollection<ApplicationRoleActionAuthorizations> ApplicationRoleActionAuthorizations { get; set; }
    }
    public class AdminViewModel
    {
        public AdminViewModel()
        {
            SetSearchField();
        }
        public string Id { get; set; }
        public string PersonName { get; set; }
        public string Email { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public bool isActive { get; set; }
        public string Status
        {
            get
            {
                if (isActive == true)
                {
                    return "Aktif";
                }
                else
                {
                    return "Inaktif";
                }
            }
        }
        public IEnumerable<ApplicationRole> Roles { get; set; }
        public IEnumerable<ApplicationRole> RolesOpt { get; set; }
        public virtual ICollection<ApplicationUserRole> UserRoles { get; set; }
        public DateTime CreatedDate { set; get; }
        public DateTime? UpdatedDate { set; get; }

        [JsonIgnore]
        protected List<string> SearchField { set; get; }

        public List<string> GetSearchField()
        {
            return this.SearchField;
        }
        public virtual void SetSearchField()
        {
            var searchField = new List<string>() { "PersonName", "Email", "RoleName" };

            this.SearchField = searchField;
        }
        public List<string> GetProperties()
        {
            var properties = new List<string>();
            var props = this.GetType().GetProperties();

            foreach (var prop in props)
            {
                properties.Add(prop.Name);
            }
            return properties;
        }
    }
    public class UserViewModel
    {
        public UserViewModel()
        {
            SetSearchField();
        }
        public string Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public long? PersonNumber { get; set; }
        public string PersonName { get; set; }
        public string EmployeeNumber { get; set; }
        public DateTime? OriginalDateOfHire { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string TownOfBirth { get; set; }
        public string NationalIdentifier { get; set; }
        public string Sex { get; set; }
        public double? JobId { get; set; }
        public string JobName { get; set; }
        public string JobType { get; set; }
        public double? RecentPositionId { get; set; }
        public string PositionName { get; set; }
        public string PositionNameAlias { get; set; }
        public string PositionNameAtasan { get; set; }
        public string NomorSk { get; set; }
        public string TanggalSk { get; set; }
        public string TanggalEfektifSk { get; set; }
        public double? OrganizationId { get; set; }
        public string OrgName { get; set; }
        public double? OrgIdParent { get; set; }
        public string OrgParentName { get; set; }
        public double? OrgGrandParent { get; set; }
        public string OrgGrandParentName { get; set; }
        public string UnitKerja { get; set; }
        public double? LocationId { get; set; }
        public string LocationName { get; set; }
        public string Golongan { get; set; }
        public string Baris { get; set; }
        public string Ruang { get; set; }
        public string Npwp { get; set; }
        public string JenisNpwp { get; set; }
        public string StatusNpwp { get; set; }
        public string MaritalStatus { get; set; }
        public string Religion { get; set; }
        public string Password { get; set; }
        public double? Isldap { get; set; }
        public string StatusSat { get; set; }
        public string StatusSap { get; set; }
        public double? MainRole { get; set; }
        public long? UnitKerjaId { get; set; }
        public string PayScaleArea { get; set; }
        public string UrlImage { get; set; }
        public string UrlImageResize { get; set; }
        public string ImageFileName { get; set; }
        public string ImageFileNameResize { get; set; }
        public double? PersonNumberApprover { get; set; }
        public long? PersonnelNumberSap { get; set; }
        public short? EmployeeGroup { get; set; }
        public short? CntSubOrd { get; set; }
        public short? PersonalArea { get; set; }
        public int? PositionIdAtasan { get; set; }
        public int? PayrollGroupId { get; set; }
        public string EmployeeSubgroup { get; set; }
        public string BusinessArea { get; set; }
        public string PersonalSubArea { get; set; }
        public string CompanyCode { get; set; }
        public short? EmpType { get; set; }
        public string NppAbsen { get; set; }
        public string EmpStatus { get; set; }
        public string DataSource { get; set; }
        public int Level { get; set; }
        public string BODLevel { get; set; }
        public double? JmId { get; set; }
        public bool IsUnitKearsipan { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public bool isActive { get; set; }
        public bool IsGroup { get; set; }
        public string ParentUserId { get; set; }
        public GroupUnitUserViewModels GroupUnit { get; set; }
        public bool? DashboardIsUnitKearsipan { get; set; }
        public string Status
        {
            get
            {
                if (isActive == true)
                {
                    return "Aktif";
                }
                else
                {
                    return "Inaktif";
                }
            }
        }
        public NotificationsCountViewModels UnreadNotifications { get; set; }
        public List<UserRolesAdditionalViewModels> ReceiverDelegation { get; set; }
        public List<UserRolesAdditionalViewModels> ReceiverSecretary { get; set; }
        public List<string> PrivilegedMainRole { get; set; }
        //public virtual ICollection<ApplicationUserRole> UserRoles { get; set; }
        //public IEnumerable<ApplicationRole> Roles { get; set; }
        public UserRolesAdditionalViewModels ActiveAdditionalRoles { get; set; }
        public List<UserAdditionalViewModels> UserAdditionals { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { set; get; }
        public DateTime? UpdatedDate { set; get; }
        public string UpdatedBy { get; set; }
        public string Type { get; set; }
        public string DelegationId { get; set; }

        [JsonIgnore]
        protected List<string> SearchField { set; get; }

        public List<string> GetSearchField()
        {
            return this.SearchField;
        }
        public virtual void SetSearchField()
        {
            var searchField = new List<string>() { "PersonName", "Email", "UnitKerja", "PositionName", "RoleName" ,"EmployeeNumber" };

            this.SearchField = searchField;
        }
        public List<string> GetProperties()
        {
            var properties = new List<string>();
            var props = this.GetType().GetProperties();
            foreach (var prop in props)
            {
                properties.Add(prop.Name);
            }
            return properties;
        }

        public List<UserRolesAdditionalViewModels> ListRoles { get; set; }

        public IFormFile ImageAttachment { get; set; }
    }
    public class TokenResponse
    {
        public int StatusCode { set; get; }
        public string Message { set; get; }
        public string Token { set; get; }
        public string Type { set; get; }
        public string UserId { get; set; }
        public string RefreshToken { get; set; }
    }

    public class UnitViewModels
    {
        public long Id { set; get; }
        public string NamaUnit { set; get; }
    }

    public class UploadImageViewModels
    {
        public IFormFile ImageAttachment { get; set; }
    }

    public class UpdateUserViewModel
    {
        public string Id { get; set; }
        public bool isActive { get; set; }
        public bool IsUnitKearsipan { get; set; }
        public string RoleId { get; set; }

        public IFormFile ImageAttachment { get; set; }
    }
    public class UpdateUserViewModelV2
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string PersonName { get; set; }
        public string Email { get; set; }
        public string EmployeeNumber { get; set; }
        public string Golongan { get; set; }
        public double? RecentPositionId { get; set; }
        public string PositionName { get; set; }
        public double? OrganizationId { get; set; }
        public string OrgName { get; set; }
        public double? OrgIdParent { get; set; }
        public long? UnitKerjaId { get; set; }
        public string UnitKerja { get; set; }
        public bool isActive { get; set; }
        public bool IsUnitKearsipan { get; set; }
        public string RoleId { get; set; }
        public IFormFile ImageAttachment { get; set; }
    }

    public class UsersViewModel
    {
        public UsersViewModel()
        {
            SetSearchField();
        }
        public string Id { get; set; }
        public string PersonId { get; set; }
        public string UserId { get; set; }
        public string UserPositionId { get; set; }
        public string UserPosition { get; set; }
        public string Email { get; set; }
        //public string UserName { get; set; }
        //public long? PersonNumber { get; set; }
        public string PersonName { get; set; }
        public string EmployeeNumber { get; set; }
        //public string Sex { get; set; }
        //public double? JobId { get; set; }
        //public string JobName { get; set; }
        //public string JobType { get; set; }
        public double? RecentPositionId { get; set; }
        public string PositionName { get; set; }
        //public string PositionNameAlias { get; set; }
        //public string PositionNameAtasan { get; set; }
        public double? OrganizationId { get; set; }
        //public string OrgName { get; set; }
        //public double? OrgIdParent { get; set; }
        //public string OrgParentName { get; set; }
        //public double? OrgGrandParent { get; set; }
        //public string OrgGrandParentName { get; set; }
        public string UnitKerja { get; set; }
        //public double? LocationId { get; set; }
        //public string LocationName { get; set; }
        public string Golongan { get; set; }
        public double? Isldap { get; set; }
        public long? UnitKerjaId { get; set; }
        public int Level { get; set; }
        public string BODLevel { get; set; }
        //public double? JmId { get; set; }
        public bool IsUnitKearsipan { get; set; }
        //public string RoleId { get; set; }
        //public string RoleName { get; set; }
        public bool isActive { get; set; }
        public bool IsGroup { get; set; }
        [DefaultValue(false)]
        public bool IsDelegation { get; set; }
        public string UrlImageResize { get; set; }
        public string ImageFileNameResize { get; set; }
        public GroupUnitUserViewModels GroupUnit { get; set; }
        public string Status
        {
            get
            {
                if (isActive == true)
                {
                    return "Aktif";
                }
                else
                {
                    return "Inaktif";
                }
            }
        }
        public string ParentUserId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { set; get; }
        public DateTime? UpdatedDate { set; get; }
        public string UpdatedBy { get; set; }
        public string Type { get; set; }
        public string DelegationId { get; set; }
        public List<UserAdditionalViewModels> UserAdditionals { get; set; }
        public UserRolesAdditionalViewModels ActiveAdditionalRoles { get; set; }
        public DelegationViewModel Delegation { get; set; }

        //public List<UserRolesAdditionalViewModels> DelegatedFrom { get; set; }
        [JsonIgnore]
        protected List<string> SearchField { set; get; }

        public List<string> GetSearchField()
        {
            return this.SearchField;
        }
        public virtual void SetSearchField()
        {
            var searchField = new List<string>() { "PersonName", "Email", "UnitKerja", "PositionName", "RoleName", "EmployeeNumber" };

            this.SearchField = searchField;
        }
        public List<string> GetProperties()
        {
            var properties = new List<string>();
            var props = this.GetType().GetProperties();
            foreach (var prop in props)
            {
                properties.Add(prop.Name);
            }
            return properties;
        }
    }

    public class UsersMemberViewModel
    {
        public string Id { get; set; }
        public string PersonName { get; set; }
        public List<UserMemberDetailViewModel> MemberList { get; set; }

    }

    public class UserMemberDetailViewModel
    {
        public string Id { get; set; }
        public string PersonName { get; set; }
        public string RecentPositionId { get; set; }
        public string PositionName { get; set; }
        public DelegationViewModel Delegation { get; set; }
    }

    public class ImageViewModel
    {
        public string UrlImage { get; set; }
        public string UrlImageResize { get; set; }
        public string FileName { get; set; }
        public string FileNameResize { get; set; }
    }

    public class GolonganViewModel
    {
        public string Golongan { get; set; }
    }

    public class UserAdditionalViewModels
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public double? RecentPositionId { get; set; }
        public string PositionName { get; set; }
        public string UnitKerja { get; set; }
        public long? UnitKerjaId { get; set; }
        public int Level { get; set; }
        public string BODLevel { get; set; }
        public bool IsUnitKearsipan { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public bool isActive { get; set; }
        public bool IsGroup { get; set; }
        public string Type { get; set; }
        public string DelegationType { get; set; }
        public string EmployeeNumber { get; set; }
    }
    public class UserJMViewModel
    {
        public string Id { get; set; }
        public string PersonName { get; set; }
        public string EmployeeNumber { get; set; }
        public double? RecentPositionId { get; set; }
        public long? UnitKerjaId { get; set; }
        public string UnitKerja { get; set; }
    }

    public class DelegationViewModel
    {
        public string Id { get; set; }
        public string PersonName { get; set; }
        public double? RecentPositionId { get; set; }
        public string PositionName { get; set; }
    }
}
