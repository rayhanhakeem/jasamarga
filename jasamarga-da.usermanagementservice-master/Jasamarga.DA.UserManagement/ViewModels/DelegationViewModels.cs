﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.ViewModels
{
    public class DelegationViewModels
    {
        public string Id { get; set; }
        public string DelegateUserId { get; set; }
        public string DelegateUserName { get; set; }
        public double? DelegateJabatanId { get; set; }
        public string DelegateJabatanName { get; set; }
        public string Replacement1UserId { get; set; }
        public string Replacement1UserName { get; set; }
        public double? Replacement1JabatanId { get; set; }
        public string Replacement1JabatanName { get; set; }
        public string Replacement2UserId { get; set; }
        public string Replacement2UserName { get; set; }
        public double? Replacement2JabatanId { get; set; }
        public string Replacement2JabatanName { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }
}
