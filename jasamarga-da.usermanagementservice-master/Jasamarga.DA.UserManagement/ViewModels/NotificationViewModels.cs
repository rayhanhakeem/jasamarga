﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.ViewModels
{
    public class NotificationViewModels
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public bool IsRead { get; set; }
        public bool IsActive { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string IconUrl { get; set; }
        public string DetailUrl { get; set; }
        public string Modul { get; set; }
        public string SubModul { get; set; }
        public string TabName { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
    public class NotificationsCountViewModels
    {
        public bool UnRead {get; set;}
        public int? Count { get; set; }
    }

}
