﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.ViewModels
{
    public class TriggerActivityLogViewModel
    {
        public string Type { get; set; }
        public string Message { get; set; }
        public bool IsSuccess { get; set; }
    }
}
