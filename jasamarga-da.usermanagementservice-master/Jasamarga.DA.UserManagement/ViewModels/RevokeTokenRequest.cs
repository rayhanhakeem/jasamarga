﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.ViewModels
{
    public class RevokeTokenRequest
    {
        public string Token { get; set; }
    }
}
