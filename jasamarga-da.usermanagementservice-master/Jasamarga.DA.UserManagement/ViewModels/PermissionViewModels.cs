﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.ViewModels
{
    public class PermissionViewModels : BaseViewModels
    {
        [Required]
        public string PermissionName { get; set; }
        [Required]
        public string Url { get; set; }
        public override void SetSearchField()
        {
            var searchField = new List<string>() { "PermissionName", "Url" };

            this.SearchField = searchField;

        }
    }
}
