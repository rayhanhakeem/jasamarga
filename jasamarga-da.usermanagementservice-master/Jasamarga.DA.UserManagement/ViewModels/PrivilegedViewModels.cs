﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.ViewModels
{
    public class PrivilegedViewModels
    {
       public string RoleId { get; set; }
        public string RoleName { get; set; }
        public List<string> PrivilegedList { get; set; }
    }
}
