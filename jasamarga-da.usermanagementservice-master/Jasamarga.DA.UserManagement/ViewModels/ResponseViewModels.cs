﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.ViewModels
{
    public class ResponseViewModels<T>
    {
        public int StatusCode { set; get; }
        public string Message { set; get; }
        public T Data { set; get; }
    }
}
