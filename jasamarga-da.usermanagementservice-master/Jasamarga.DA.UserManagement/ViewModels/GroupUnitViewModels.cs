﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.ViewModels
{
    public class GroupUnitViewModels
    {
        public string Id { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }
        public string MemberName { get; set; }
        public string MemberId { get; set; }
        public string MemberPosition { get; set; }
        public string WorkUnitName { get; set; }
        public string WorkUnitId { get; set; }
       
    }

    public class GroupUnitUserViewModels
    {
        public string GroupName { get; set; }
        public string Description { get; set; }
        public List<UsersViewModel> memberList { get; set; }

    }

    public class GroupUnitUserMemberViewModels
    {
        public string GroupName { get; set; }
        public string Description { get; set; }
        public List<UserMemberDetailViewModel> memberList { get; set; }

    }
    public class JobMappingViewModels
    {
        public double? OrgId { get; set; }
        public string OrgName { get; set; }
        public double? ParentOrgId { get; set; }
        public double? PositionId { get; set; }
        public string PositionName { get; set; }
    }
    public class ProcessingUnitViewModel : BaseViewModels
    {
        public string Id { get; set; }
        public string UnitName { get; set; }
        public string IdExt { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public short? Isdeleted { get; set; }
        public short? UnitType { get; set; }
    }

    public class ProcessingUnitFilter : BaseFilter
    {
        public string FirstChar { get; set; }
    }

    public class BaseFilter
    {
        public string QueryString { get; set; }
        /// <summary>
        /// Default Value = 1
        /// </summary>
        public int? Page { set; get; }
        /// <summary>
        /// Default Value = 10
        /// </summary>
        public int? PerPage { set; get; }
        public string OrderBy { set; get; }
        /// <summary>
        /// ascending or descending
        /// </summary>
        public string Order { get; set; }
        public bool? IncludeDeleted { get; set; }
    }
}
