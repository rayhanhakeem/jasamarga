﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.ViewModels
{
    public class SecretaryViewModels
    {
        public string Id { get; set; }
        public string UserSecretaryId { get; set; }
        public string UserSecretaryName { get; set; }
        public long? UserSecretaryWorkUnitId { get; set; }
        public string UserSecretaryWorkUnitName { get; set; }
        public string UserPlacemanId { get; set; }
        public string UserPlacemanName { get; set; }
        public double? UserPlacemanPositionId { get; set; }
        public string UserPlacemanPositionName { get; set; }
        public bool IsActive { get; set; }
    }
}
