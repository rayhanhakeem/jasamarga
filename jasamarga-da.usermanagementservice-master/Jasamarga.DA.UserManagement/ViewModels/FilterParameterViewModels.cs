﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.ViewModels
{
    public class FilterParameterViewModels
    {
        public int? Page { set; get; } = 1;
        public int? PerPage { set; get; } = 10;
        public string OrderBy { set; get; }
        public string Order { set; get; }
        public string SearchField { set; get; }
        public string Query { set; get; }
        public string Status { set; get; }
        public string FirstChar { set; get; }
        public DateTime? CreatedDate { set; get; }
        public DateTime? UpdatedDate { set; get; }
        public bool? IsNaskahDinas { set; get; }
        public bool? IsEdit { set; get; }
    }
}
