﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.ViewModels
{
    public class BaseViewModels
    {
        public BaseViewModels()
        {
            SetSearchField();
        }

        public string Id { set; get; }
        public DateTime CreatedDate { set; get; }
        public DateTime? UpdatedDate { set; get; }
        public bool IsDeleted { get; set; }

        [JsonIgnore]
        protected List<string> SearchField { set; get; }

        public List<string> GetSearchField()
        {
            return this.SearchField;
        }

        public virtual void SetSearchField()
        {

        }

        public List<string> GetProperties()
        {
            var properties = new List<string>();
            var props = this.GetType().GetProperties();

            foreach (var prop in props)
            {
                properties.Add(prop.Name);
            }

            return properties;
        }
    }
}
