﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.ViewModels
{
    public class UserRolesAdditionalViewModels
    {
        public string Id { get; set; }
        public string TypeId { get; set; }
        public string Type { get; set; }
        public string LoginUserPersonId { get; set; }
        public string LoginUserPersonName { get; set; }
        public string LoginUserJabatanName { get; set; }
        public string LoginUserUnitKerja { get; set; }
        public double? LoginUserUnitKerjaId { get; set; }
        public string PejabatRoleId { get; set; }
        public string PejabatRoleName { get; set; }
        public string PejabatId { get; set; }
        public string PejabatName { get; set; }
        public double? PejabatJabatanId { get; set; }
        public string PejabatJabatanName { get; set; }
        public string PejabatUnitKerjaName { get; set; }
        public string PejabatBODLEvel { get; set; }
        public bool isActive { get; set; }
        public bool SecretaryActive { get; set; }
        public bool IsUnitKearsipan { get; set; }
        public DateTime? DelegationStartDate { get; set; }
        public DateTime? DelegationEndDate { get; set; }
        public List<string> PrivilegedList { get; set; }
        public int? Order { get; set; }
        public string DelegationType { get; set; }
        public string ParentUserId { get; set; }
        public string DelegationUserId { get; set; }
    }

    public class RoleActiveViewModels
    {
        public string Id { get; set; }
        //public string LoginUserUnitKerja { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public string PejabatId { get; set; }
        public double? JabatanId { get; set; }
        public string JabatanName { get; set; }
        public bool isActive { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public List<string> PrivilegedList { get; set; }
    }

    public class RoleFromDelegationViewModels
    {
         public string Id { get; set; }
        //public string LoginUserUnitKerja { get; set; }
        public string userPejabatId { get; set; }
        public string pengganti1Id { get; set; }
        public string pengganti2Id { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
