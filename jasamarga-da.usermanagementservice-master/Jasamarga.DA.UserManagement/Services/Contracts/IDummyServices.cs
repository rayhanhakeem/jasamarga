﻿using Jasamarga.DA.UserManagement.Services.Repositories;
using Jasamarga.DA.UserManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Services.Contracts
{
    public interface IDummyServices
    {
        SeedMessage Seed(string key, string seedKey);
        ResponseViewModels<bool> UpdateUser();
        ResponseViewModels<List<ProcessingUnitViewModel>> GetAll(ProcessingUnitViewModel vm, ProcessingUnitFilter filter);
    }
}
