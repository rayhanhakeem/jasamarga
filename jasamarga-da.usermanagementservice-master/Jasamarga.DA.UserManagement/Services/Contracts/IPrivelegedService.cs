﻿using Jasamarga.DA.UserManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Services.Contracts
{
    public interface IPrivelegedService
    {
        List<string> GetPrivileged(/*string url, string id*/);
        void SavePrivileged(/*string url, string id*/);
        void DeletePrivileged(/*string url, string id*/);
        PrivilegedViewModels ListPrivileged();
        PrivilegedViewModels ListPrivilegedByRoles(string id);
        ResponsePrivileged Validation(string url);
    }
}
