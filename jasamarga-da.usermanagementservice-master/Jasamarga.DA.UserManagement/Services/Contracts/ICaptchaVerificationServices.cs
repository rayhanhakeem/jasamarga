﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Services.Contracts
{
    public interface ICaptchaVerificationServices
    {
        dynamic IsCaptchaValid(string token);
    }
}
