﻿using Jasamarga.DA.UserManagement.Databases.Models;
using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Services.Contracts
{
    public interface IRoleServices
    {
        #region Role
        ResponseViewModels<RoleViewModel> DeleteRole(string id);
        ResponseViewModels<RoleViewModel> RestoreRole(string id);
        Task<ResponseViewModels<RoleViewModel>> InsertRole(RoleViewModel model);
        ResponseViewModels<RoleViewModel> GetRoleViewModel(string id);
        ResponseViewModels<RoleViewModel> EditRole(RoleViewModel model);
        ResponseViewModels<PaginatedList<RoleViewModel>> GetAllRole(RoleViewModel vm, int? page = 1,
            int? perPage = 10, string orderBy = "CreatedAt", string order = "descending",
            string queryString = null, string includes = null, bool? isDeleted = null);
        Task<ResponseViewModels<UserViewModel>> SwitchRoles(string Id);
        TokenResponse SwitchRolesV2(string Id);
        Task<ResponseViewModels<UserRolesAdditionalViewModels>> RolesFromDelegation(string id);
        ResponseViewModels<UserRolesAdditionalViewModels> RolesFromSecretary(string Id, string userPejabatId, string secretaryId, bool isActive);
        ResponseViewModels<UserRolesAdditionalViewModels> DeleteUserRolesAdditional(string Id);
        ResponseViewModels<UserRolesAdditionalViewModels> RestoreUserRolesAdditional(string Id);
        #endregion
    }
}
