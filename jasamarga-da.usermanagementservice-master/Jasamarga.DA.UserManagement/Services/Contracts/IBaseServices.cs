﻿using Jasamarga.DA.UserManagement.Databases.Models;
using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Services.Contracts
{
    public interface IBaseServices<T, U>
         where T : BaseViewModels
         where U : BaseModel
    {
        ResponseViewModels<PaginatedList<T>> GetList(T vm, int? page = 1, int? perPage = 10, string orderBy = "CreatedDate", string order = "descending", string searchField = null, string queryString = null, bool? isDeleted = null);
        ResponseViewModels<T> Store(T vm);
        ResponseViewModels<T> Update(T vm);
        ResponseViewModels<T> Delete(string id);
        ResponseViewModels<T> Restore(string id);
        ResponseViewModels<T> Detail(string id, string includes = null);
    }
}
