﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Services.Contracts
{
    public interface ITokenServices
    {
        void SetToken(string token);
    }
}
