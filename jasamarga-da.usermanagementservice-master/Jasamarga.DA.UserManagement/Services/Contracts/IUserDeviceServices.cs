﻿using Jasamarga.DA.UserManagement.Databases.Models;
using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Services.Contracts
{
    public interface IUserDeviceServices
    {
        #region userdevice
        ResponseViewModels<List<UserDevicesViewModels>> GetListUserDevices();
        ResponseViewModels<List<UserDevicesViewModels>> GetListUserDevicesByUserId(string userId);
        ResponseViewModels<UserDevicesViewModels> GetUserDevices(string deviceId);
        #endregion
    }
}
