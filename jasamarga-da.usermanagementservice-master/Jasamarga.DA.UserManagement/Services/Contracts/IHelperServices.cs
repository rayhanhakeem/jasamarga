﻿using Jasamarga.DA.UserManagement.Databases.Identity;
using Jasamarga.DA.UserManagement.Databases.Models;
using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Services.Contracts
{
    public interface IHelperServices
    {
        void DefaultRoles(string userId);
        void DefaultRolesV2(string userId);
        ResponseViewModels<List<GolonganViewModel>> GetListGolongan();
        ImageViewModel Upload(IFormFile ImageAttachment, ApplicationUser user);
        ResponseViewModels<UserViewModel> GetUserByEmail(string email);
        UserViewModel GetPersonName(string ids);
        int GetLevelUser(string golongan);
        string GetBODLevelUser(string golongan);
        UserRolesAdditionalViewModels GetActiveAdditionalRoles(string userId);
        UserRolesAdditionalViewModels GetActiveAdditionalRolesV2(string id);
        List<string> GetListRoles(string id);
        List<UserRolesAdditionalViewModels> GetListUserRolesAdditionals(string id);
        PositionViewModels GetPosition(string name);
        int CheckCount(double? orgId);
        void InsertPosition(double? orgId, string orgName, double? parentOrgId, string positionName);
        void RolesFromUser(string Id);
        Task<UserViewModel> GetUser(string id);
        public List<UserAdditionalViewModels> GetUserAdditional(string id);
        List<UnitViewModels> GetAllUnit();
        void UpdateUserMainRole(string id, double? jabatanId, string jabatanName);
        void UpdateUserAdditional(string id, double? jabatanId, string jabatanName, string roleId, string roleName, bool isActive);
        ResponseViewModels<bool> UpdateUserAdditionaMainRole();
        ApplicationUser CreateUserDelegation(DelegationViewModels vm);
        List<UsersViewModel> GetDelegasiUser(string firstChar);
        List<UserRolesAdditionalViewModels> GetDelegatedFrom(string id);
        List<UserAdditionalViewModels> GetUserAdditionalDelegation(string id);
        List<UsersViewModel> GetDelegasiUserV2(string firstChar, List<string> ids);
    }
}
