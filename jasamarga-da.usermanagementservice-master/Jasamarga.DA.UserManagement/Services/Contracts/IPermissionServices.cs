﻿using Jasamarga.DA.UserManagement.Databases.Models;
using Jasamarga.DA.UserManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Services.Contracts
{
    public interface IPermissionServices : IBaseServices<PermissionViewModels, Permission>
    {
    }
}

