﻿using Jasamarga.DA.UserManagement.Databases.Models;
using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Services.Contracts
{
    public interface IAccountServices
    {
        #region User
        void UpdateUser(string userName);
        ResponseViewModels<UserViewModel> DeleteUser(string id);
        ResponseViewModels<UserViewModel> CreateUserGroup(string id, string userName, string personName);
        ResponseViewModels<UserViewModel> DeleteUserGroup(string id);
        ResponseViewModels<UserViewModel> RestoreUserGroup(string id);
        IEnumerable<Permission> GetActionList();
        ResponseViewModels<UserViewModel> EditUser(UpdateUserViewModel model);
        Task<ResponseViewModels<UserViewModel>> GetUser(string id);
        void GetEmployee(string personName, string userName, string email, string password);
        UserViewModel GetEmployeeByEmployeeNumber(string EmployeeNumber);
        Task<ResponseViewModels<UserViewModel>> GetUserByToken();
        Task<ResponseViewModels<PaginatedUserList<UsersViewModel>>> GetAllActiveUser(UsersViewModel vm, FilterParameterViewModels filter, bool? isActive = null, bool? includeParent = null, bool? includeDelegation = null, bool? includeGroup = null, string level = null, string unitKerjaId = null);
        Task<ResponseViewModels<PaginatedList<UsersViewModel>>> PejabatSekretaris(UsersViewModel vm, FilterParameterViewModels filter, string userId = null, string secretaryId = null);
        List<UsersViewModel> GetListUserByLevelV2(FilterParameterViewModels filter);
        List<UsersViewModel> GetUserDelegate(int user1, FilterParameterViewModels filter);
        List<UsersViewModel> GetUserDelegate2(int user1, int user2, FilterParameterViewModels filter);
        Task<ResponseViewModels<List<UsersViewModel>>> GetListUserById(List<string> id);
        Task<ResponseViewModels<List<UsersViewModel>>> GetListUserByPositionId(List<string> ids);
        Task<ResponseViewModels<List<UsersMemberViewModel>>> GetListUserMemberById(List<string> ids);
        ResponseViewModels<List<UsersViewModel>> GetListUserByUnitKerja(string unitKerja);
        ResponseViewModels<UserViewModel> UploadImageUser(IFormFile ImageAttachment);
        Task<ResponseViewModels<RegisterViewModel>> RegisterUser(RegisterViewModel model);
        Task<ResponseViewModels<RegisterViewModelV2>> RegisterUserV2(RegisterViewModelV2 vm);
        Task<ResponseViewModels<ResetPasswordViewModel>> ResetPassword(ResetPasswordViewModel model);
        ResponseViewModels<string> ForgetPassword(string userEmail);
        Task<ResponseViewModels<UserViewModel>> EditUserV2(UpdateUserViewModelV2 model);
        ResponseViewModels<List<UserJMViewModel>> GetListUserByADAccount(List<string> adAccounts);
        ResponseViewModels<List<UserJMViewModel>> GetListUserByADAccountV2();
        ResponseViewModels<UserViewModel> DeleteNonLdap();
        #endregion
    }
}
