﻿using Jasamarga.DA.UserManagement.Databases.Models;
using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Services.Contracts
{
    public interface IAuthenticateServices
    {
        #region token
        TokenResponse Authenticate(LoginViewModel model, string ipAddress);
        TokenResponse AuthenticateV2(string id);
        TokenResponse RefreshToken(string token, string ipAddress);
        bool RevokeToken(string token, string ipAddress);
        TokenResponse Revoke();

        void SaveUserDevice(string userId, string DeviceId, string DeviceType);
        #endregion
    }
}
