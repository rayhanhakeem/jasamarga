﻿using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Services.Contracts
{
    public interface IAdminServices
    {
        #region Admin
        ResponseViewModels<PaginatedList<AdminViewModel>> GetAllActiveAdmin(AdminViewModel vm, int? page = 1,
            int? perPage = 10, string orderBy = "CreatedAt", string order = "descending",
            string queryString = null, string includes = null);
        ResponseViewModels<AdminViewModel> EditAdmin(AdminViewModel model);
        ResponseViewModels<AdminViewModel> DeleteAdmin(string id);
        ResponseViewModels<AdminViewModel> GetAdmin(string id);
        #endregion
    }
}
