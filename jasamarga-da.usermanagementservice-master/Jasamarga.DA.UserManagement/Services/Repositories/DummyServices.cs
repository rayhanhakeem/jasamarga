﻿using AutoMapper;
using Jasamarga.DA.UserManagement.Databases;
using Jasamarga.DA.UserManagement.Databases.Identity;
using Jasamarga.DA.UserManagement.Databases.Models;
using Jasamarga.DA.UserManagement.Models;
using Jasamarga.DA.UserManagement.Services.Contracts;
using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using MoreLinq;
using Newtonsoft.Json;
using Npgsql;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Services.Repositories
{
    public class DummyServices : IDummyServices
    {
        protected ApplicationDbContext dbContext { get; set; }
        protected JMCoreContext jMCoreContext { get; set; }
        private RoleManager<ApplicationRole> _roleManager;
        private UserManager<ApplicationUser> _userManager;
        private readonly AppSettings _appSettings;
        private readonly ApiEndpoint _apiEndpoint;
        private IAccountServices svc;
        private IHelperServices helperSvc;
        protected IMapper mapper { set; get; }

        public DummyServices(IMapper mapper, IAccountServices svc, ApplicationDbContext dbContext, JMCoreContext jMCoreContext, IOptions<AppSettings> appSettings, IOptions<ApiEndpoint> apiEndpoint, UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager, IHelperServices helperSvc)
        {
            this.mapper = mapper;
            this.svc = svc;
            this.dbContext = dbContext;
            this._userManager = userManager;
            this._roleManager = roleManager;
            this._appSettings = appSettings.Value;
            this.helperSvc = helperSvc;
            this.jMCoreContext = jMCoreContext;
            this._apiEndpoint = apiEndpoint.Value;
        }
        public SeedMessage Seed(string key, string seedKey)
        {
            if (!key.Equals(seedKey))
            {
                return new SeedMessage
                {
                    Status = SeedStatus.ERROR.ToString().ToLower(),
                    Message = "Invalid key"
                };
            }

            try
            {
                //var rand = new Random();
                //var Status = new bool[] { true, false };
                //var permissions = new List<Permission>()
                //{
                //    new Permission { Id = Guid.NewGuid().ToString(), PermissionName = "Naskah Dinas Tugas", Url = "naskah-dinas/tugas/disposisi-masuk", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now },
                //    new Permission { Id = Guid.NewGuid().ToString(), PermissionName = "Naskah Dinas Disposisi Tugas", Url = "naskah-dinas/tugas/disposisi-keluar", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now },
                //    new Permission { Id = Guid.NewGuid().ToString(), PermissionName = "Naskah Dinas Review Tugas", Url = "naskah-dinas/tugas/review", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now },
                //    new Permission { Id = Guid.NewGuid().ToString(), PermissionName = "Naskah Dinas Kotak Masuk", Url = "naskah-dinas/tugas/kotak-masuk", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now },
                //    new Permission { Id = Guid.NewGuid().ToString(), PermissionName = "Naskah Dinas Kotak Masuk Pejabat", Url = "naskah-dinas/tugas/kotak-masuk-pejabat", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now },
                //    new Permission { Id = Guid.NewGuid().ToString(), PermissionName = "Master Data", Url = "master-data", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now },
                //    new Permission { Id = Guid.NewGuid().ToString(), PermissionName = "Pencarian", Url = "pencarian", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now },
                //    new Permission { Id = Guid.NewGuid().ToString(), PermissionName = "Arsip", Url = "arsip", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now },
                //    //new Permission { Id = Guid.NewGuid().ToString(), PermissionName = "", Url = "", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now },
                //    //new Permission { Id = Guid.NewGuid().ToString(), PermissionName = "", Url = "", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now },
                //    //new Permission { Id = Guid.NewGuid().ToString(), PermissionName = "", Url = "", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now },
                //    //new Permission { Id = Guid.NewGuid().ToString(), PermissionName = "", Url = "", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now },
                //    //new Permission { Id = Guid.NewGuid().ToString(), PermissionName = "", Url = "", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now },
                //    //new Permission { Id = Guid.NewGuid().ToString(), PermissionName = "", Url = "", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now },
                //    //new Permission { Id = Guid.NewGuid().ToString(), PermissionName = "", Url = "", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now },
                //    new Permission { Id = Guid.NewGuid().ToString(), PermissionName = "", Url = "", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now },
                //};
                //dbContext.AddRange(permissions);
                //dbContext.SaveChanges();

                //var roles = new List<ApplicationRole>()
                //{
                //    new ApplicationRole { Id = Guid.NewGuid().ToString(), Name = "inputer", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now  },
                //    new ApplicationRole { Id = Guid.NewGuid().ToString(), Name = "konseptor", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now  },
                //    new ApplicationRole { Id = Guid.NewGuid().ToString(), Name = "reviewer", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now  },
                //    new ApplicationRole { Id = Guid.NewGuid().ToString(), Name = "role 1", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now  },
                //    new ApplicationRole { Id = Guid.NewGuid().ToString(), Name = "role 2", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now  },
                //    new ApplicationRole { Id = Guid.NewGuid().ToString(), Name = "role 3", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now  },
                //    new ApplicationRole { Id = Guid.NewGuid().ToString(), Name = "role 4", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now  },
                //    new ApplicationRole { Id = Guid.NewGuid().ToString(), Name = "role 5", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now  },
                //    new ApplicationRole { Id = Guid.NewGuid().ToString(), Name = "role 6", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now  },
                //    new ApplicationRole { Id = Guid.NewGuid().ToString(), Name = "role 7", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now  },
                //    new ApplicationRole { Id = Guid.NewGuid().ToString(), Name = "role 8", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now  },
                //    new ApplicationRole { Id = Guid.NewGuid().ToString(), Name = "role 9", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now  },
                //    new ApplicationRole { Id = Guid.NewGuid().ToString(), Name = "role 10", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now  },
                //    new ApplicationRole { Id = Guid.NewGuid().ToString(), Name = "role 11", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now  },
                //    new ApplicationRole { Id = Guid.NewGuid().ToString(), Name = "role 12", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now  }
                //};


                //foreach (var item in roles)
                //{
                //    IdentityResult roleResult = _roleManager.CreateAsync(item).Result;
                //    foreach (var permission in permissions)
                //    {
                //        ApplicationRoleActionAuthorizations actionAuthorizations = new ApplicationRoleActionAuthorizations()
                //        {
                //            ApplicationRoleId = item.Id,
                //            PermissionId = permission.Id,
                //            forCreate = Status[rand.Next(0, 2)],
                //            forEdit = Status[rand.Next(0, 2)],
                //            forView = Status[rand.Next(0, 2)],
                //            forDelete = Status[rand.Next(0, 2)]
                //        };
                //        dbContext.Add(actionAuthorizations);
                //        dbContext.SaveChanges();
                //    }
                //}

                //var users = GetEmployeeNumberV3();


                //foreach (var item in users)
                //{
                //    var exist = dbContext.Users.Where(a => a.UserName == item.UserName).FirstOrDefault();
                //    if (exist == null)
                //    {
                //        CreateUserDummyV3(item.UserName, item.Password);
                //    }
                //}

                //var users = GetEmployeeNumber();


                //foreach (var item in users)
                //{
                //    var exist = dbContext.Users.Where(a => a.UserName == item.UserName).FirstOrDefault();
                //    if (exist == null)
                //    {
                //        CreateUserDummy(item.UserName, item.Password);
                //    }
                //}
                //TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                //var users = dbContext.Users.ToList();
                //foreach (var item in users)
                //{
                //    if (item.Email != null && item.PersonName != null)
                //    {
                //        string email = item.Email.ToLower();
                //        string name = ti.ToTitleCase(item.PersonName.ToLower());
                //        item.Email = email;
                //        item.PersonName = name;
                //        dbContext.Users.Update(item);
                //        dbContext.SaveChanges();
                //    }
                //}
                //dbContext.SaveChanges();
                return new SeedMessage
                {
                    Status = SeedStatus.SUCCESS.ToString().ToLower(),
                    Message = "Seed data successful"
                };
            }
            catch (Exception e)
            {
                return new SeedMessage
                {
                    Status = SeedStatus.ERROR.ToString().ToLower(),
                    Message = e.Message
                };
            }
        }

        public void CreateUserDummy(string username, string password)
        {
            ApplicationUser model = new ApplicationUser();
            string connStr = _appSettings.ServerConnString /*"Server=10.1.3.47;Port=5432;Database=jm_core_data;User Id=postgres;Password=5U1tm3di4!"*/;
            var sql = "SELECT * FROM public.jm_employee where employee_number = '" + username + "'";
            using (NpgsqlConnection con = new NpgsqlConnection(connStr))
            {
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                con.Open();
                NpgsqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                        string personName = reader["person_name"].ToString();
                        string email = Convert.ToString(reader["email_address"]);
                        model.UserName = username;
                        model.PersonNumber = Convert.ToInt64(reader["person_number"]);
                        model.Email = email.ToLower();
                        model.PersonName = ti.ToTitleCase(personName.ToLower());
                        model.EmployeeNumber = reader["employee_number"].ToString();
                        model.OriginalDateOfHire = Convert.ToDateTime(reader["original_date_of_hire"]);
                        model.ValidFrom = Convert.ToDateTime(reader["valid_from"]);
                        model.ValidTo = Convert.ToDateTime(reader["valid_to"]);
                        model.DateOfBirth = Convert.ToDateTime(reader["date_of_birth"]);
                        model.TownOfBirth = reader["town_of_birth"].ToString();
                        model.NationalIdentifier = reader["national_identifier"].ToString();
                        model.Sex = reader["sex"].ToString();
                        model.JobId = Convert.ToDouble(reader["job_id"]);
                        model.JobName = reader["job_name"].ToString();
                        model.JobType = reader["job_type"].ToString();
                        model.RecentPositionId = Convert.ToDouble(reader["recent_position_id"]);
                        model.PositionName = reader["position_name"].ToString().TrimStart();
                        model.PositionNameAlias = reader["position_name_alias"].ToString();
                        model.PositionNameAtasan = reader["position_name_atasan"].ToString();
                        model.NomorSk = reader["nomor_sk"].ToString();
                        model.TanggalSk = reader["tanggal_sk"].ToString();
                        model.TanggalEfektifSk = reader["tanggal_efektif_sk"].ToString();
                        model.OrganizationId = Convert.ToDouble(reader["organization_id"]);
                        model.OrgName = reader["org_name"].ToString();
                        model.OrgIdParent = Convert.ToDouble(reader["org_id_parent"]);
                        model.OrgParentName = reader["org_parent_name"].ToString();
                        model.OrgGrandParent = Convert.IsDBNull(reader["org_grand_parent"]) ? null : (double?)reader["org_grand_parent"];
                        model.OrgGrandParentName = reader["org_grand_parent_name"].ToString();
                        model.UnitKerja = reader["unit_kerja"].ToString();
                        model.LocationId = Convert.ToDouble(reader["location_id"]);
                        model.LocationName = reader["location_name"].ToString();
                        model.Golongan = reader["golongan"].ToString();
                        model.Baris = reader["baris"].ToString();
                        model.Ruang = reader["ruang"].ToString();
                        model.Npwp = reader["npwp"].ToString();
                        model.JenisNpwp = reader["jenis_npwp"].ToString();
                        model.StatusNpwp = reader["status_npwp"].ToString();
                        model.MaritalStatus = reader["marital_status"].ToString();
                        model.Religion = reader["religion"].ToString();
                        model.Isldap = Convert.ToDouble(reader["isldap"]);
                        model.StatusSat = reader["status_sat"].ToString();
                        model.StatusSap = reader["status_sap"].ToString();
                        model.CreatedDate = DateTime.Now;
                        model.UpdatedBy = reader["last_updated_by"].ToString();
                        model.UpdatedDate = DateTime.Now;
                        model.MainRole = Convert.ToDouble(reader["main_role"]);
                        model.UnitKerjaId = Convert.ToInt64(reader["unit_kerja_id"]);
                        model.PayScaleArea = reader["pay_scale_area"].ToString();
                        model.UrlImage = reader["url_image"].ToString();
                        model.PersonNumberApprover = Convert.IsDBNull(reader["person_number_approver"]) ? null : (double?)reader["person_number_approver"];
                        model.EmployeeGroup = Convert.ToInt16(reader["employee_group"]);
                        model.CntSubOrd = Convert.IsDBNull(reader["cnt_sub_ord"]) ? null : (Int16?)reader["cnt_sub_ord"];
                        model.PersonalArea = Convert.ToInt16(reader["personal_area"]);
                        model.PositionIdAtasan = Convert.IsDBNull(reader["position_id_atasan"]) ? null : (Int32?)reader["position_id_atasan"];
                        model.PayrollGroupId = Convert.IsDBNull(reader["payroll_group_id"]) ? null : (Int32?)reader["payroll_group_id"];
                        model.EmployeeSubgroup = reader["employee_subgroup"].ToString();
                        model.BusinessArea = reader["business_area"].ToString();
                        model.PersonalSubArea = reader["personal_sub_area"].ToString();
                        model.CompanyCode = reader["company_code"].ToString();
                        model.EmpType = Convert.ToInt16(reader["emp_type"]);
                        model.NppAbsen = reader["npp_absen"].ToString();
                        model.EmpStatus = reader["emp_status"].ToString();
                        model.DataSource = reader["data_source"].ToString();
                        model.Level = GetLevelUser(model.Golongan);
                        model.BODLevel = GetBODLevelUser(model.Golongan);
                        model.JmId = Convert.ToDouble(reader["id"]);
                        model.RoleId = "f4a3bfb3-511c-40f3-8e26-b873561d4d81";
                        model.RoleName = "Public";
                        model.IsActive = true;
                        model.Init();
                        IdentityResult result = _userManager.CreateAsync(model, password).Result;
                        IdentityResult roles = _userManager.AddToRoleAsync(model, "Public").Result;
                        helperSvc.RolesFromUser(model.Id);
                    }
                }
                catch (Exception e)
                {

                }
            }
        }

        public List<LoginViewModel> GetEmployeeNumber()
        {
            List<LoginViewModel> empNumberList = new List<LoginViewModel>();
            string connStr = _appSettings.ServerConnString /*"Server=10.1.3.47;Port=5432;Database=jm_core_data;User Id=postgres;Password=5U1tm3di4!"*/;
            var sql = "SELECT employee_number FROM public.jm_employee where recent_position_id != 99999999";
            using (NpgsqlConnection con = new NpgsqlConnection(connStr))
            {
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                con.Open();
                NpgsqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        var model = new LoginViewModel();
                        model.UserName = reader["employee_number"].ToString();
                        model.Password = _appSettings.DefaultPassword;
                        empNumberList.Add(model);
                    }
                }
                catch (Exception e)
                {

                }
            }
            return empNumberList;
        }

        public void CreateUserDummyV3(string username, string password)
        {
            ApplicationUser model = new ApplicationUser();
            string connStr = _appSettings.ServerConnString;
            var sql = "SELECT * FROM public.jm_employee where employee_number = '" + username + "'";
            using (NpgsqlConnection con = new NpgsqlConnection(connStr))
            {
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                con.Open();
                NpgsqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                        string personName = reader["person_name"].ToString();
                        string email = Convert.ToString(reader["email_address"]);
                        model.UserName = username;
                        model.PersonNumber = Convert.ToInt64(reader["person_number"]);
                        model.Email = email.ToLower();
                        model.PersonName = ti.ToTitleCase(personName.ToLower());
                        model.EmployeeNumber = reader["employee_number"].ToString();
                        //model.OriginalDateOfHire = Convert.ToDateTime(reader["original_date_of_hire"]);
                        //model.ValidFrom = Convert.ToDateTime(reader["valid_from"]);
                        //model.ValidTo = Convert.ToDateTime(reader["valid_to"]);
                        //model.DateOfBirth = Convert.ToDateTime(reader["date_of_birth"]);
                        //model.TownOfBirth = reader["town_of_birth"].ToString();
                        //model.NationalIdentifier = reader["national_identifier"].ToString();
                        //model.Sex = reader["sex"].ToString();
                        //model.JobId = Convert.ToDouble(reader["job_id"]);
                        //model.JobName = reader["job_name"].ToString();
                        //model.JobType = reader["job_type"].ToString();
                        model.RecentPositionId = Convert.ToDouble(reader["recent_position_id"]);
                        model.PositionName = reader["position_name"].ToString().TrimStart();
                        //model.PositionNameAlias = reader["position_name_alias"].ToString();
                        //model.PositionNameAtasan = reader["position_name_atasan"].ToString();
                        //model.NomorSk = reader["nomor_sk"].ToString();
                        //model.TanggalSk = reader["tanggal_sk"].ToString();
                        //model.TanggalEfektifSk = reader["tanggal_efektif_sk"].ToString();
                        //model.OrganizationId = Convert.ToDouble(reader["organization_id"]);
                        //model.OrgName = reader["org_name"].ToString();
                        //model.OrgIdParent = Convert.ToDouble(reader["org_id_parent"]);
                        //model.OrgParentName = reader["org_parent_name"].ToString();
                        //model.OrgGrandParent = Convert.IsDBNull(reader["org_grand_parent"]) ? null : (double?)reader["org_grand_parent"];
                        //model.OrgGrandParentName = reader["org_grand_parent_name"].ToString();
                        model.UnitKerja = reader["unit_kerja"].ToString();
                        //model.LocationId = Convert.ToDouble(reader["location_id"]);
                        //model.LocationName = reader["location_name"].ToString();
                        //model.Golongan = reader["golongan"].ToString();
                        //model.Baris = reader["baris"].ToString();
                        //model.Ruang = reader["ruang"].ToString();
                        //model.Npwp = reader["npwp"].ToString();
                        //model.JenisNpwp = reader["jenis_npwp"].ToString();
                        //model.StatusNpwp = reader["status_npwp"].ToString();
                        //model.MaritalStatus = reader["marital_status"].ToString();
                        //model.Religion = reader["religion"].ToString();
                        model.Isldap = Convert.ToDouble(reader["isldap"]);
                        //model.StatusSat = reader["status_sat"].ToString();
                        //model.StatusSap = reader["status_sap"].ToString();
                        model.CreatedDate = DateTime.Now;
                        model.UpdatedBy = reader["last_updated_by"].ToString();
                        model.UpdatedDate = DateTime.Now;
                        //model.MainRole = Convert.ToDouble(reader["main_role"]);
                        model.UnitKerjaId = Convert.ToInt64(reader["unit_kerja_id"]);
                        //model.PayScaleArea = reader["pay_scale_area"].ToString();
                        //model.UrlImage = reader["url_image"].ToString();
                        //model.PersonNumberApprover = Convert.IsDBNull(reader["person_number_approver"]) ? null : (double?)reader["person_number_approver"];
                        //model.EmployeeGroup = Convert.ToInt16(reader["employee_group"]);
                        //model.CntSubOrd = Convert.IsDBNull(reader["cnt_sub_ord"]) ? null : (Int16?)reader["cnt_sub_ord"];
                        //model.PersonalArea = Convert.ToInt16(reader["personal_area"]);
                        //model.PositionIdAtasan = Convert.IsDBNull(reader["position_id_atasan"]) ? null : (Int32?)reader["position_id_atasan"];
                        //model.PayrollGroupId = Convert.IsDBNull(reader["payroll_group_id"]) ? null : (Int32?)reader["payroll_group_id"];
                        //model.EmployeeSubgroup = reader["employee_subgroup"].ToString();
                        //model.BusinessArea = reader["business_area"].ToString();
                        //model.PersonalSubArea = reader["personal_sub_area"].ToString();
                        //model.CompanyCode = reader["company_code"].ToString();
                        //model.EmpType = Convert.ToInt16(reader["emp_type"]);
                        //model.NppAbsen = reader["npp_absen"].ToString();
                        //model.EmpStatus = reader["emp_status"].ToString();
                        //model.DataSource = reader["data_source"].ToString();
                        //model.Level = GetLevelUser(model.Golongan);
                        //model.BODLevel = GetBODLevelUser(model.Golongan);
                        //model.JmId = Convert.ToDouble(reader["id"]);
                        //model.RoleId = "f4a3bfb3-511c-40f3-8e26-b873561d4d81";
                        //model.RoleName = "Public";
                        model.IsActive = true;
                        model.Init();
                        IdentityResult result = _userManager.CreateAsync(model, password).Result;
                        //IdentityResult roles = _userManager.AddToRoleAsync(model, "Public").Result;
                        //helperSvc.RolesFromUser(model.Id);
                    }
                }
                catch (Exception e)
                {

                }
            }
        }
        public List<LoginViewModel> GetEmployeeNumberV3()
        {
            List<LoginViewModel> empNumberList = new List<LoginViewModel>();
            string connStr = _appSettings.ServerConnString /*"Server=10.1.3.47;Port=5432;Database=jm_core_data;User Id=postgres;Password=5U1tm3di4!"*/;
            var sql = "SELECT employee_number FROM public.jm_employee where recent_position_id = 99999999";
            using (NpgsqlConnection con = new NpgsqlConnection(connStr))
            {
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                con.Open();
                NpgsqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        var model = new LoginViewModel();
                        model.UserName = reader["employee_number"].ToString();
                        model.Password = _appSettings.InaktifDefaultPassword;
                        empNumberList.Add(model);
                    }
                }
                catch (Exception e)
                {

                }
            }
            return empNumberList;
        }

        public int GetLevelUser(string golongan)
        {
            int level = 0;
            if (golongan == "ALL" || golongan == "K")
            {
                level = 1;
            }
            //else if (golongan == "K")
            //{
            //    level = 1;
            //}
            //else if (golongan == "D")
            //{
            //    level = 2;
            //}
            //else if (golongan == "E")
            //{
            //    level = 2;
            //}
            else if (golongan == "BOD-1" || golongan == "D" || golongan == "E" || golongan == "F")
            {
                level = 2;
            }
            else if (golongan == "1" || golongan == "BOD-2")
            {
                level = 3;
            }
            else if (golongan == "2" || golongan == "BOD-3")
            {
                level = 4;
            }
            else if (golongan == "3" || golongan == "BOD-4")
            {
                level = 5;
            }
            else if (golongan == "4" || golongan == "BOD-5")
            {
                level = 6;
            }
            else if (golongan == "5" || golongan == "BOD-6")
            {
                level = 6;
            }
            else
            {
                level = 7;
            }
            return level;
        }
        public string GetBODLevelUser(string golongan)
        {
            string level = string.Empty;
            if (golongan == "ALL" || golongan == "K")
            {
                level = "BOD";
            }
            //else if (golongan == "K")
            //{
            //    level = "BOD";
            //}
            //else if (golongan == "D")
            //{
            //    level = "BOD-1";
            //}
            //else if (golongan == "E")
            //{
            //    level = "BOD-1";
            //}
            //else if (golongan == "F")
            //{
            //    level = "BOD-1";
            //}
            else if (golongan == "BOD-1" || golongan == "D" || golongan == "E" || golongan == "F")
            {
                level = "BOD-1";
            }
            else if (golongan == "1" || golongan == "BOD-2")
            {
                level = "BOD-2";
            }
            else if (golongan == "2" || golongan == "BOD-3")
            {
                level = "BOD-3";
            }
            else if (golongan == "3" || golongan == "BOD-4")
            {
                level = "BOD-4";
            }
            else if (golongan == "4" || golongan == "BOD-5")
            {
                level = "BOD-5";
            }
            else if (golongan == "5" || golongan == "BOD-6")
            {
                level = "BOD-6";
            }
            else
            {
                level = "BOD-7";
            }
            return level;
        }

        public ResponseViewModels<bool> UpdateUser()
        {
            string password = _appSettings.DefaultPassword;
            var log = new TriggerActivityLogViewModel();
            try
            {
                var users = dbContext.Users.Where(a => a.IsDeleted == false && (a.Isldap == 1 || a.Isldap == 0) && a.RecentPositionId != 99999999).Select(a => a.EmployeeNumber).ToList();
                foreach (var item in users)
                {
                    try
                    {
                        svc.UpdateUser(item);
                    }
                    catch (Exception e)
                    {

                    }
                }
                log.Type = "Update Data User";
                log.Message = "Proses Berhasil";
                log.IsSuccess = true;
                SendLog(log);
            }
            catch (Exception e)
            {
                return new ResponseViewModels<bool>()
                {
                    StatusCode = 400,
                    Message = "Failed Update Data . Error :" + e.Message + ". " + e.StackTrace,
                    Data = false,
                };
            }
            //new user
            try
            {
                var userJmCore = jMCoreContext.JmEmployee.Where(a => a.RecentPositionId != 99999999 && a.RecentPositionId != null && a.PersonName != null).ToList();
                var userJMArsip = dbContext.Users.Where(a => a.IsDeleted == false && (a.Isldap == 1 || a.Isldap == 0) && a.RecentPositionId != 99999999).Select(a => a.EmployeeNumber).ToList();

                userJmCore = userJmCore.Where(a => !userJMArsip.Any(t => t == a.EmployeeNumber)).ToList();
                foreach (var item in userJmCore)
                {
                    try
                    {
                        svc.GetEmployee(item.PersonName, item.EmployeeNumber, item.EmailAddress, password);
                    }
                    catch (Exception e)
                    {

                    }
                }

                log.Type = "Entry Data User from JM Core";
                log.Message = "Proses Berhasil";
                log.IsSuccess = true;
                SendLog(log);
            }
            catch (Exception e)
            {

            }
            return new ResponseViewModels<bool>()
            {
                StatusCode = 200,
                Message = "Succesfully Update Data",
                Data = false,
            };
        }

        public void SendLog(TriggerActivityLogViewModel vm)
        {
            string url = _apiEndpoint.SendLog;
            //url = "https://localhost:44333/api/TriggerActivityLog";
            string token = string.Empty;
            string jsonData = "{\"type\":" + JsonConvert.SerializeObject(vm.Type) + "," +
                                           "\"message\":" + JsonConvert.SerializeObject(vm.Message) + "," +
                                           "\"isTrigger\":true," +
                                            "\"isSuccess\":" + JsonConvert.SerializeObject(vm.IsSuccess) + "}";

            IRestClient client = new RestClient();
            IRestRequest req = new RestRequest()
            {
                Resource = url
            };
            client.AddDefaultHeader("accept", "application/json");
            client.AddDefaultHeader("Content-type", "application/json");
            req.AddHeader("Authorization", $"Bearer {token}");
            req.AddJsonBody(jsonData);

            IRestResponse responseApi = client.Post(req);
        }

        public ResponseViewModels<List<ProcessingUnitViewModel>> GetAll(ProcessingUnitViewModel vm, ProcessingUnitFilter filter)
        {
            var datas = new List<ProcessingUnitViewModel>();
            var query = jMCoreContext.JmHistoryJabatan2.Where(a => a.UnitKerja != null).AsQueryable();

            if (!string.IsNullOrEmpty(filter.FirstChar))
            {
                query = query.Where(a => a.UnitKerja.ToLower().Contains(filter.FirstChar.ToLower()));
            }


            var data = query.DistinctBy(a => a.UnitKerja).Select(t => new ProcessingUnitViewModel
            {
                UnitName = t.UnitKerja,
                Id = t.UnitKerjaId
            }).ToList();

            return new ResponseViewModels<List<ProcessingUnitViewModel>>
            {
                StatusCode = 200,
                Message = "Data Retrieved",
                Data = data
            };
        }
    }


    public class SeedMessage
    {
        public string Status { set; get; }
        public string Message { set; get; }
    }

    public enum SeedStatus
    {
        ERROR,
        SUCCESS
    }
}
