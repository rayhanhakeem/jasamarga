﻿using AutoMapper;
using Jasamarga.DA.UserManagement.Databases;
using Jasamarga.DA.UserManagement.Databases.Identity;
using Jasamarga.DA.UserManagement.Databases.Models;
using Jasamarga.DA.UserManagement.Services.Contracts;
using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using Npgsql;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using MimeKit;
using MimeKit.Text;
using MailKit.Net.Smtp;

namespace Jasamarga.DA.UserManagement.Services.Repositories
{
    public class UserDeviceServices : TokenServices, IUserDeviceServices
    {
        protected IMapper mapper { set; get; }
        protected ApplicationDbContext dbContext { set; get; }
        protected string includes { set; get; }
        private UserManager<ApplicationUser> _userManager;
        private RoleManager<ApplicationRole> _roleManager;
        private readonly AppSettings _appSettings;
        private readonly ApiEndpoint _apiEndpoint;
        public static string DirName = "User";
        private IHttpContextAccessor accessor;

        public UserDeviceServices(IMapper mapper,
            ApplicationDbContext dbContext,
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            IOptions<AppSettings> appSettings,
            IOptions<ApiEndpoint> apiEndpoint,
            IHttpContextAccessor accessor) : base(accessor, dbContext)
        {
            this.mapper = mapper;
            this.dbContext = dbContext;
            this._userManager = userManager;
            this._roleManager = roleManager;
            this._appSettings = appSettings.Value;
            this._apiEndpoint = apiEndpoint.Value;
            this.accessor = accessor;
        }

        #region userDevice
        public ResponseViewModels<List<UserDevicesViewModels>> GetListUserDevices()
        {
            var model = dbContext.UserDevices.ToList();
            if (model.Count < 1)
            {
                return new ResponseViewModels<List<UserDevicesViewModels>>()
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }
            List<UserDevicesViewModels> listvm = new List<UserDevicesViewModels>();
            foreach (var item in model)
            {
                UserDevicesViewModels vm = new UserDevicesViewModels();
                item.User = dbContext.Users.Where(a => a.Id == item.UserId).FirstOrDefault();
                vm = mapper.Map<UserDevicesViewModels>(item);

                listvm.Add(vm);
            }

            return new ResponseViewModels<List<UserDevicesViewModels>>()
            {
                StatusCode = 200,
                Message = "Data Retrieved",
                Data = listvm
            };
        }
        public ResponseViewModels<UserDevicesViewModels> GetUserDevices(string deviceId)
        {
            var model = dbContext.UserDevices.Where(a => a.DeviceId == deviceId).FirstOrDefault();
            if (model == null)
            {
                return new ResponseViewModels<UserDevicesViewModels>()
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }
            UserDevicesViewModels vm = new UserDevicesViewModels();
            model.User = dbContext.Users.Where(a => a.Id == model.UserId).FirstOrDefault();
            vm = mapper.Map<UserDevicesViewModels>(model);

            return new ResponseViewModels<UserDevicesViewModels>()
            {
                StatusCode = 200,
                Message = "Data Retrieved",
                Data = vm
            };
        }
        public ResponseViewModels<List<UserDevicesViewModels>> GetListUserDevicesByUserId(string userId)
        {
            var model = dbContext.UserDevices.Where(a => a.UserId == userId && a.DeviceId != string.Empty).ToList();
            if (model.Count == 0)
            {
                return new ResponseViewModels<List<UserDevicesViewModels>>()
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }
            List<UserDevicesViewModels> listvm = new List<UserDevicesViewModels>();
            foreach (var item in model)
            {
                UserDevicesViewModels vm = new UserDevicesViewModels();
                //item.User = dbContext.Users.Where(a => a.Id == item.UserId).FirstOrDefault();
                vm = mapper.Map<UserDevicesViewModels>(item);

                listvm.Add(vm);
            }

            return new ResponseViewModels<List<UserDevicesViewModels>>()
            {
                StatusCode = 200,
                Message = "Data Retrieved",
                Data = listvm
            };
        }
        #endregion
    }
}
