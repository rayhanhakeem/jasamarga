﻿using AutoMapper;
using Hanssens.Net;
using Jasamarga.DA.UserManagement.Databases;
using Jasamarga.DA.UserManagement.Databases.Identity;
using Jasamarga.DA.UserManagement.Databases.Models;
using Jasamarga.DA.UserManagement.Services.Contracts;
using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Http.Extensions;
using Npgsql;
using RestSharp;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Features;
using System.Globalization;

namespace Jasamarga.DA.UserManagement.Services.Repositories
{
    public class AdminServices : TokenServices, IAdminServices
    {
        protected IMapper mapper { set; get; }
        private IConfiguration configuration;
        protected ApplicationDbContext dbContext { set; get; }
        protected string includes { set; get; }
        private UserManager<ApplicationUser> _userManager;
        private RoleManager<ApplicationRole> _roleManager;
        private SignInManager<ApplicationUser> _signInManager;
        private readonly AppSettings _appSettings;
        private readonly ApiEndpoint _apiEndpoint;
        public static string DirName = "User";
        private IHttpContextAccessor accessor;

        public AdminServices(IMapper mapper,
            ApplicationDbContext dbContext,
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            IOptions<AppSettings> appSettings,
            IOptions<ApiEndpoint> apiEndpoint,
            SignInManager<ApplicationUser> signInManager,
            IConfiguration configuration,
            IHttpContextAccessor accessor) : base(accessor, dbContext)
        {
            this.mapper = mapper;
            this.dbContext = dbContext;
            this._userManager = userManager;
            this._roleManager = roleManager;
            this._appSettings = appSettings.Value;
            this._signInManager = signInManager;
            this._apiEndpoint = apiEndpoint.Value;
            this.configuration = configuration;
            this.accessor = accessor;
        }

        #region Admin
        public ResponseViewModels<PaginatedList<AdminViewModel>> GetAllActiveAdmin(AdminViewModel vm, int? page = 1, int? perPage = 10, string orderBy = null, string order = null, string searchField = null, string queryString = null)
        {
            var response = new ResponseViewModels<PaginatedList<AdminViewModel>>();
            var query = dbContext.Set<ApplicationUser>().AsQueryable();
            query = query.Where(a => a.RoleName == "Admin");
            if (!string.IsNullOrEmpty(queryString))
            {
                if (!string.IsNullOrEmpty(searchField))
                {
                    query = query.Where($"{searchField}.ToLower().Contains(@0)", queryString);
                }
                else
                {
                    string searchTemplate = "";
                    var fields = vm.GetSearchField();
                    foreach (var field in fields)
                    {
                        searchTemplate = string.IsNullOrEmpty(searchTemplate) ? $"{field}.ToLower().Contains(@0)" : $"{searchTemplate} or {field}.ToLower().Contains(@0)";
                    }

                    query = query.Where(searchTemplate, queryString);
                }
            }

            if (!string.IsNullOrEmpty(includes))
            {
                var includeList = includes.Split(',');
                foreach (var inc in includeList)
                {
                    query = query.Include(inc.Trim());
                }
            }

            var data = new List<AdminViewModel>();
            if (!string.IsNullOrEmpty(orderBy) && vm.GetProperties().Contains(orderBy))
            {
                data = mapper.Map<List<AdminViewModel>>(query.OrderBy(orderBy + " " + (order ?? "ascending")).ToDynamicList());
            }
            else
            {
                data = mapper.Map<List<AdminViewModel>>(query.OrderBy("CreatedDate " + (order ?? "ascending")).ToDynamicList());
            }

            var dataV2 = new List<AdminViewModel>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;
            foreach (var it in data)
            {
                mapper.Map<AdminViewModel>(it);
                it.PersonName = ti.ToTitleCase(it.PersonName.ToLower());
                dataV2.Add(it);
            }

            if (dataV2.Count == 0)
            {
                response.StatusCode = 204;
                response.Message = "No Data Retrieved";
            }
            else
            {
                response.StatusCode = 200;
                response.Message = "Data Retrieved";
            }

            response.Data = new PaginatedList<AdminViewModel>(dataV2, dataV2.Count, page ?? 1, perPage ?? 10);
            return response;
        }
        public ResponseViewModels<AdminViewModel> EditAdmin(AdminViewModel model)
        {
            var current = _userManager.FindByIdAsync(model.Id).Result;
            if (current == null)
            {
                return new ResponseViewModels<AdminViewModel>
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }
            try
            {
                var roleAdmin = dbContext.Roles.Where(a => a.Name == model.RoleName).FirstOrDefault();
                var currentUserRoles = dbContext.UserRoles.Where(a => a.UserId == current.Id).FirstOrDefault();

                if (currentUserRoles != null)
                {
                    //deleting User Roles
                    dbContext.Remove(currentUserRoles);
                    dbContext.SaveChanges();
                }
                //updating User Roles
                current.IsActive = model.isActive;
                current.RoleId = roleAdmin.Id;
                current.RoleName = roleAdmin.Name;
                current.UpdatedDate = DateTime.Now;
                
                IdentityResult updateAdmin = _userManager.AddToRoleAsync(current, roleAdmin.Name).Result;
                dbContext.SaveChanges();

                RolesFromUser(current.Id);

                return new ResponseViewModels<AdminViewModel>
                {
                    StatusCode = 201,
                    Message = "Data Successfully Saved",
                    Data = mapper.Map<AdminViewModel>(model)
                };
            }
            catch (Exception e)
            {
                return new ResponseViewModels<AdminViewModel>
                {
                    StatusCode = 400,
                    Message = "Failed To Save Data",
                    Data = null
                };
            }
        }
        public ResponseViewModels<AdminViewModel> DeleteAdmin(string id)
        {
            var model = dbContext.Users.Where(a => a.Id == id).FirstOrDefault();

            if (model == null)
            {

                return new ResponseViewModels<AdminViewModel>
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }

            try
            {
                if (model != null)
                {
                    var rolesForUser = dbContext.UserRoles.Where(a => a.UserId == id).FirstOrDefault();
                    if (rolesForUser != null)
                    {
                        dbContext.Remove(rolesForUser);
                    }

                    //IdentityResult roles = _userManager.AddToRoleAsync(model, "Public").Result;
                    model.RoleId = "f4a3bfb3-511c-40f3-8e26-b873561d4d81"; 
                    model.RoleName = "Public";
                    dbContext.Users.Update(model);
                    dbContext.SaveChanges();

                    IdentityResult roles = _userManager.AddToRoleAsync(model, "Public").Result;
                }
                RolesFromUser(model.Id);
                return new ResponseViewModels<AdminViewModel>
                {
                    StatusCode = 200,
                    Message = "Data Successfully Deleted",
                    Data = null
                };
            }
            catch (Exception e)
            {
                return new ResponseViewModels<AdminViewModel>
                {
                    StatusCode = 400,
                    Message = "Failed To Delete Data",
                    Data = null
                };
            }
        }
        public ResponseViewModels<AdminViewModel> GetAdmin(string id)
        {
            var model = dbContext.Users.Include("UserRoles").Where(a => a.Id == id).FirstOrDefault();

            if (model == null)
            {
                return new ResponseViewModels<AdminViewModel>
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }

            return new ResponseViewModels<AdminViewModel>
            {
                StatusCode = 200,
                Message = "Data Retrieved",
                Data = mapper.Map<AdminViewModel>(model)
            };
        }

        public void RolesFromUser(string Id)
        {
            var user = dbContext.Users.Where(a => a.Id == Id).FirstOrDefault();
            if (user != null)
            {
                var exist = dbContext.UserRolesAdditionals.Where(a => a.Type == "User Main Role" && a.TypeId == Id).FirstOrDefault();
                if (exist != null)
                {
                    exist.PejabatRoleId = user.RoleId;
                    exist.PejabatRoleName = user.RoleName;
                    exist.UpdatedDate = DateTime.Now;
                    dbContext.UserRolesAdditionals.Update(exist);
                    dbContext.SaveChanges();
                }
                else
                {
                    var userRoles = new UserRolesAdditional()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Type = "User Main Role",
                        TypeId = Id,
                        LoginUserPersonId = user.Id,
                        LoginUserUnitKerja = user.UnitKerja,
                        LoginUserUnitKerjaId = user.UnitKerjaId,
                        PejabatId = user.Id,
                        PejabatBODLEvel = user.BODLevel,
                        PejabatName = user.PersonName,
                        PejabatJabatanId = user.RecentPositionId,
                        PejabatJabatanName = user.PositionName,
                        PejabatRoleId = user.RoleId,
                        PejabatRoleName = user.RoleName,
                        SecretaryActive = false,
                        isActive = false,
                        DelegationEndDate = null,
                        DelegationStartDate = null,
                        CreatedDate = DateTime.Now
                    };
                    dbContext.UserRolesAdditionals.Add(userRoles);
                    dbContext.SaveChanges();
                }
            }
        }
        #endregion
    }
}
