﻿using AutoMapper;
using Jasamarga.DA.UserManagement.Databases;
using Jasamarga.DA.UserManagement.Databases.Identity;
using Jasamarga.DA.UserManagement.Databases.Models;
using Jasamarga.DA.UserManagement.Services.Contracts;
using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using Npgsql;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using MimeKit;
using MimeKit.Text;
using MailKit.Net.Smtp;

namespace Jasamarga.DA.UserManagement.Services.Repositories
{
    public class AccountServices : TokenServices, IAccountServices
    {
        protected IMapper mapper { set; get; }
        protected ApplicationDbContext dbContext { set; get; }
        protected string includes { set; get; }
        private UserManager<ApplicationUser> _userManager;
        private RoleManager<ApplicationRole> _roleManager;
        private readonly AppSettings _appSettings;
        private readonly ApiEndpoint _apiEndpoint;
        public static string DirName = "User";
        private IHttpContextAccessor accessor;
        protected IHelperServices helperSvc;

        public AccountServices(IMapper mapper,
            ApplicationDbContext dbContext,
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            IOptions<AppSettings> appSettings,
            IOptions<ApiEndpoint> apiEndpoint,
            IHttpContextAccessor accessor,
            IHelperServices helperSvc) : base(accessor, dbContext)
        {
            this.mapper = mapper;
            this.dbContext = dbContext;
            this._userManager = userManager;
            this._roleManager = roleManager;
            this._appSettings = appSettings.Value;
            this._apiEndpoint = apiEndpoint.Value;
            this.accessor = accessor;
            this.helperSvc = helperSvc;
        }

        public IEnumerable<Permission> GetActionList()
        {
            var list = dbContext.Permissions.Where(a => a.IsDeleted == false).ToList();
            return list;
        }
        #region User
        public async Task<ResponseViewModels<PaginatedUserList<UsersViewModel>>> GetAllActiveUser(UsersViewModel vm, FilterParameterViewModels filter, bool? isActive = null, bool? includeParent = null, bool? includeDelegation = null, bool? includeGroup = null, string level = null, string unitKerjaId = null)
        {

            var response = new ResponseViewModels<PaginatedUserList<UsersViewModel>>();
            var query = dbContext.Set<ApplicationUser>().AsQueryable();

            if (filter.FirstChar != null)
            {
                bool isIndex = false;
                query = query.Where(a => a.RecentPositionId != 99999999);
                query = query.Where(a => a.IsDelegation != true);
                query = query.Where(a => a.IsDeleted == false);
                //query = query.Where(a => a.IsDelegation == false || a.IsDelegation == null);

                if (filter.FirstChar != null)
                {
                    query = query.Where(a => a.EmployeeNumber.ToLower().Contains(filter.FirstChar.ToLower()) || a.PersonName.ToLower().Contains(filter.FirstChar.ToLower())
                    || a.PositionName.ToLower().Contains(filter.FirstChar.ToLower()));
                }
                //if (filter.FirstChar == null)
                //{
                //    query = query.Where(a => a.PersonName.ToLower().StartsWith("a")).Take(30);
                //}
                //if (filter.IsNaskahDinas == null || filter.IsNaskahDinas == false)
                //{
                //    //query = query.Where(a => a.IsGroup == false);
                //    query = query.Where(a => a.IsDelegation == false || a.IsDelegation == null);
                //}
                if (level != null)
                {
                    query = query.Where(a => a.BODLevel == level);
                }
                if (includeParent == false)
                {
                    query = query.Where(a => a.ParentUserId == null);
                }
                if (includeGroup == false || includeGroup == null)
                {
                    query = query.Where(a => a.IsGroup == false || a.IsGroup == null);
                }
                if (unitKerjaId != null)
                {
                    query = query.Where(a => a.UnitKerjaId == Convert.ToInt64(unitKerjaId) );
                }
                if (!string.IsNullOrEmpty(filter.Query))
                {
                    if (!string.IsNullOrEmpty(filter.SearchField))
                    {
                        query = query.Where($"{filter.SearchField}.ToLower().Contains(@0)", filter.Query);
                    }
                    else
                    {
                        string searchTemplate = "";
                        var fields = vm.GetSearchField();
                        foreach (var field in fields)
                        {
                            searchTemplate = string.IsNullOrEmpty(searchTemplate) ? $"{field}.ToLower().Contains(@0)" : $"{searchTemplate} or {field}.ToLower().Contains(@0)";
                        }
                        query = query.Where(searchTemplate, filter.Query);
                    }
                }

                if (!string.IsNullOrEmpty(includes))
                {
                    var includeList = includes.Split(',');
                    foreach (var inc in includeList)
                    {
                        query = query.Include(inc.Trim());
                    }
                }

                var data = new List<UsersViewModel>();
                if (includeGroup == true && !string.IsNullOrEmpty(filter.OrderBy) && vm.GetProperties().Contains(filter.OrderBy))
                {
                    data = mapper.Map<List<UsersViewModel>>(query.OrderBy("IsGroup" + " " + ("descending")).ThenBy(filter.OrderBy + " " + (filter.Order ?? "ascending")).ToDynamicList());
                }
                else if (includeGroup == false || filter.IsNaskahDinas == null && !string.IsNullOrEmpty(filter.OrderBy) && vm.GetProperties().Contains(filter.OrderBy))
                {
                    data = mapper.Map<List<UsersViewModel>>(query.OrderBy(filter.OrderBy + " " + (filter.Order ?? "ascending")).ToDynamicList());
                }
                else
                {
                    data = mapper.Map<List<UsersViewModel>>(query.OrderBy("CreatedDate " + (filter.Order ?? "ascending")).ToDynamicList());
                }

                if (isActive != null)
                {
                    data = data.Where(a => a.isActive == isActive).ToList();
                }
                var dataV2 = new List<UsersViewModel>();
                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                foreach (var item in data)
                {
                    UsersViewModel uvm = new UsersViewModel();
                    uvm = mapper.Map<UsersViewModel>(item);
                    if (item.IsGroup == false)
                    {
                        uvm.PersonName = ti.ToTitleCase(item.PersonName.ToLower());
                    }
                    uvm.PersonId = item.Id;
                    uvm.UserId = item.Id;
                    uvm.UserPositionId = item.RecentPositionId.ToString();
                    uvm.UserPosition = item.PositionName;
                    uvm.UserAdditionals = helperSvc.GetUserAdditional(item.Id);
                    uvm.ActiveAdditionalRoles = helperSvc.GetActiveAdditionalRolesV2(item.Id);
                    uvm.Delegation = GetDelegation(item.Id);
                    if (uvm.Delegation != null)
                    {
                        uvm.DelegationId = uvm.Delegation.Id;
                    }
                    else
                    {
                        uvm.DelegationId = null;
                    }

                    uvm.DelegationId = uvm.Delegation?.Id;
                    if (vm.ActiveAdditionalRoles == null)
                    {
                        uvm.Type = "User Main Role";
                    }
                    else
                    {
                        uvm.Type = vm.ActiveAdditionalRoles.Type;
                    }
                    //uvm.DelegatedFrom = helperSvc.GetDelegatedFrom(item.Id);
                    if (item.IsGroup == true)
                    {
                        uvm.GroupUnit = await GetGroupUnit(item.Id);
                    }
                    dataV2.Add(uvm);
                }
                if (includeDelegation == true)
                {
                    var listuser = dataV2.Select(a => a.Id).ToList();
                    if (filter.FirstChar != null)
                    {
                        var listuserdelegasi = helperSvc.GetDelegasiUserV2(filter.FirstChar, listuser);
                        dataV2.AddRange(listuserdelegasi);
                        dataV2 = dataV2.OrderBy(a => a.PersonName).ToList();
                    }
                }

                if (dataV2.Count == 0)
                {
                    response.StatusCode = 204;
                    response.Message = "No Data Retrieved";
                }
                else
                {
                    response.StatusCode = 200;
                    response.Message = "Data Retrieved";
                }

                filter.PerPage = filter.PerPage == -1 ? dataV2.Count() : filter.PerPage;
                response.Data = new PaginatedUserList<UsersViewModel>(dataV2, dataV2.Count, filter.Page ?? 1, filter.PerPage ?? 10, isIndex);
            }
            else
            {
                int take = filter.PerPage ?? 10;
                int page = filter.Page ?? 1;
                int skip = take * (page - 1);
                bool isIndex = true;
                query = query.Where(a => a.RecentPositionId != 99999999);
                query = query.Where(a => a.IsDelegation != true);
                query = query.Where(a => a.IsDeleted == false);
                //query = query.Where(a => a.IsDelegation == false || a.IsDelegation == null);


                if (level != null)
                {
                    query = query.Where(a => a.BODLevel == level);
                }
                if (includeParent == false)
                {
                    query = query.Where(a => a.ParentUserId == null);
                }
                if (includeGroup == false || includeGroup == null)
                {
                    query = query.Where(a => a.IsGroup == false || a.IsGroup == null);
                }

                if (!string.IsNullOrEmpty(filter.Query))
                {
                    if (!string.IsNullOrEmpty(filter.SearchField))
                    {
                        query = query.Where($"{filter.SearchField}.ToLower().Contains(@0)", filter.Query);
                    }
                    else
                    {
                        string searchTemplate = "";
                        var fields = vm.GetSearchField();
                        foreach (var field in fields)
                        {
                            searchTemplate = string.IsNullOrEmpty(searchTemplate) ? $"{field}.ToLower().Contains(@0)" : $"{searchTemplate} or {field}.ToLower().Contains(@0)";
                        }
                        query = query.Where(searchTemplate, filter.Query);
                    }
                }

                if (!string.IsNullOrEmpty(includes))
                {
                    var includeList = includes.Split(',');
                    foreach (var inc in includeList)
                    {
                        query = query.Include(inc.Trim());
                    }
                }

                if (isActive != null)
                {
                    query = query.Where(a => a.IsActive == isActive);
                }

                var data = new List<UsersViewModel>();
                int count = query.Count();

                if (includeGroup == true && !string.IsNullOrEmpty(filter.OrderBy) && vm.GetProperties().Contains(filter.OrderBy))
                {
                    data = mapper.Map<List<UsersViewModel>>(query.OrderBy("IsGroup" + " " + ("descending")).ThenBy(filter.OrderBy + " " + (filter.Order ?? "ascending")).Skip(skip).Take(take).ToDynamicList());
                }
                else if (includeGroup == false || filter.IsNaskahDinas == null && !string.IsNullOrEmpty(filter.OrderBy) && vm.GetProperties().Contains(filter.OrderBy))
                {
                    data = mapper.Map<List<UsersViewModel>>(query.OrderBy(filter.OrderBy + " " + (filter.Order ?? "ascending")).Skip(skip).Take(take).ToDynamicList());
                }
                else
                {
                    data = mapper.Map<List<UsersViewModel>>(query.OrderBy("CreatedDate " + (filter.Order ?? "ascending")).Skip(skip).Take(take).ToDynamicList());
                }
                var dataV2 = new List<UsersViewModel>();
                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                foreach (var item in data)
                {
                    UsersViewModel uvm = new UsersViewModel();
                    uvm = mapper.Map<UsersViewModel>(item);
                    if (item.IsGroup == false)
                    {
                        uvm.PersonName = ti.ToTitleCase(item.PersonName.ToLower());
                    }
                    //uvm.DelegatedFrom = helperSvc.GetDelegatedFrom(item.Id);
                    if (item.IsGroup == true)
                    {
                        uvm.GroupUnit = await GetGroupUnit(item.Id);
                    }
                    dataV2.Add(uvm);
                }
                if (includeDelegation == true)
                {
                    var listuser = dataV2.Select(a => a.Id).ToList();
                    if (filter.FirstChar != null)
                    {
                        var listuserdelegasi = helperSvc.GetDelegasiUserV2(filter.FirstChar, listuser);
                        dataV2.AddRange(listuserdelegasi);
                        dataV2 = dataV2.OrderBy(a => a.PersonName).ToList();
                    }
                }

                if (dataV2.Count == 0)
                {
                    response.StatusCode = 204;
                    response.Message = "No Data Retrieved";
                }
                else
                {
                    response.StatusCode = 200;
                    response.Message = "Data Retrieved";
                }

                filter.PerPage = filter.PerPage == -1 ? count : filter.PerPage;
                response.Data = new PaginatedUserList<UsersViewModel>(dataV2, count, filter.Page ?? 1, filter.PerPage ?? 10, isIndex);
            }

            return response;
        }
        public async Task<ResponseViewModels<PaginatedList<UsersViewModel>>> PejabatSekretaris(UsersViewModel vm, FilterParameterViewModels filter, string userId = null, string secretaryId = null)
        {
            List<SecretaryViewModels> activeSecretary = await GetActiveSecretary();
            var response = new ResponseViewModels<PaginatedList<UsersViewModel>>();
            var model = dbContext.Users.Where(a => a.Id == userId).FirstOrDefault();
            var listModel = dbContext.Users.Where(a => a.IsActive == true && a.Level < model.Level && a.RecentPositionId != 99999999 && a.RecentPositionId != null
            && a.ParentUserId == null && a.IsDelegation == null).ToList();
            if (filter.FirstChar != null)
            {
                listModel = listModel.Where(a => a.PersonName.ToLower().Contains(filter.FirstChar.ToLower())
                || a.PositionName.ToLower().Contains(filter.FirstChar.ToLower())).ToList();
            }

            if (activeSecretary != null)
            {
                var exist = activeSecretary.Where(a => a.UserSecretaryId == userId && a.Id == secretaryId).FirstOrDefault();
                listModel = listModel.Where(a => !activeSecretary.Any(p => p.UserPlacemanId == a.Id)).ToList();
                if (filter.IsEdit == true)
                {
                    if (exist != null)
                    {
                        var userExist = dbContext.Users.Where(a => a.Id == exist.UserPlacemanId).FirstOrDefault();
                        listModel.Add(userExist);
                    }
                }
            }
            var data = new List<UsersViewModel>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;
            foreach (var item in listModel)
            {
                var user = new UsersViewModel();
                user = mapper.Map<UsersViewModel>(item);
                user.PersonName = ti.ToTitleCase(user.PersonName.ToLower());
                data.Add(user);
            }
            data = data.OrderBy(a => a.PersonName).ToList();

            if (data.Count == 0)
            {
                response.StatusCode = 204;
                response.Message = "No Data Retrieved";
            }
            else
            {
                response.StatusCode = 200;
                response.Message = "Data Retrieved";
            }

            filter.PerPage = filter.PerPage == -1 ? data.Count() : filter.PerPage;
            response.Data = new PaginatedList<UsersViewModel>(data, data.Count, filter.Page ?? 1, filter.PerPage ?? 10);
            return response;
        }
        public ResponseViewModels<UserViewModel> EditUser(UpdateUserViewModel model)
        {
            var current = dbContext.Users.Where(a => a.Id == model.Id).FirstOrDefault();
            if (current == null)
            {
                return new ResponseViewModels<UserViewModel>
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }
            try
            {
                //updating User
                current.UpdatedDate = DateTime.Now;
                current.IsActive = model.isActive;
                current.IsUnitKearsipan = model.IsUnitKearsipan;

                //update image
                if (model.ImageAttachment != null)
                {
                    var image = helperSvc.Upload(model.ImageAttachment, current);
                    current.UrlImage = image.UrlImage;
                    current.ImageFileName = image.FileName;
                    current.UrlImageResize = image.UrlImageResize;
                    current.ImageFileNameResize = image.FileNameResize;
                }
                var role = dbContext.Roles.Where(a => a.Id == model.RoleId).FirstOrDefault();
                var currentUserRoles = dbContext.UserRoles.Where(a => a.UserId == current.Id).FirstOrDefault();
                current.RoleId = role.Id;
                current.RoleName = role.Name;
                if (currentUserRoles == null)
                {
                    IdentityResult updateRoles = _userManager.AddToRoleAsync(current, role.Name).Result;
                }
                else if (currentUserRoles.RoleId != model.RoleId)
                {
                    dbContext.UserRoles.Remove(currentUserRoles);
                    dbContext.SaveChanges();
                    IdentityResult updateRoles = _userManager.AddToRoleAsync(current, role.Name).Result;
                }
                dbContext.Users.Update(current);
                dbContext.SaveChanges();
                helperSvc.RolesFromUser(current.Id);
                var user = dbContext.Users.Where(a => a.Id == model.Id).FirstOrDefault();
                return new ResponseViewModels<UserViewModel>
                {
                    StatusCode = 201,
                    Message = "Data Successfully Saved",
                    Data = mapper.Map<UserViewModel>(user)
                };
            }
            catch (Exception e)
            {
                return new ResponseViewModels<UserViewModel>
                {
                    StatusCode = 400,
                    Message = "Failed To Save Data",
                    Data = null
                };
            }
        }
        public async Task<ResponseViewModels<UserViewModel>> EditUserV2(UpdateUserViewModelV2 model)
        {
            var current = dbContext.Users.Where(a => a.Id == model.Id).FirstOrDefault();
            if (current == null)
            {
                return new ResponseViewModels<UserViewModel>
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }
            try
            {

                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                current.IsActive = model.isActive;
                current.IsUnitKearsipan = model.IsUnitKearsipan;
                var role = dbContext.Roles.Where(a => a.Id == model.RoleId).FirstOrDefault();
                var currentUserRoles = dbContext.UserRoles.Where(a => a.UserId == current.Id).FirstOrDefault();
                current.RoleId = role.Id;
                current.RoleName = role.Name;
                current.UserName = model.UserName;
                current.PersonName = ti.ToTitleCase(model.PersonName.ToLower());
                current.Email = model.Email.ToLower();
                current.EmployeeNumber = model.EmployeeNumber;
                current.Golongan = model.Golongan;
                current.Level = helperSvc.GetLevelUser(current.Golongan);
                current.BODLevel = helperSvc.GetBODLevelUser(current.Golongan);
                current.UnitKerja = model.UnitKerja;
                current.UnitKerjaId = model.UnitKerjaId;
                current.OrganizationId = model.OrganizationId;
                current.OrgName = model.OrgName;
                current.OrgIdParent = model.OrgIdParent;
                if (model.RecentPositionId == null)
                {
                    helperSvc.InsertPosition(model.OrganizationId, model.OrgName, model.OrgIdParent, model.PositionName);
                    var position = helperSvc.GetPosition(model.PositionName);
                    current.RecentPositionId = position.RecentPositionId;
                    current.PositionName = position.PositionName;
                }
                else if (current.RecentPositionId != null && current.RecentPositionId != model.RecentPositionId)
                {
                    current.RecentPositionId = model.RecentPositionId;
                    current.PositionName = model.PositionName;
                    helperSvc.UpdateUserMainRole(current.Id, current.RecentPositionId, current.PositionName);
                }
                else if (current.RecentPositionId == null && current.RecentPositionId == null)
                {
                    current.RecentPositionId = model.RecentPositionId;
                    current.PositionName = model.PositionName;
                    helperSvc.UpdateUserMainRole(current.Id, current.RecentPositionId, current.PositionName);
                }
                if (currentUserRoles == null)
                {
                    ApplicationUserRole userRole = new ApplicationUserRole();
                    userRole.User = current;
                    userRole.UserId = current.Id;
                    userRole.Role = role;
                    userRole.RoleId = role.Id;
                    dbContext.UserRoles.Add(userRole);
                    dbContext.SaveChanges();
                    //IdentityResult updateRoles = _userManager.AddToRoleAsync(current, role.Name).Result;
                }
                else if (currentUserRoles.RoleId != model.RoleId)
                {
                    dbContext.UserRoles.Remove(currentUserRoles);
                    dbContext.SaveChanges();
                    ApplicationUserRole userRole = new ApplicationUserRole();
                    userRole.User = current;
                    userRole.UserId = current.Id;
                    userRole.Role = role;
                    userRole.RoleId = role.Id;
                    dbContext.UserRoles.Add(userRole);
                    dbContext.SaveChanges();
                    //IdentityResult updateRoles = _userManager.AddToRoleAsync(current, role.Name).Result;
                }
                //update image
                if (model.ImageAttachment != null)
                {
                    var image = helperSvc.Upload(model.ImageAttachment, current);
                    current.UrlImage = image.UrlImage;
                    current.ImageFileName = image.FileName;
                    current.UrlImageResize = image.UrlImageResize;
                    current.ImageFileNameResize = image.FileNameResize;
                }
                current.UpdatedDate = DateTime.Now;
                if (current.Isldap == 2)
                {
                    //Cek username
                    if (current.UserName != model.UserName)
                    {
                        var check = await _userManager.FindByNameAsync(model.UserName);
                        if (check != null)
                        {
                            return new ResponseViewModels<UserViewModel>
                            {
                                StatusCode = 400,
                                Message = "Username sudah dipakai",
                                Data = null
                            };
                        }
                    }
                    //updating User

                }
                dbContext.Users.Update(current);
                dbContext.SaveChanges();
                helperSvc.RolesFromUser(current.Id);
                helperSvc.UpdateUserAdditional(current.Id, current.RecentPositionId, current.PositionName, current.RoleId, current.RoleName, current.IsActive);
                //var user = dbContext.Users.Where(a => a.Id == model.Id).FirstOrDefault();
                //var result = await _userManager.UpdateAsync(user);
                return new ResponseViewModels<UserViewModel>
                {
                    StatusCode = 201,
                    Message = "Data Successfully Saved",
                    Data = mapper.Map<UserViewModel>(current)
                };
            }
            catch (Exception e)
            {
                return new ResponseViewModels<UserViewModel>
                {
                    StatusCode = 400,
                    Message = "Failed To Save Data",
                    Data = null
                };
            }
        }
        public async Task<ResponseViewModels<RegisterViewModel>> RegisterUser(RegisterViewModel vm)
        {
            var userExist = dbContext.Users.Where(a => a.Id == vm.Id).FirstOrDefault();
            if (userExist == null)
            {
                return new ResponseViewModels<RegisterViewModel>
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }

            var check = await _userManager.FindByNameAsync(vm.UserName);
            if (check == null)
            {
                ApplicationUser model = new ApplicationUser();
                try
                {
                    string positionName = string.Empty;
                    string connStr = _appSettings.ServerConnString /*"Server=10.1.3.47;Port=5432;Database=jm_core_data;User Id=postgres;Password=5U1tm3di4!"*/;
                    var sql = "SELECT * FROM public.jm_sap_hierarchy where position_id = '" + vm.RecentPositionId + "'" + "Limit 1";

                    using (NpgsqlConnection con = new NpgsqlConnection(connStr))
                    {
                        NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                        con.Open();
                        NpgsqlDataReader reader = cmd.ExecuteReader();
                        try
                        {
                            while (reader.Read())
                            {
                                positionName = reader["position_name"].ToString();
                            }
                        }
                        catch (Exception e)
                        {

                        }
                    }
                    var role = dbContext.Roles.Where(a => a.Id == vm.RoleId).FirstOrDefault();
                    string password = _appSettings.DefaultPassword;
                    if (!string.IsNullOrEmpty(vm.Password)) password = vm.Password;
                    if (vm.ImageAttachment != null)
                    {
                        var image = helperSvc.Upload(vm.ImageAttachment, model);
                        //model.UrlImage = image.UrlImage;
                        model.UrlImage = image.UrlImage;
                        model.ImageFileName = image.FileName;
                        model.UrlImageResize = image.UrlImageResize;
                        model.ImageFileNameResize = image.FileNameResize;
                    }
                    TextInfo ti = CultureInfo.CurrentCulture.TextInfo;
                    string personName = userExist.PersonName;
                    model.UserName = vm.UserName;
                    model.PersonNumber = userExist.PersonNumber;
                    model.Email = ti.ToTitleCase(userExist.Email).ToLower();
                    model.PersonName = ti.ToTitleCase(personName).ToLower();
                    model.EmployeeNumber = userExist.EmployeeNumber;
                    model.OriginalDateOfHire = userExist.OriginalDateOfHire;
                    model.ValidFrom = userExist.ValidFrom;
                    model.ValidTo = userExist.ValidTo;
                    model.DateOfBirth = userExist.DateOfBirth;
                    model.TownOfBirth = userExist.TownOfBirth;
                    model.NationalIdentifier = userExist.NationalIdentifier;
                    model.Sex = userExist.Sex;
                    model.JobId = userExist.JobId;
                    model.JobName = userExist.JobName;
                    model.JobType = userExist.JobType;
                    model.RecentPositionId = vm.RecentPositionId;
                    model.PositionName = positionName.TrimStart();
                    model.PositionNameAlias = userExist.PositionNameAlias;
                    model.PositionNameAtasan = userExist.PositionNameAtasan;
                    model.NomorSk = userExist.NomorSk;
                    model.TanggalSk = userExist.TanggalSk;
                    model.TanggalEfektifSk = userExist.TanggalEfektifSk;
                    model.OrganizationId = userExist.OrganizationId;
                    model.OrgName = userExist.OrgName;
                    model.OrgIdParent = userExist.OrgIdParent;
                    model.OrgParentName = userExist.OrgGrandParentName;
                    model.OrgGrandParent = userExist.OrgGrandParent;
                    model.OrgGrandParentName = userExist.OrgGrandParentName;
                    model.UnitKerja = userExist.UnitKerja;
                    model.LocationId = userExist.LocationId;
                    model.LocationName = userExist.LocationName;
                    model.Golongan = userExist.Golongan;
                    model.Baris = userExist.Baris;
                    model.Ruang = userExist.Ruang;
                    model.Npwp = userExist.Npwp;
                    model.JenisNpwp = userExist.JenisNpwp;
                    model.StatusNpwp = userExist.StatusNpwp;
                    model.MaritalStatus = userExist.MaritalStatus;
                    model.Religion = userExist.Religion;
                    //model.Password = userExist.Password;
                    model.Isldap = 2;
                    model.StatusSat = userExist.StatusSat;
                    model.StatusSap = userExist.StatusSap;
                    model.CreatedDate = DateTime.Now;
                    model.UpdatedDate = DateTime.Now;
                    model.MainRole = userExist.MainRole;
                    model.UnitKerjaId = userExist.UnitKerjaId;
                    model.PayScaleArea = userExist.PayScaleArea;
                    model.PersonNumberApprover = userExist.PersonNumberApprover;
                    model.EmployeeGroup = userExist.EmployeeGroup;
                    model.CntSubOrd = userExist.CntSubOrd;
                    model.PersonalArea = userExist.PersonalArea;
                    model.PositionIdAtasan = userExist.PositionIdAtasan;
                    model.PayrollGroupId = userExist.PayrollGroupId;
                    model.EmployeeSubgroup = userExist.EmployeeSubgroup;
                    model.BusinessArea = userExist.BusinessArea;
                    model.PersonalSubArea = userExist.PersonalSubArea;
                    model.CompanyCode = userExist.CompanyCode;
                    model.EmpType = userExist.EmpType;
                    model.NppAbsen = userExist.NppAbsen;
                    model.EmpStatus = userExist.EmpStatus;
                    model.DataSource = userExist.DataSource;
                    model.Level = helperSvc.GetLevelUser(model.Golongan);
                    model.BODLevel = helperSvc.GetBODLevelUser(model.Golongan);
                    model.JmId = userExist.JmId;
                    model.RoleId = vm.RoleId;
                    model.RoleName = role.Name;
                    model.IsActive = vm.isActive;
                    model.IsUnitKearsipan = vm.IsUnitKearsipan;
                    model.Init();
                    IdentityResult result = _userManager.CreateAsync(model, password).Result;
                    IdentityResult roles = _userManager.AddToRoleAsync(model, role.Name).Result;
                    helperSvc.RolesFromUser(model.Id);
                    //var resultUser = await _userManager.CreateAsync(model, password);
                }
                catch (Exception e)
                {

                }
            }
            else
            {
                return new ResponseViewModels<RegisterViewModel>
                {
                    StatusCode = 400,
                    Message = "Username Already Exist",
                    Data = null
                };
            }

            return new ResponseViewModels<RegisterViewModel>
            {
                StatusCode = 201,
                Message = "Data Successfully Saved",
                Data = null
            };
        }
        public async Task<ResponseViewModels<RegisterViewModelV2>> RegisterUserV2(RegisterViewModelV2 vm)
        {
            if (vm.UserName != null)
            {
                var check = await _userManager.FindByNameAsync(vm.UserName);
                if (check != null)
                {
                    return new ResponseViewModels<RegisterViewModelV2>
                    {
                        StatusCode = 400,
                        Message = "Username sudah dipakai",
                        Data = null
                    };
                }
            }


            try
            {
                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                ApplicationUser model = new ApplicationUser();
                if (vm.RecentPositionId == null)
                {
                    helperSvc.InsertPosition(vm.OrganizationId, vm.OrgName, vm.OrgIdParent, vm.PositionName);
                    var position = helperSvc.GetPosition(vm.PositionName);
                    model.RecentPositionId = position.RecentPositionId;
                    model.PositionName = position.PositionName;
                }
                else
                {
                    model.RecentPositionId = vm.RecentPositionId;
                    model.PositionName = vm.PositionName;
                }
                string password = _appSettings.DefaultPassword;
                var role = dbContext.Roles.Where(a => a.Id == vm.RoleId).FirstOrDefault();

                model.Email = vm.Email.ToLower();
                model.PersonName = ti.ToTitleCase(vm.PersonName.ToLower());
                model.EmployeeNumber = vm.EmployeeNumber;
                model.Isldap = 2;
                if (vm.ImageAttachment != null)
                {
                    var image = helperSvc.Upload(vm.ImageAttachment, model);
                    //model.UrlImage = image.UrlImage;
                    model.UrlImage = image.UrlImage;
                    model.ImageFileName = image.FileName;
                    model.UrlImageResize = image.UrlImageResize;
                    model.ImageFileNameResize = image.FileNameResize;
                }
                model.RoleId = vm.RoleId;
                model.RoleName = role.Name;
                model.IsActive = vm.isActive;
                model.IsUnitKearsipan = vm.IsUnitKearsipan;
                model.Golongan = vm.Golongan;
                model.Level = helperSvc.GetLevelUser(model.Golongan);
                model.BODLevel = helperSvc.GetBODLevelUser(model.Golongan);
                model.UpdatedDate = DateTime.Now;
                model.UnitKerja = vm.UnitKerja;
                model.UnitKerjaId = vm.UnitKerjaId;
                model.OrganizationId = vm.OrganizationId;
                model.OrgName = vm.OrgName;
                model.OrgIdParent = vm.OrgIdParent;
                model.Init();
                //model.ParentUserId = model.Id;
                if (vm.Id != null)
                {
                    var userExist = dbContext.Users.Where(a => a.Id == vm.Id).FirstOrDefault();
                    if (userExist != null)
                    {
                        int count = dbContext.Users.Where(a => a.ParentUserId == userExist.Id).ToList().Count();
                        model.UserName = userExist.UserName + "_" + count.ToString();
                        model.PersonNumber = userExist.PersonNumber;
                        model.OriginalDateOfHire = userExist.OriginalDateOfHire;
                        model.ValidFrom = userExist.ValidFrom;
                        model.ValidTo = userExist.ValidTo;
                        model.DateOfBirth = userExist.DateOfBirth;
                        model.TownOfBirth = userExist.TownOfBirth;
                        model.NationalIdentifier = userExist.NationalIdentifier;
                        model.Sex = userExist.Sex;
                        model.JobId = userExist.JobId;
                        model.JobName = userExist.JobName;
                        model.JobType = userExist.JobType;
                        model.PositionNameAlias = userExist.PositionNameAlias;
                        model.PositionNameAtasan = userExist.PositionNameAtasan;
                        model.NomorSk = userExist.NomorSk;
                        model.TanggalSk = userExist.TanggalSk;
                        model.TanggalEfektifSk = userExist.TanggalEfektifSk;
                        model.OrgParentName = userExist.OrgGrandParentName;
                        model.OrgGrandParent = userExist.OrgGrandParent;
                        model.OrgGrandParentName = userExist.OrgGrandParentName;
                        model.LocationId = userExist.LocationId;
                        model.LocationName = userExist.LocationName;
                        model.Baris = userExist.Baris;
                        model.Ruang = userExist.Ruang;
                        model.Npwp = userExist.Npwp;
                        model.JenisNpwp = userExist.JenisNpwp;
                        model.StatusNpwp = userExist.StatusNpwp;
                        model.MaritalStatus = userExist.MaritalStatus;
                        model.Religion = userExist.Religion;
                        model.StatusSat = userExist.StatusSat;
                        model.StatusSap = userExist.StatusSap;
                        model.MainRole = userExist.MainRole;
                        model.PayScaleArea = userExist.PayScaleArea;
                        model.PersonNumberApprover = userExist.PersonNumberApprover;
                        model.EmployeeGroup = userExist.EmployeeGroup;
                        model.CntSubOrd = userExist.CntSubOrd;
                        model.PersonalArea = userExist.PersonalArea;
                        model.PositionIdAtasan = userExist.PositionIdAtasan;
                        model.PayrollGroupId = userExist.PayrollGroupId;
                        model.EmployeeSubgroup = userExist.EmployeeSubgroup;
                        model.BusinessArea = userExist.BusinessArea;
                        model.PersonalSubArea = userExist.PersonalSubArea;
                        model.CompanyCode = userExist.CompanyCode;
                        model.EmpType = userExist.EmpType;
                        model.NppAbsen = userExist.NppAbsen;
                        model.EmpStatus = userExist.EmpStatus;
                        model.DataSource = userExist.DataSource;
                        model.JmId = userExist.JmId;
                        model.ParentUserId = userExist.Id;
                    }
                }
                else
                {
                    model.UserName = vm.UserName;
                }
                IdentityResult result = _userManager.CreateAsync(model, password).Result;
                IdentityResult roles = _userManager.AddToRoleAsync(model, role.Name).Result;
                helperSvc.RolesFromUser(model.Id);
            }
            catch (Exception e)
            {
                return new ResponseViewModels<RegisterViewModelV2>
                {
                    StatusCode = 400,
                    Message = "Failed To Save Data",
                    Data = null
                };
            }
            return new ResponseViewModels<RegisterViewModelV2>
            {
                StatusCode = 201,
                Message = "Data Successfully Saved",
                Data = null
            };
        }
        public ResponseViewModels<UserViewModel> UploadImageUser(IFormFile ImageAttachment)
        {
            try
            {
                string id = GetUserId();
                var model = dbContext.Users.Where(a => a.Id == id).FirstOrDefault();
                var image = helperSvc.Upload(ImageAttachment, model);
                model.UrlImage = image.UrlImage;
                model.ImageFileName = image.FileName;
                model.UrlImageResize = image.UrlImageResize;
                model.ImageFileNameResize = image.FileNameResize;

                dbContext.Users.Update(model);
                dbContext.SaveChanges();

                return new ResponseViewModels<UserViewModel>
                {
                    StatusCode = 201,
                    Message = "Data Successfully Saved",
                    Data = mapper.Map<UserViewModel>(model)
                };
            }
            catch (Exception e)
            {
                return new ResponseViewModels<UserViewModel>
                {
                    StatusCode = 400,
                    Message = "Failed To Save Data",
                    Data = null
                };
            }
        }
        public ResponseViewModels<UserViewModel> DeleteUser(string id)
        {
            var model = dbContext.Users.Where(a => a.Id == id).FirstOrDefault();

            if (model == null)
            {

                return new ResponseViewModels<UserViewModel>
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }

            try
            {
                if (model != null)
                {
                    var logins = dbContext.UserLogins.Where(a => a.UserId == id).ToList();
                    if (logins != null)
                    {
                        foreach (var login in logins)
                        {
                            dbContext.Remove(login);
                        }
                    }
                    var rolesForUser = dbContext.UserRoles.Where(a => a.UserId == id).FirstOrDefault();
                    if (rolesForUser != null)
                    {
                        dbContext.Remove(rolesForUser);
                    }
                    dbContext.Remove(model);
                    dbContext.SaveChanges();
                }

                return new ResponseViewModels<UserViewModel>
                {
                    StatusCode = 200,
                    Message = "Data Successfully Deleted",
                    Data = null
                };
            }
            catch (Exception e)
            {
                return new ResponseViewModels<UserViewModel>
                {
                    StatusCode = 400,
                    Message = "Failed To Delete Data",
                    Data = null
                };
            }
        }

        public ResponseViewModels<UserViewModel> DeleteNonLdap()
        {
            var models = dbContext.Users.Where(a => a.Isldap == 2).ToList();
            if (models.Count() == 0)
            {

                return new ResponseViewModels<UserViewModel>
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }

            try
            {
                foreach (var item in models)
                {
                    var userAdd = dbContext.UserRolesAdditionals.Where(a => a.PejabatId == item.Id).FirstOrDefault();
                    dbContext.UserRolesAdditionals.Remove(userAdd);
                }
                dbContext.Users.RemoveRange(models);
                dbContext.SaveChanges();
                return new ResponseViewModels<UserViewModel>
                {
                    StatusCode = 200,
                    Message = "Data Successfully Deleted",
                    Data = null
                };
            }
            catch (Exception e)
            {
                return new ResponseViewModels<UserViewModel>
                {
                    StatusCode = 400,
                    Message = "Failed To Delete Data",
                    Data = null
                };
            }
        }
        public async Task<ResponseViewModels<UserViewModel>> GetUser(string id)
        {
            var vm = await helperSvc.GetUser(id);
            if (vm == null)
            {
                return new ResponseViewModels<UserViewModel>
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }
            ResponseViewModels<UserViewModel> response = new ResponseViewModels<UserViewModel>();
            response.StatusCode = 200;
            response.Message = "Data Retrieved";
            response.Data = vm;
            return response;
        }
        public async Task<ResponseViewModels<List<UsersViewModel>>> GetListUserById(List<string> ids)
        {
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;
            List<UsersViewModel> lvm = new List<UsersViewModel>();
            foreach (string item in ids)
            {
                UsersViewModel vm = new UsersViewModel();
                var uvm = dbContext.Users/*.Include("UserRoles")*/.Where(a => a.Id == item).FirstOrDefault();
                if (uvm != null)
                {
                    vm = mapper.Map<UsersViewModel>(uvm);
                    vm.PersonId = uvm.Id;
                    vm.UserId = uvm.Id;
                    vm.UserAdditionals = helperSvc.GetUserAdditional(item);
                    vm.Delegation = GetDelegation(uvm.Id);
                    vm.ActiveAdditionalRoles = helperSvc.GetActiveAdditionalRolesV2(item);
                    if (vm.Delegation != null)
                    {
                        vm.DelegationId = vm.Delegation.Id;
                    }
                    else
                    {
                        vm.DelegationId = null;
                    }
                    vm.UserPositionId = uvm.RecentPositionId.ToString();
                    vm.UserPosition = uvm.PositionName;
                    vm.Type = "User Main Role";
                    //vm.DelegatedFrom = helperSvc.GetDelegatedFrom(item);
                    if (uvm.IsGroup == true)
                    {
                        vm.GroupUnit = await GetGroupUnit(uvm.Id);
                    }
                    if (uvm.IsDelegation == true)
                    {
                        vm.UserAdditionals = helperSvc.GetUserAdditionalDelegation(item);
                    }
                    lvm.Add(vm);
                }
                else
                {
                    var delegation = dbContext.UserRolesAdditionals.Where(a => a.Id == item).FirstOrDefault();
                    if (delegation != null)
                    {
                        var userlogin = dbContext.Users.Where(a => a.Id == delegation.LoginUserPersonId).FirstOrDefault();
                        var delegationVm = new UsersViewModel();
                        delegationVm.Id = delegation.Id;
                        delegationVm.PersonId = delegation.TypeId;
                        delegationVm.UserId = delegation.LoginUserPersonId;
                        delegationVm.UserPositionId = userlogin.RecentPositionId.ToString();
                        delegationVm.UserPosition = userlogin.PositionName;
                        delegationVm.PersonName = ti.ToTitleCase(delegation.LoginUserPersonName.ToLower());
                        delegationVm.RecentPositionId = delegation.PejabatJabatanId;
                        delegationVm.PositionName = delegation.PejabatJabatanName;
                        delegationVm.UserAdditionals = helperSvc.GetUserAdditionalDelegation(delegation.TypeId);
                        delegationVm.ActiveAdditionalRoles = helperSvc.GetActiveAdditionalRolesV2(delegation.TypeId);
                        delegationVm.Type = "Delegation";
                        delegationVm.DelegationId = delegation.TypeId;
                        lvm.Add(delegationVm);
                    }
                }
            }
            return new ResponseViewModels<List<UsersViewModel>>
            {
                StatusCode = 200,
                Message = "Data Retrieved",
                Data = mapper.Map<List<UsersViewModel>>(lvm)
            };
        }
        public async Task<ResponseViewModels<List<UsersViewModel>>> GetListUserByPositionId(List<string> ids)
        {
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;
            List<UsersViewModel> lvm = new List<UsersViewModel>();
            foreach (string item in ids)
            {
                UsersViewModel vm = new UsersViewModel();
                var uvm = dbContext.Users/*.Include("UserRoles")*/.Where(a => a.IsActive == true && a.IsDeleted == false &&  a.RecentPositionId.ToString() == item).FirstOrDefault();
                if (uvm != null)
                {
                    vm = mapper.Map<UsersViewModel>(uvm);
                    vm.PersonId = uvm.Id;
                    vm.UserId = uvm.Id;
                    vm.UserAdditionals = helperSvc.GetUserAdditional(item);
                    vm.Delegation = GetDelegation(uvm.Id);
                    vm.ActiveAdditionalRoles = helperSvc.GetActiveAdditionalRolesV2(item);
                    if (vm.Delegation != null)
                    {
                        vm.DelegationId = vm.Delegation.Id;
                    }
                    else
                    {
                        vm.DelegationId = null;
                    }
                    vm.UserPositionId = uvm.RecentPositionId.ToString();
                    vm.UserPosition = uvm.PositionName;
                    vm.Type = "User Main Role";
                    //vm.DelegatedFrom = helperSvc.GetDelegatedFrom(item);
                    if (uvm.IsGroup == true)
                    {
                        vm.GroupUnit = await GetGroupUnit(uvm.Id);
                    }
                    if (uvm.IsDelegation == true)
                    {
                        vm.UserAdditionals = helperSvc.GetUserAdditionalDelegation(item);
                    }
                    lvm.Add(vm);
                }
                else
                {
                    var delegation = dbContext.UserRolesAdditionals.Where(a => a.Id == item).FirstOrDefault();
                    if (delegation != null)
                    {
                        var userlogin = dbContext.Users.Where(a => a.Id == delegation.LoginUserPersonId).FirstOrDefault();
                        var delegationVm = new UsersViewModel();
                        delegationVm.Id = delegation.Id;
                        delegationVm.PersonId = delegation.TypeId;
                        delegationVm.UserId = delegation.LoginUserPersonId;
                        delegationVm.UserPositionId = userlogin.RecentPositionId.ToString();
                        delegationVm.UserPosition = userlogin.PositionName;
                        delegationVm.PersonName = ti.ToTitleCase(delegation.LoginUserPersonName.ToLower());
                        delegationVm.RecentPositionId = delegation.PejabatJabatanId;
                        delegationVm.PositionName = delegation.PejabatJabatanName;
                        delegationVm.UserAdditionals = helperSvc.GetUserAdditionalDelegation(delegation.TypeId);
                        delegationVm.ActiveAdditionalRoles = helperSvc.GetActiveAdditionalRolesV2(delegation.TypeId);
                        delegationVm.Type = "Delegation";
                        delegationVm.DelegationId = delegation.TypeId;
                        lvm.Add(delegationVm);
                    }
                }
            }
            return new ResponseViewModels<List<UsersViewModel>>
            {
                StatusCode = 200,
                Message = "Data Retrieved",
                Data = mapper.Map<List<UsersViewModel>>(lvm)
            };
        }
        public async Task<ResponseViewModels<List<UsersMemberViewModel>>> GetListUserMemberById(List<string> ids)
        {
            List<UsersMemberViewModel> lvm = new List<UsersMemberViewModel>();
            foreach (string item in ids)
            {
                UsersMemberViewModel vm = new UsersMemberViewModel();
                var uvm = dbContext.Users.Where(a => a.Id == item).FirstOrDefault();
                if (uvm != null)
                {
                    vm = mapper.Map<UsersMemberViewModel>(uvm);
                    if (uvm.IsGroup == true)
                    {
                        vm.MemberList = await GetGroupUnitV2(uvm.Id);
                    }
                    lvm.Add(vm);
                }
            }
            return new ResponseViewModels<List<UsersMemberViewModel>>
            {
                StatusCode = 200,
                Message = "Data Retrieved",
                Data = mapper.Map<List<UsersMemberViewModel>>(lvm)
            };
        }
        public async Task<ResponseViewModels<UserViewModel>> GetUserByToken()
        {
            try
            {
                string id = GetUserId();
                var vm = await helperSvc.GetUser(id);
                if (vm == null)
                {
                    return new ResponseViewModels<UserViewModel>
                    {
                        StatusCode = 404,
                        Message = "Data Not Found",
                        Data = null
                    };
                }
                ResponseViewModels<UserViewModel> response = new ResponseViewModels<UserViewModel>();
                response.StatusCode = 200;
                response.Message = "Data Retrieved";
                response.Data = vm;
                return response;
            }
            catch (Exception e)
            {
                ResponseViewModels<UserViewModel> response = new ResponseViewModels<UserViewModel>();
                response.StatusCode = 400;
                response.Message = "Error " + e.Message + "  " + e.StackTrace;
                response.Data = null;
                return response;
            }

        }
        public void GetEmployee(string personName, string userName, string email, string password)
        {
            ApplicationUser model = new ApplicationUser();
            string connStr = _appSettings.ServerConnString;
            var sql = "SELECT * FROM public.jm_employee where employee_number = '" + userName + "'";
            using (NpgsqlConnection con = new NpgsqlConnection(connStr))
            {
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                con.Open();
                NpgsqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        TextInfo ti = CultureInfo.CurrentCulture.TextInfo;
                        model.UserName = userName;
                        model.PersonName = ti.ToTitleCase(personName.ToLower());
                        model.Email = email?.ToLower();
                        model.PersonNumber = Convert.ToInt64(reader["person_number"]);
                        model.EmployeeNumber = reader["employee_number"].ToString();
                        model.OriginalDateOfHire = Convert.ToDateTime(reader["original_date_of_hire"]);
                        model.ValidFrom = Convert.ToDateTime(reader["valid_from"]);
                        model.ValidTo = Convert.ToDateTime(reader["valid_to"]);
                        model.DateOfBirth = Convert.ToDateTime(reader["date_of_birth"]);
                        model.TownOfBirth = reader["town_of_birth"].ToString();
                        model.NationalIdentifier = reader["national_identifier"].ToString();
                        model.Sex = reader["sex"].ToString();
                        model.JobId = Convert.ToDouble(reader["job_id"]);
                        model.JobName = reader["job_name"].ToString();
                        model.JobType = reader["job_type"].ToString();
                        model.RecentPositionId = Convert.ToDouble(reader["recent_position_id"]);
                        model.PositionName = reader["position_name"].ToString().TrimStart();
                        model.PositionNameAlias = reader["position_name_alias"].ToString();
                        model.PositionNameAtasan = reader["position_name_atasan"].ToString();
                        model.NomorSk = reader["nomor_sk"].ToString();
                        model.TanggalSk = reader["tanggal_sk"].ToString();
                        model.TanggalEfektifSk = reader["tanggal_efektif_sk"].ToString();
                        model.OrganizationId = Convert.ToDouble(reader["organization_id"]);
                        model.OrgName = reader["org_name"].ToString();
                        model.OrgIdParent = Convert.ToDouble(reader["org_id_parent"]);
                        model.OrgParentName = reader["org_parent_name"].ToString();
                        model.OrgGrandParent = Convert.IsDBNull(reader["org_grand_parent"]) ? null : (double?)reader["org_grand_parent"];
                        model.OrgGrandParentName = reader["org_grand_parent_name"].ToString();
                        model.UnitKerja = reader["unit_kerja"].ToString();
                        model.LocationId = Convert.ToDouble(reader["location_id"]);
                        model.LocationName = reader["location_name"].ToString();
                        model.Golongan = reader["golongan"].ToString();
                        model.Baris = reader["baris"].ToString();
                        model.Ruang = reader["ruang"].ToString();
                        model.Npwp = reader["npwp"].ToString();
                        model.JenisNpwp = reader["jenis_npwp"].ToString();
                        model.StatusNpwp = reader["status_npwp"].ToString();
                        model.MaritalStatus = reader["marital_status"].ToString();
                        model.Religion = reader["religion"].ToString();
                        //model.Password = reader["password"].ToString();
                        model.Isldap = Convert.ToDouble(reader["isldap"]);
                        model.StatusSat = reader["status_sat"].ToString();
                        model.StatusSap = reader["status_sap"].ToString();
                        model.CreatedDate = DateTime.Now;
                        model.UpdatedBy = reader["last_updated_by"].ToString();
                        model.UpdatedDate = DateTime.Now;
                        model.MainRole = Convert.ToDouble(reader["main_role"]);
                        model.UnitKerjaId = Convert.ToInt64(reader["unit_kerja_id"]);
                        model.PayScaleArea = reader["pay_scale_area"].ToString();
                        model.UrlImage = reader["url_image"].ToString();
                        model.PersonNumberApprover = Convert.IsDBNull(reader["person_number_approver"]) ? null : (double?)reader["person_number_approver"];
                        model.EmployeeGroup = Convert.ToInt16(reader["employee_group"]);
                        model.CntSubOrd = Convert.IsDBNull(reader["cnt_sub_ord"]) ? null : (Int16?)reader["cnt_sub_ord"];
                        model.PersonalArea = Convert.ToInt16(reader["personal_area"]);
                        model.PositionIdAtasan = Convert.IsDBNull(reader["position_id_atasan"]) ? null : (Int32?)reader["position_id_atasan"];
                        model.PayrollGroupId = Convert.IsDBNull(reader["payroll_group_id"]) ? null : (Int32?)reader["payroll_group_id"];
                        model.EmployeeSubgroup = reader["employee_subgroup"].ToString();
                        model.BusinessArea = reader["business_area"].ToString();
                        model.PersonalSubArea = reader["personal_sub_area"].ToString();
                        model.CompanyCode = reader["company_code"].ToString();
                        model.EmpType = Convert.ToInt16(reader["emp_type"]);
                        model.NppAbsen = reader["npp_absen"].ToString();
                        model.EmpStatus = reader["emp_status"].ToString();
                        model.DataSource = reader["data_source"].ToString();
                        model.Level = helperSvc.GetLevelUser(model.Golongan);
                        model.BODLevel = helperSvc.GetBODLevelUser(model.Golongan);
                        model.JmId = Convert.ToDouble(reader["id"]);
                        model.RoleId = "f4a3bfb3-511c-40f3-8e26-b873561d4d81";
                        model.RoleName = "Public";
                        model.IsActive = true;
                        model.Init();
                        IdentityResult result = _userManager.CreateAsync(model, password).Result;
                        IdentityResult roles = _userManager.AddToRoleAsync(model, "Public").Result;
                        helperSvc.RolesFromUser(model.Id);
                    }
                }
                catch (Exception e)
                {

                }
            }
        }
        public async Task<List<SecretaryViewModels>> GetActiveSecretary()
        {
            List<SecretaryViewModels> list = new List<SecretaryViewModels>();
            var token = GetUserToken();
            string url = _apiEndpoint.ActiveSecretary;
            var client = new RestClient($"{url}");
            var req = new RestRequest(Method.GET);
            req.AddHeader("Authorization", $"Bearer {token}");
            var responseApi = await client.ExecuteAsync(req);
            var response = JsonConvert.DeserializeObject<ResponseViewModels<PaginatedList<SecretaryViewModels>>>(responseApi.Content);
            list = response.Data.Items;

            return list;
        }
        public void UpdateUser(string userName)
        {
            var model = dbContext.Users.Where(a => a.UserName == userName).FirstOrDefault();
            string connStr = _appSettings.ServerConnString;
            var sql = "SELECT * FROM public.jm_employee where employee_number = '" + userName + "'";
            using (NpgsqlConnection con = new NpgsqlConnection(connStr))
            {
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                con.Open();
                NpgsqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        TextInfo ti = CultureInfo.CurrentCulture.TextInfo;
                        model.PersonName = ti.ToTitleCase(model.PersonName.ToLower());
                        //model.Email = reader["email_address"].ToString().ToLower();
                        model.PersonNumber = Convert.ToInt64(reader["person_number"]);
                        model.EmployeeNumber = reader["employee_number"].ToString();
                        model.OriginalDateOfHire = Convert.ToDateTime(reader["original_date_of_hire"]);
                        model.ValidFrom = Convert.ToDateTime(reader["valid_from"]);
                        model.ValidTo = Convert.ToDateTime(reader["valid_to"]);
                        model.DateOfBirth = Convert.ToDateTime(reader["date_of_birth"]);
                        model.TownOfBirth = reader["town_of_birth"].ToString();
                        model.NationalIdentifier = reader["national_identifier"].ToString();
                        model.Sex = reader["sex"].ToString();
                        model.JobId = Convert.ToDouble(reader["job_id"]);
                        model.JobName = reader["job_name"].ToString();
                        model.JobType = reader["job_type"].ToString();
                        model.RecentPositionId = Convert.ToDouble(reader["recent_position_id"]);
                        model.PositionName = reader["position_name"].ToString().TrimStart();
                        model.PositionNameAlias = reader["position_name_alias"].ToString();
                        model.PositionNameAtasan = reader["position_name_atasan"].ToString();
                        model.NomorSk = reader["nomor_sk"].ToString();
                        model.TanggalSk = reader["tanggal_sk"].ToString();
                        model.TanggalEfektifSk = reader["tanggal_efektif_sk"].ToString();
                        model.OrganizationId = Convert.ToDouble(reader["organization_id"]);
                        model.OrgName = reader["org_name"].ToString();
                        model.OrgIdParent = Convert.ToDouble(reader["org_id_parent"]);
                        model.OrgParentName = reader["org_parent_name"].ToString();
                        model.OrgGrandParent = Convert.IsDBNull(reader["org_grand_parent"]) ? null : (double?)reader["org_grand_parent"];
                        model.OrgGrandParentName = reader["org_grand_parent_name"].ToString();
                        model.UnitKerja = reader["unit_kerja"].ToString();
                        model.LocationId = Convert.ToDouble(reader["location_id"]);
                        model.LocationName = reader["location_name"].ToString();
                        model.Golongan = reader["golongan"].ToString();
                        model.Baris = reader["baris"].ToString();
                        model.Ruang = reader["ruang"].ToString();
                        model.Npwp = reader["npwp"].ToString();
                        model.JenisNpwp = reader["jenis_npwp"].ToString();
                        model.StatusNpwp = reader["status_npwp"].ToString();
                        model.MaritalStatus = reader["marital_status"].ToString();
                        model.Religion = reader["religion"].ToString();
                        model.Password = reader["password"].ToString();
                        model.Isldap = Convert.ToDouble(reader["isldap"]);
                        model.StatusSat = reader["status_sat"].ToString();
                        model.StatusSap = reader["status_sap"].ToString();
                        //model.CreatedDate = Convert.ToDateTime(reader["created_date"]);
                        //model.UpdatedBy = reader["last_updated_by"].ToString();
                        //model.UpdatedDate = Convert.ToDateTime(reader["last_updated_date"]);
                        model.MainRole = Convert.ToDouble(reader["main_role"]);
                        model.UnitKerjaId = Convert.ToInt64(reader["unit_kerja_id"]);
                        model.PayScaleArea = reader["pay_scale_area"].ToString();
                        //model.UrlImage = reader["url_image"].ToString();
                        model.PersonNumberApprover = Convert.IsDBNull(reader["person_number_approver"]) ? null : (double?)reader["person_number_approver"];
                        model.EmployeeGroup = Convert.ToInt16(reader["employee_group"]);
                        model.CntSubOrd = Convert.IsDBNull(reader["cnt_sub_ord"]) ? null : (Int16?)reader["cnt_sub_ord"];
                        model.PersonalArea = Convert.ToInt16(reader["personal_area"]);
                        model.PositionIdAtasan = Convert.IsDBNull(reader["position_id_atasan"]) ? null : (Int32?)reader["position_id_atasan"];
                        model.PayrollGroupId = Convert.IsDBNull(reader["payroll_group_id"]) ? null : (Int32?)reader["payroll_group_id"];
                        model.EmployeeSubgroup = reader["employee_subgroup"].ToString();
                        model.BusinessArea = reader["business_area"].ToString();
                        model.PersonalSubArea = reader["personal_sub_area"].ToString();
                        model.CompanyCode = reader["company_code"].ToString();
                        model.EmpType = Convert.ToInt16(reader["emp_type"]);
                        model.NppAbsen = reader["npp_absen"].ToString();
                        model.EmpStatus = reader["emp_status"].ToString();
                        model.DataSource = reader["data_source"].ToString();
                        model.Level = helperSvc.GetLevelUser(model.Golongan);
                        model.BODLevel = helperSvc.GetBODLevelUser(model.Golongan);
                        model.JmId = Convert.ToDouble(reader["id"]);

                        dbContext.Users.Update(model);
                        dbContext.SaveChanges();
                        helperSvc.RolesFromUser(model.Id);
                    }
                }
                catch (Exception e)
                {

                }
            }
        }
        public List<UsersViewModel> GetListUserByLevelV2(FilterParameterViewModels filter)
        {
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;
            string id = GetUserId();
            var user = dbContext.Users.Where(a => a.Id == id).FirstOrDefault();
            var model = new List<UsersViewModel>();
            var query = dbContext.Users.Where(a => a.IsActive == true && a.RecentPositionId != null && a.RecentPositionId != 99999999 && a.IsDelegation != true).OrderBy(a => a.PersonName).ToList();
            if (filter.FirstChar != null)
            {
                query = query.Where(a => a.PersonName.ToLower().StartsWith(filter.FirstChar.ToLower())
                || a.PositionName.ToLower().StartsWith(filter.FirstChar.ToLower())).ToList();
            }
            foreach (var item in query)
            {
                var vm = new UsersViewModel();
                vm = mapper.Map<UsersViewModel>(item);
                vm.PersonName = ti.ToTitleCase(vm.PersonName.ToLower());
                vm.ActiveAdditionalRoles = helperSvc.GetActiveAdditionalRolesV2(item.Id);
                vm.UserAdditionals = helperSvc.GetUserAdditional(item.Id);
                model.Add(vm);
            }
            //var listuserdelegasi = GetDelegasiUser(filter.FirstChar);
            //model.AddRange(listuserdelegasi);
            //model = model.OrderBy(a => a.PersonName).ToList();

            return model;
        }
        public List<UsersViewModel> GetUserDelegate(int user1, FilterParameterViewModels filter)
        {
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;
            var model = new List<UsersViewModel>();
            var query = dbContext.Users.Where(a => (a.Level == user1 || a.Level == user1 + 1) && a.RecentPositionId != null && a.RecentPositionId != 99999999
            && a.ParentUserId == null && a.IsDelegation == null).OrderBy(a => a.PersonName).ToList();
            if (filter.FirstChar != null)
            {
                query = query.Where(a => a.PersonName.ToLower().StartsWith(filter.FirstChar.ToLower())
                || a.PositionName.ToLower().StartsWith(filter.FirstChar.ToLower())).ToList();
            }
            foreach (var item in query)
            {
                var user = new UsersViewModel();
                user = mapper.Map<UsersViewModel>(item);
                user.PersonName = ti.ToTitleCase(user.PersonName.ToLower());
                model.Add(user);
            }
            return model;
        }
        public List<UsersViewModel> GetUserDelegate2(int user1, int user2, FilterParameterViewModels filter)
        {
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;
            var model = new List<UsersViewModel>();
            if (user1 == user2)
            {
                return null;
            }
            else
            {
                var query = dbContext.Users.Where(a => a.Level == user2 && a.RecentPositionId != null && a.RecentPositionId != 99999999
                && a.ParentUserId == null && a.IsDelegation == null).OrderBy(a => a.PersonName).ToList();
                if (filter.FirstChar != null)
                {
                    query = query.Where(a => a.PersonName.ToLower().StartsWith(filter.FirstChar.ToLower()) || a.PositionName.ToLower().StartsWith(filter.FirstChar.ToLower())).ToList();
                }

                foreach (var item in query)
                {
                    var user = new UsersViewModel();
                    user = mapper.Map<UsersViewModel>(item);
                    user.PersonName = ti.ToTitleCase(user.PersonName.ToLower());
                    model.Add(user);
                }
            }
            return model;
        }
        public UserViewModel GetEmployeeByEmployeeNumber(string EmployeeNumber)
        {
            var model = new UserViewModel();
            var query = dbContext.Users.Where(a => a.EmployeeNumber == EmployeeNumber).FirstOrDefault();
            if (model != null)
            {
                model = mapper.Map<UserViewModel>(query);
            };
            return model;
        }
        public ResponseViewModels<UserViewModel> CreateUserGroup(string id, string userName, string personName)
        {
            try
            {
                TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                //string name = ti.ToTitleCase(personName.ToLower());
                //string username = username;
                var current = dbContext.Users.Where(a => a.Id == id).FirstOrDefault();
                if (current == null)
                {
                    var user = new ApplicationUser();
                    user.Id = id;
                    user.UserName = userName;
                    user.PersonName = personName;
                    user.IsActive = true;
                    user.IsGroup = true;
                    user.CreatedDate = DateTime.Now;
                    user.UpdatedDate = DateTime.Now;
                    string userPWD = "Welcome1!";
                    IdentityResult result = _userManager.CreateAsync(user, userPWD).Result;
                    dbContext.SaveChanges();
                }
                else
                {
                    current.PersonName = personName;
                    current.UpdatedDate = DateTime.Now;
                    dbContext.Users.Update(current);
                    dbContext.SaveChanges();
                }

            }
            catch (Exception e)
            {
                return new ResponseViewModels<UserViewModel>()
                {
                    StatusCode = 400,
                    Message = "Failed Save Data",
                    Data = null
                };
            }
            return new ResponseViewModels<UserViewModel>()
            {
                StatusCode = 200,
                Message = "Data Succesfully Saved",
                Data = null
            };
        }
        public ResponseViewModels<UserViewModel> DeleteUserGroup(string id)
        {
            try
            {
                var current = dbContext.Users.Where(a => a.Id == id).FirstOrDefault();
                if (current == null)
                {
                    return new ResponseViewModels<UserViewModel>()
                    {
                        StatusCode = 400,
                        Message = "data null",
                        Data = null
                    };
                }
                else
                {
                    current.IsActive = false;
                    current.IsDeleted = true;
                    current.UpdatedDate = DateTime.Now;
                    dbContext.Users.Update(current);
                    dbContext.SaveChanges();
                }

            }
            catch (Exception e)
            {
                return new ResponseViewModels<UserViewModel>()
                {
                    StatusCode = 400,
                    Message = "Failed Save Data",
                    Data = null
                };
            }
            return new ResponseViewModels<UserViewModel>()
            {
                StatusCode = 201,
                Message = "Data Succesfully Deleted",
                Data = null
            };
        }
        public ResponseViewModels<UserViewModel> RestoreUserGroup(string id)
        {
            try
            {
                var current = dbContext.Users.Where(a => a.Id == id).FirstOrDefault();
                if (current == null)
                {
                    return new ResponseViewModels<UserViewModel>()
                    {
                        StatusCode = 400,
                        Message = "data null",
                        Data = null
                    };
                }
                else
                {
                    current.IsActive = true;
                    current.IsDeleted = false;
                    current.UpdatedDate = DateTime.Now;
                    dbContext.Users.Update(current);
                    dbContext.SaveChanges();
                }
            }
            catch (Exception e)
            {
                return new ResponseViewModels<UserViewModel>()
                {
                    StatusCode = 400,
                    Message = "Failed Save Data",
                    Data = null
                };
            }
            return new ResponseViewModels<UserViewModel>()
            {
                StatusCode = 201,
                Message = "Data Succesfully Deleted",
                Data = null
            };
        }
        public async Task<GroupUnitUserViewModels> GetGroupUnit(string id)
        {
            GroupUnitUserViewModels groupUnitUserViewModels = new GroupUnitUserViewModels();
            List<UsersViewModel> listUser = new List<UsersViewModel>();
            var token = GetUserToken();
            string url = _apiEndpoint.GroupUnit + id;
            var client = new RestClient($"{url}");
            var req = new RestRequest(Method.GET);
            req.AddHeader("Authorization", $"Bearer {token}");
            var responseApi = await client.ExecuteAsync(req);
            var value = JObject.Parse(responseApi.Content);
            var item = value.GetValue("data");
            GroupUnitViewModels vm = new GroupUnitViewModels
            {
                GroupName = item["groupName"].ToString(),
                MemberId = item["memberId"].ToString()
            };
            //List<string> userlist = vm.MemberId.Split(",").ToList();
            //foreach (var uvm in userlist)
            //{
            //    UsersViewModel usvm = new UsersViewModel();
            //    var user = dbContext.Users.Where(a => a.Id == uvm).FirstOrDefault();
            //    usvm = mapper.Map<UsersViewModel>(user);
            //    //usvm.ActiveAdditionalRoles = GetActiveAdditionalRolesV2(user.Id);
            //    usvm.UserAdditionals = helperSvc.GetUserAdditional(user.Id);
            //    usvm.ActiveAdditionalRoles = helperSvc.GetActiveAdditionalRolesV2(user.Id);
            //    listUser.Add(usvm);
            //}

            groupUnitUserViewModels.GroupName = vm.GroupName;
            groupUnitUserViewModels.Description = vm.Description;
            groupUnitUserViewModels.memberList = listUser;

            return groupUnitUserViewModels;
        }

        public async Task<List<UserMemberDetailViewModel>> GetGroupUnitV2(string id)
        {
            //GroupUnitUserMemberViewModels groupUnitUserViewModels = new GroupUnitUserMemberViewModels();
            List<UserMemberDetailViewModel> listUser = new List<UserMemberDetailViewModel>();
            var token = GetUserToken();
            string url = _apiEndpoint.GroupUnit + id;
            var client = new RestClient($"{url}");
            var req = new RestRequest(Method.GET);
            req.AddHeader("Authorization", $"Bearer {token}");
            var responseApi = await client.ExecuteAsync(req);
            var value = JObject.Parse(responseApi.Content);
            var item = value.GetValue("data");
            GroupUnitViewModels vm = new GroupUnitViewModels
            {
                GroupName = item["groupName"].ToString(),
                MemberId = item["memberId"].ToString()
            };
            List<string> userlist = vm.MemberId.Split(",").ToList();
            foreach (var uvm in userlist)
            {
                UserMemberDetailViewModel usvm = new UserMemberDetailViewModel();
                var user = dbContext.Users.Where(a => a.Id == uvm).FirstOrDefault();
                usvm.Id = user.Id;
                usvm.PersonName = user.PersonName;
                usvm.PositionName = user.PositionName;
                usvm.RecentPositionId = user.RecentPositionId.ToString();
                usvm.Delegation = GetDelegation(user.Id);
                listUser.Add(usvm);
            }

            return listUser;
        }
        public ResponseViewModels<List<UsersViewModel>> GetListUserByUnitKerja(string unitKerja)
        {
            var model = new List<UsersViewModel>();
            var query = dbContext.Users.Where(a => a.UnitKerja == unitKerja || a.IsUnitKearsipan == true).ToList();
            if (query.Count == 0)
            {
                return new ResponseViewModels<List<UsersViewModel>>()
                {
                    StatusCode = 201,
                    Message = "No Data Retrieved",
                    Data = null
                };
            }
            model = mapper.Map<List<UsersViewModel>>(query).ToList();

            return new ResponseViewModels<List<UsersViewModel>>()
            {
                StatusCode = 201,
                Message = "Data Retrieved",
                Data = model
            };
        }
        public async Task<ResponseViewModels<ResetPasswordViewModel>> ResetPassword(ResetPasswordViewModel model)
        {
            string userId = GetUserId();
            var user = await _userManager.FindByIdAsync(userId);
            if (!string.IsNullOrEmpty(model.UserName))
            {
                user = await _userManager.FindByNameAsync(model.UserName);
            }

            var checkOldPassword = await _userManager.CheckPasswordAsync(user, model.OldPassword);
            if (!checkOldPassword)
            {
                return new ResponseViewModels<ResetPasswordViewModel>()
                {
                    StatusCode = 400,
                    Message = "Password Lama yang Anda masukkan tidak sesuai",
                    Data = null
                };
            }

            if (model.NewPassword == model.ConfirmNewPassword)
            {
                try
                {
                    {
                        var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                        var result = await _userManager.ResetPasswordAsync(user, token, model.NewPassword);
                    }
                }

                catch (Exception e)
                {
                    return new ResponseViewModels<ResetPasswordViewModel>()
                    {
                        StatusCode = 400,
                        Message = "Error Change Password",
                        Data = null
                    };
                }
            }
            else
            {
                return new ResponseViewModels<ResetPasswordViewModel>()
                {
                    StatusCode = 400,
                    Message = "Password Confirmation yang Anda masukkan tidak sesuai",
                    Data = null
                };
            }

            return new ResponseViewModels<ResetPasswordViewModel>()
            {
                StatusCode = 200,
                Message = "Reset Password Succesfully",
                Data = null
            };
        }
        public ResponseViewModels<string> ForgetPassword(string userEmail)
        {
            string id = GetUserId();
            var user = dbContext.Users.FirstOrDefault(a => a.Id.Equals(id));
            try
            {
                // create email message
                string emailBody = "Hello " + user.PersonName + "," + " <br /> " +
                                    " <br /> " +
                                    "Anda telah meminta untuk mengatur ulang kata sandi Anda dengan username : " + user.UserName + "." + " <br /> " +
                                    "Untuk mengatur ulang kata sandi Anda, silakan klik tombol dibawah ini untuk memasukkan kata sandi baru Anda." + " <br /> " +
                                    " <br /> " +
                                    "<a href=" + _appSettings.DirectLink + ">Reset Password</a>";
                string senderEmail = _appSettings.DefaultSender;
                var email = new MimeMessage();
                email.From.Add(MailboxAddress.Parse(senderEmail));
                email.To.Add(MailboxAddress.Parse(userEmail));
                email.Subject = "Reset Password";
                email.Body = new TextPart(TextFormat.Html) { Text = emailBody };
                // send email
                using (var smtp = new SmtpClient())
                {
                    smtp.Connect(_appSettings.Host, _appSettings.Port, _appSettings.UseSsl);
                    smtp.Send(email);
                    smtp.Disconnect(true);
                }

            }
            catch (Exception e)
            {
                return new ResponseViewModels<string>()
                {
                    StatusCode = 400,
                    Message = "Failed send Email" + e.Message.ToString() + "" + e.StackTrace.ToString(),
                    Data = null
                };
            }
            return new ResponseViewModels<string>()
            {
                StatusCode = 200,
                Message = "Send Email Succesfully",
                Data = null
            };
        }
        public ResponseViewModels<List<UserJMViewModel>> GetListUserByADAccount(List<string> adAccounts)
        {
            List<UserJMViewModel> lvm = new List<UserJMViewModel>();
            foreach (string item in adAccounts)
            {
                UserJMViewModel vm = new UserJMViewModel();
                var uvm = dbContext.Users.Where(a => a.UserName == item).FirstOrDefault();
                if (uvm != null)
                {
                    vm = mapper.Map<UserJMViewModel>(uvm);
                    lvm.Add(vm);
                }
            }
            return new ResponseViewModels<List<UserJMViewModel>>
            {
                StatusCode = 200,
                Message = "Data Retrieved",
                Data = mapper.Map<List<UserJMViewModel>>(lvm)
            };
        }
        public ResponseViewModels<List<UserJMViewModel>> GetListUserByADAccountV2()
        {
            List<UserJMViewModel> lvm = new List<UserJMViewModel>();

            var uvm = dbContext.Users.Where(a => a.Isldap != 2 && a.IsDelegation != true).ToList();
            if (uvm.Count > 0)
            {
                lvm = mapper.Map<List<UserJMViewModel>>(uvm);
            }
            return new ResponseViewModels<List<UserJMViewModel>>
            {
                StatusCode = 200,
                Message = "Data Retrieved",
                Data = mapper.Map<List<UserJMViewModel>>(lvm)
            };
        }

        public DelegationViewModel GetDelegation(string id)
        {
            DateTime? start = null;
            DateTime? end = null;
            var vm = new DelegationViewModel();
            //string id = GetUserId();
            var list = dbContext.UserRolesAdditionals.Where(a => a.DelegationUserId == id
            && a.Type.ToLower() == "Delegation".ToLower() && a.IsDeleted == false && a.Order == 1).ToList();
            if (list.Count() == 0)
            {
                return null;
            }
            foreach (var item in list)
            {
                if (item.DelegationStartDate != null)
                {
                    start = item.DelegationStartDate.Value.StartOfDay();
                }
                if (item.DelegationEndDate != null)
                {
                    end = item.DelegationEndDate.Value.EndOfDay();
                }
                if (DateTime.Now >= start && DateTime.Now <= end)
                {
                    var user = dbContext.Users.Where(a => a.Id == item.PejabatId).FirstOrDefault();
                    var pgs = dbContext.Users.Where(a => a.Id == item.DelegationUserId).FirstOrDefault();

                    vm.Id = user.Id;
                    vm.PersonName = item.LoginUserPersonName;
                    vm.RecentPositionId = user.RecentPositionId;
                    vm.PositionName = user.PositionName;
                }
                //if (DateTime.Now > end || DateTime.Now < start)
                //{
                //    return null;
                //}
            }
            if (vm.Id == null)
            {
                return null;
            }
            return vm;
        }
        #endregion


    }
}
