﻿using AutoMapper;
using Jasamarga.DA.UserManagement.Databases;
using Jasamarga.DA.UserManagement.Databases.Models;
using Jasamarga.DA.UserManagement.Services.Contracts;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Services.Repositories
{
    public class PermissionServices : BaseServices<PermissionViewModels, Permission>, IPermissionServices
    {
        public PermissionServices(IMapper mapper, ApplicationDbContext dbContext, IHttpContextAccessor accessor) : base(mapper, dbContext, accessor)
        {

        }

        public ResponseViewModels<PermissionViewModels> Delete(string id)
        {
            var model = dbContext.Set<Permission>().Where(a=> a.Id == id).FirstOrDefault();

            if (model == null)
            {

                return new ResponseViewModels<PermissionViewModels>
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }

            try
            {
                if (model != null)
                {
                    //var listAuth = dbContext.ApplicationRoleActionAuthorizations.Where(a => a.PermissionId == id).ToList();
                    //foreach (var item in listAuth)
                    //{
                    //    dbContext.ApplicationRoleActionAuthorizations.Remove(item);
                    //}
                    model.IsDeleted = true;
                    dbContext.Set<Permission>().Update(model);
                    dbContext.SaveChanges();
                }

                return new ResponseViewModels<PermissionViewModels>
                {
                    StatusCode = 200,
                    Message = "Data Successfully Deleted",
                    Data = null
                };
            }
            catch (Exception e)
            {
                return new ResponseViewModels<PermissionViewModels>
                {
                    StatusCode = 400,
                    Message = "Failed To Delete Data",
                    Data = null
                };
            }
        }
    }
}