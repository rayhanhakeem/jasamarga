﻿using AutoMapper;
using Jasamarga.DA.UserManagement.Databases;
using Jasamarga.DA.UserManagement.Databases.Models;
using Jasamarga.DA.UserManagement.Services.Contracts;
using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Services.Repositories
{
    public class BaseServices<T, U> : TokenServices, IBaseServices<T, U>
         where T : BaseViewModels
         where U : BaseModel

    {

        protected IMapper mapper { set; get; }
        protected ApplicationDbContext dbContext { set; get; }
        protected string includes { set; get; }
        public BaseServices(IMapper mapper, ApplicationDbContext dbContext, IHttpContextAccessor accessor) : base(accessor, dbContext)
        {
            this.mapper = mapper;
            this.dbContext = dbContext;
        }

        public virtual ResponseViewModels<PaginatedList<T>> GetList(T vm, int? page = 1, int? perPage = 10, string orderBy = null, string order = null, string searchField = null, string queryString = null, bool? IncludeDeleted = null)
        {
            var response = new ResponseViewModels<PaginatedList<T>>();
            var query = dbContext.Set<U>().AsQueryable();
            if (IncludeDeleted == false || IncludeDeleted == null)
            {
                query = query.Where(a => a.IsDeleted == false);
            }
            if (IncludeDeleted == true)
            {
                query = query.Where(a => a.IsDeleted == true || a.IsDeleted == false);
            }
            if (!string.IsNullOrEmpty(queryString))
            {
                if (!string.IsNullOrEmpty(searchField))
                {
                    query = query.Where($"{searchField}.ToLower().Contains(@0)", queryString);
                }
                else
                {
                    string searchTemplate = "";
                    var fields = vm.GetSearchField();
                    foreach (var field in fields)
                    {
                        searchTemplate = string.IsNullOrEmpty(searchTemplate) ? $"{field}.ToLower().Contains(@0)" : $"{searchTemplate} or {field}.ToLower().Contains(@0)";
                    }

                    query = query.Where(searchTemplate, queryString);
                }
            }

            if (!string.IsNullOrEmpty(includes))
            {
                var includeList = includes.Split(',');
                foreach (var inc in includeList)
                {
                    query = query.Include(inc.Trim());
                }
            }

            var data = new List<T>();
            if (!string.IsNullOrEmpty(orderBy) && vm.GetProperties().Contains(orderBy))
            {
                data = mapper.Map<List<T>>(query.OrderBy(orderBy + " " + (order ?? "ascending")).ToDynamicList());
            }
            else
            {
                data = mapper.Map<List<T>>(query.OrderBy("CreatedDate " + (order ?? "ascending")).ToDynamicList());
            }

            if (data.Count == 0)
            {
                response.StatusCode = 204;
                response.Message = "No Data Retrieved";
            }
            else
            {
                response.StatusCode = 200;
                response.Message = "Data Retrieved";
            }

            response.Data = new PaginatedList<T>(data, data.Count, page ?? 1, perPage ?? 10);
            return response;
        }

        public ResponseViewModels<T> Store(T vm)
        {
            try
            {
                var model = mapper.Map<U>(vm);
                model.CreatedBy = GetPersonName();
                model.SetCreated();
                model.UpdatedDate = DateTime.Now;
                dbContext.Add(model);
                dbContext.SaveChanges();
                return new ResponseViewModels<T>
                {
                    StatusCode = 201,
                    Message = "Data Successfully Saved",
                    Data = mapper.Map<T>(model)
                };
            }
            catch (Exception e)
            {
                return new ResponseViewModels<T>
                {
                    StatusCode = 400,
                    Message = "Failed To Save Data",
                    Data = null
                };
            }
        }

        public ResponseViewModels<T> Update(T vm)
        {
            var model = dbContext.Set<U>().Where("Id == @0", vm.Id).FirstOrDefault();
            if (model == null)
            {
                return new ResponseViewModels<T>
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }

            try
            {
                if (model != null)
                {
                    vm.CreatedDate = model.CreatedDate;
                    model = mapper.Map(vm, model);
                    model.SetUpdated();
                    dbContext.SaveChanges();
                }

                return new ResponseViewModels<T>
                {
                    StatusCode = 201,
                    Message = "Data Successfully Saved",
                    Data = mapper.Map<T>(model)
                };
            }
            catch (Exception e)
            {
                return new ResponseViewModels<T>
                {
                    StatusCode = 400,
                    Message = "Failed To Save Data",
                    Data = null
                };
            }
        }

        public ResponseViewModels<T> Delete(string id)
        {
            var model = dbContext.Set<U>().Where("Id == @0", id).FirstOrDefault();
            if (model == null)
            {
                return new ResponseViewModels<T>
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }
            try
            {
                if (model != null)
                {
                    model.IsDeleted = true;
                    dbContext.Set<U>().Update(model);
                    dbContext.SaveChanges();
                }
                return new ResponseViewModels<T>
                {
                    StatusCode = 200,
                    Message = "Data Successfully Deleted",
                    Data = null
                };
            }
            catch (Exception e)
            {
                return new ResponseViewModels<T>
                {
                    StatusCode = 400,
                    Message = "Failed To Delete Data",
                    Data = null
                };
            }
        }

        public ResponseViewModels<T> Detail(string id, string includes = null)
        {
            var query = dbContext.Set<U>().AsQueryable().Where("Id == @0", id);
            if (!string.IsNullOrEmpty(includes))
            {
                var includeList = includes.Split(',');
                query = query.Include(includes.Trim());
            }

            var data = query.FirstOrDefault();
            if (data == null)
            {
                return new ResponseViewModels<T>
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }

            return new ResponseViewModels<T>
            {
                StatusCode = 200,
                Message = "Data Retrieved",
                Data = mapper.Map<T>(data)
            };
        }

        public ResponseViewModels<T> Restore(string id)
        {
            var model = dbContext.Set<U>().Where("Id == @0", id).FirstOrDefault();
            if (model == null)
            {
                return new ResponseViewModels<T>
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }
            try
            {
                if (model != null)
                {
                    model.IsDeleted = false;
                    dbContext.Set<U>().Update(model);
                    dbContext.SaveChanges();
                }
                return new ResponseViewModels<T>
                {
                    StatusCode = 200,
                    Message = "Data Successfully Restored",
                    Data = null
                };
            }
            catch (Exception e)
            {
                return new ResponseViewModels<T>
                {
                    StatusCode = 400,
                    Message = "Failed To Delete Data",
                    Data = null
                };
            }
        }
    }
}
