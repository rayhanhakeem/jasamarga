﻿using AutoMapper;
using Jasamarga.DA.UserManagement.Databases;
using Jasamarga.DA.UserManagement.Databases.Identity;
using Jasamarga.DA.UserManagement.Databases.Models;
using Jasamarga.DA.UserManagement.Services.Contracts;
using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using Npgsql;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using MimeKit;
using MimeKit.Text;
using MailKit.Net.Smtp;

namespace Jasamarga.DA.UserManagement.Services.Repositories
{
    public class HelperServices : TokenServices, IHelperServices
    {

        protected IMapper mapper { set; get; }
        protected ApplicationDbContext dbContext { set; get; }
        protected string includes { set; get; }
        private UserManager<ApplicationUser> _userManager;
        private RoleManager<ApplicationRole> _roleManager;
        private readonly AppSettings _appSettings;
        private readonly ApiEndpoint _apiEndpoint;
        public static string DirName = "User";
        private IHttpContextAccessor accessor;

        public HelperServices(IMapper mapper,
            ApplicationDbContext dbContext,
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            IOptions<AppSettings> appSettings,
            IOptions<ApiEndpoint> apiEndpoint,
            IHttpContextAccessor accessor) : base(accessor, dbContext)
        {
            this.mapper = mapper;
            this.dbContext = dbContext;
            this._userManager = userManager;
            this._roleManager = roleManager;
            this._appSettings = appSettings.Value;
            this._apiEndpoint = apiEndpoint.Value;
            this.accessor = accessor;
        }
        public async Task<UserViewModel> GetUser(string id)
        {
            var user = dbContext.Users.Where(a => a.Id == id).FirstOrDefault();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;
            // return null if no user found with token
            if (user == null)
            {
                return null;
            }
            UserViewModel vm = new UserViewModel();
            //user.UrlImage = user.UrlImageResize;
            vm = mapper.Map<UserViewModel>(user);
            vm.PersonName = ti.ToTitleCase(vm.PersonName.ToLower());
            //var additionalRoles = dbContext.UserRolesAdditionals.Where(a => a.LoginUserPersonId == id || a.ParentUserId == id || a.LoginUserPersonId == ).ToList();
            vm.ActiveAdditionalRoles = GetActiveAdditionalRoles(id);
            vm.ListRoles = GetListUserRolesAdditionalsV2(id);
            vm.PrivilegedMainRole = GetListRoles(id);
            vm.ReceiverDelegation = GetDelegationReceiver(id);
            vm.ReceiverSecretary = GetSecretaryReceiver(id);
            vm.UserAdditionals = GetUserAdditional(id);
            if (vm.ActiveAdditionalRoles == null)
            {
                vm.Type = "User Main Role";
            }
            else
            {
                vm.Type = vm.ActiveAdditionalRoles.Type;
            }
            string userId = id;
            if (vm.ActiveAdditionalRoles != null)
            {
                userId = vm.ActiveAdditionalRoles.TypeId;
            }
            vm.DashboardIsUnitKearsipan = user.IsUnitKearsipan;
            if (vm.ReceiverDelegation.Count() > 0)
            {
                vm.DelegationId = vm.ReceiverDelegation?.FirstOrDefault().TypeId;
            }
            else
            {
                vm.DelegationId = null;
            }
            
            vm.UnreadNotifications = await GetNotificationList(userId);
            return vm;
        }
        public void DefaultRoles(string userId)
        {
            var userRoles = dbContext.UserRolesAdditionals.Where(a => a.LoginUserPersonId == userId).ToList();
            foreach (var item in userRoles)
            {
                if (item.Type == "User Main Role")
                {
                    item.isActive = true;
                    dbContext.UserRolesAdditionals.Update(item);
                }
                else
                {
                    item.isActive = false;
                    dbContext.UserRolesAdditionals.Update(item);
                }
                dbContext.SaveChanges();
            }
        }
        public void DefaultRolesV2(string userId)
        {
            //var user = dbContext.Users.Where(a => a.Id == userId).FirstOrDefault();
            var listModel = dbContext.UserRolesAdditionals.Where(a => a.ParentUserId == userId && a.IsDeleted == false).ToList();

            foreach (var item in listModel)
            {
                if (item.Type == "User Main Role" && item.ParentUserId == item.LoginUserPersonId)
                {
                    item.isActive = true;
                    dbContext.UserRolesAdditionals.Update(item);
                }
                else
                {
                    item.isActive = false;
                    dbContext.UserRolesAdditionals.Update(item);
                }
                dbContext.SaveChanges();
            }
        }
        public ResponseViewModels<List<GolonganViewModel>> GetListGolongan()
        {
            var model = new List<GolonganViewModel>()
                {
                    new GolonganViewModel { Golongan = "ALL"},new GolonganViewModel { Golongan = "K"},new GolonganViewModel { Golongan = "D"},
                    new GolonganViewModel { Golongan = "E"},new GolonganViewModel { Golongan = "F"},new GolonganViewModel { Golongan = "1"},
                    new GolonganViewModel { Golongan = "2"},new GolonganViewModel { Golongan = "3"},new GolonganViewModel { Golongan = "4"},
                };
            return new ResponseViewModels<List<GolonganViewModel>>()
            {
                StatusCode = 200,
                Message = "Data Retrieved",
                Data = model
            };
        }
        public ImageViewModel Upload(IFormFile ImageAttachment, ApplicationUser user)
        {
            ImageViewModel vm = new ImageViewModel();
            var dirPath = Path.Combine(Directory.GetCurrentDirectory(), _appSettings.UploadPath, DirName);
            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }
            var fileName = Path.Combine(dirPath, ImageAttachment.FileName);
            vm.FileName = ImageAttachment.FileName;
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            {
                ImageAttachment.CopyTo(fileStream);
            }

            using (Image image = Image.FromFile(fileName))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    image.Save(m, image.RawFormat);
                    byte[] imageBytes = m.ToArray();
                    string mimetype = GetContentType(fileName);
                    string base64String = "data:" + mimetype + ";" + "base64," + Convert.ToBase64String(imageBytes);
                    vm.UrlImage = base64String;

                    Image resize = image.GetThumbnailImage(50, 50, () => false, IntPtr.Zero);
                    resize.Save(Path.ChangeExtension(fileName, "thumb"));
                    string ext = Path.GetExtension(fileName);
                    var file = vm.FileName.Replace(ext, "");
                    var fileName2 = Path.Combine(dirPath, file + ".thumb");
                    vm.FileNameResize = file[0] + ".thumb";
                    using (Image image2 = Image.FromFile(fileName2))
                    {
                        using (MemoryStream m2 = new MemoryStream())
                        {
                            image2.Save(m2, image2.RawFormat);
                            byte[] imageBytesResize = m2.ToArray();
                            string base64StringResize = "data:" + "application/octet-stream" + ";" + "base64," + Convert.ToBase64String(imageBytesResize);
                            vm.UrlImageResize = base64StringResize;
                        }
                    }
                }
            }
            return vm;
        }
        public string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }
        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }
        public UserViewModel GetPersonName(string id)
        {
            var model = dbContext.Users.Include("UserRoles").Where(a => a.Id == id).FirstOrDefault();

            if (model == null)
            {

                return new UserViewModel
                {
                    PersonName = null
                };
            }

            return new UserViewModel
            {
                PersonName = model.PersonName
            };
        }
        public ResponseViewModels<UserViewModel> GetUserByEmail(string email)
        {
            var model = dbContext.Users.Where(a => a.Email == email).FirstOrDefault();
            if (model == null)
            {

                return new ResponseViewModels<UserViewModel>
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }
            UserViewModel uvm = new UserViewModel();
            uvm = mapper.Map<UserViewModel>(model);

            return new ResponseViewModels<UserViewModel>
            {
                StatusCode = 200,
                Message = "Data Retrieved",
                Data = uvm
            };
        }
        public int GetLevelUser(string golongan)
        {
            int level = 0;
            if (golongan == "ALL" || golongan == "K")
            {
                level = 1;
            }
            //else if (golongan == "K")
            //{
            //    level = 1;
            //}
            //else if (golongan == "D")
            //{
            //    level = 2;
            //}
            //else if (golongan == "E")
            //{
            //    level = 2;
            //}
            else if (golongan == "BOD-1" || golongan == "D" || golongan == "E" || golongan == "F")
            {
                level = 2;
            }
            else if (golongan == "1" || golongan == "BOD-2")
            {
                level = 3;
            }
            else if (golongan == "2" || golongan == "BOD-3")
            {
                level = 4;
            }
            else if (golongan == "3" || golongan == "BOD-4")
            {
                level = 5;
            }
            else if (golongan == "4" || golongan == "BOD-5")
            {
                level = 6;
            }
            else if (golongan == "5" || golongan == "BOD-6")
            {
                level = 6;
            }
            else
            {
                level = 7;
            }
            return level;
        }
        public string GetBODLevelUser(string golongan)
        {
            string level = string.Empty;
            if (golongan == "ALL" || golongan == "K")
            {
                level = "BOD";
            }
            //else if (golongan == "K")
            //{
            //    level = "BOD";
            //}
            //else if (golongan == "D")
            //{
            //    level = "BOD-1";
            //}
            //else if (golongan == "E")
            //{
            //    level = "BOD-1";
            //}
            //else if (golongan == "F")
            //{
            //    level = "BOD-1";
            //}
            else if (golongan == "BOD-1" || golongan == "D" || golongan == "E" || golongan == "F")
            {
                level = "BOD-1";
            }
            else if (golongan == "1" || golongan == "BOD-2")
            {
                level = "BOD-2";
            }
            else if (golongan == "2" || golongan == "BOD-3")
            {
                level = "BOD-3";
            }
            else if (golongan == "3" || golongan == "BOD-4")
            {
                level = "BOD-4";
            }
            else if (golongan == "4" || golongan == "BOD-5")
            {
                level = "BOD-5";
            }
            else if (golongan == "5" || golongan == "BOD-6")
            {
                level = "BOD-6";
            }
            else
            {
                level = "BOD-7";
            }
            return level;
        }
        public UserRolesAdditionalViewModels GetActiveAdditionalRoles(string userId)
        {
            string id = userId;
            UserRolesAdditionalViewModels usvm = new UserRolesAdditionalViewModels();
            var vm = dbContext.UserRolesAdditionals.Where(a => a.LoginUserPersonId == id && a.isActive == true).FirstOrDefault();

            if (vm == null || vm.Type == "User Main Role")
            {
                return null;
            }
            else
            {
                var user = dbContext.Users.Where(a => a.Id == vm.PejabatId).FirstOrDefault();
                usvm = new UserRolesAdditionalViewModels()
                {
                    Id = vm.Id,
                    LoginUserPersonId = vm.LoginUserPersonId,
                    LoginUserUnitKerja = vm.LoginUserUnitKerja,
                    LoginUserUnitKerjaId = vm.LoginUserUnitKerjaId,
                    PejabatRoleName = vm.PejabatRoleName,
                    PejabatId = vm.PejabatId,
                    PejabatName = vm.PejabatName,
                    PejabatJabatanId = vm.PejabatJabatanId,
                    PejabatJabatanName = vm.PejabatJabatanName,
                    PejabatUnitKerjaName = user.UnitKerja,
                    PejabatBODLEvel = vm.PejabatBODLEvel,
                    isActive = vm.isActive,
                    DelegationStartDate = vm.DelegationStartDate,
                    DelegationEndDate = vm.DelegationEndDate,
                    IsUnitKearsipan = user.IsUnitKearsipan,
                    PrivilegedList = GetListRoles(vm.PejabatId),
                    Type = vm.Type,
                    TypeId = vm.TypeId
                };
                if (vm.PejabatJabatanName.Contains("Skr."))
                {
                    usvm.DelegationType = "Skr";
                }
                else if (vm.PejabatJabatanName.Contains("Pgs."))
                {
                    usvm.DelegationType = "Pgs";
                }
            }
            return usvm;

        }
        public UserRolesAdditionalViewModels GetActiveAdditionalRolesV2(string id)
        {
            UserRolesAdditionalViewModels usvm = new UserRolesAdditionalViewModels();
            var vm = dbContext.UserRolesAdditionals.Where(a => a.LoginUserPersonId == id && a.isActive == true).FirstOrDefault();

            if (vm == null)
            {
                return null;
            }

            else if (vm.LoginUserPersonId == vm.PejabatId)
            {
                return null;
            }
            else
            {
                var user = dbContext.Users.Where(a => a.Id == vm.PejabatId).FirstOrDefault();
                usvm = new UserRolesAdditionalViewModels()
                {
                    Id = vm.Id,
                    LoginUserPersonId = vm.LoginUserPersonId,
                    LoginUserUnitKerja = vm.LoginUserUnitKerja,
                    LoginUserUnitKerjaId = vm.LoginUserUnitKerjaId,
                    PejabatRoleName = vm.PejabatRoleName,
                    PejabatId = vm.PejabatId,
                    PejabatName = vm.PejabatName,
                    PejabatJabatanId = vm.PejabatJabatanId,
                    PejabatJabatanName = vm.PejabatJabatanName,
                    PejabatBODLEvel = vm.PejabatJabatanName,
                    isActive = vm.isActive,
                    DelegationStartDate = vm.DelegationStartDate,
                    DelegationEndDate = vm.DelegationEndDate,
                    IsUnitKearsipan = user.IsUnitKearsipan,
                    Type = vm.Type,
                    TypeId = vm.TypeId,
                    PrivilegedList = GetListRoles(vm.PejabatId),
                };
                if (vm.PejabatJabatanName != null)
                {
                    if (vm.PejabatJabatanName.Contains("Skr."))
                    {
                        usvm.DelegationType = "Skr";
                    }
                    else if (vm.PejabatJabatanName.Contains("Pgs."))
                    {
                        usvm.DelegationType = "Pgs";
                    }
                }
            }
            return usvm;

        }
        public List<string> GetListRoles(string id)
        {
            var model = dbContext.Users.Include("UserRoles").Where(a => a.Id == id).FirstOrDefault();
            var action = dbContext.ApplicationRoleActionAuthorizations.Include("Permission").Where(a => a.ApplicationRoleId == model.RoleId).ToList();

            List<string> priviligedlist = new List<string>();
            foreach (var item in action)
            {
                if (item.forView == true)
                {
                    priviligedlist.Add(item.Permission.Url);
                }
                if (item.forCreate == true)
                {
                    priviligedlist.Add(item.Permission.Url + "/tambah");
                }
                if (item.forEdit == true)
                {
                    priviligedlist.Add(item.Permission.Url + "/edit");
                }
                if (item.forDelete == true)
                {
                    priviligedlist.Add(item.Permission.Url + "/delete");
                }
                if (item.forRestore == true)
                {
                    priviligedlist.Add(item.Permission.Url + "/restore");
                }
                if (item.Permission.Url.Contains("master-data") && item.forView == true)
                {
                    priviligedlist.Add("/master-data/kode-nomor-naskah");
                }
            }
            priviligedlist = priviligedlist.Distinct().ToList(); ;

            return priviligedlist;
        }
        public List<string> GetListRolesSecretary(string id)
        {
            var model = dbContext.Users.Include("UserRoles").Where(a => a.Id == id).FirstOrDefault();
            var action = dbContext.ApplicationRoleActionAuthorizations.Include("Permission").Where(a => a.ApplicationRoleId == model.RoleId).ToList();

            List<string> priviligedlist = new List<string>();
            foreach (var item in action)
            {
                if (item.forView == true)
                {
                    priviligedlist.Add(item.Permission.Url);
                }
            }
            priviligedlist.Add("/naskah-dinas/kelola-label/tambah");
            priviligedlist.Add("/naskah-dinas/kelola-label/edit");
            priviligedlist.Add("/naskah-dinas/kelola-label/delete");
            priviligedlist = priviligedlist.Distinct().ToList(); ;

            return priviligedlist;
        }
        public List<UserRolesAdditionalViewModels> GetListUserRolesAdditionals(string id)
        {
            var vm = dbContext.UserRolesAdditionals.Where(a => a.LoginUserPersonId == id).ToList();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;
            List<UserRolesAdditionalViewModels> listRoles = new List<UserRolesAdditionalViewModels>();
            foreach (var item in vm)
            {
                DateTime? start = null;
                DateTime? end = null;
                if (item.DelegationStartDate != null)
                {
                    start = item.DelegationStartDate.Value.StartOfDay();
                }
                if (item.DelegationEndDate != null)
                {
                    end = item.DelegationEndDate.Value.EndOfDay();
                }
                if (item.Type == "Delegation" && DateTime.Now >= start && DateTime.Now <= end && item.IsDeleted == false)
                {
                    UserRolesAdditionalViewModels uravm = new UserRolesAdditionalViewModels()
                    {
                        Id = item.Id,
                        LoginUserPersonId = item.LoginUserPersonId,
                        LoginUserUnitKerja = item.LoginUserUnitKerja,
                        LoginUserUnitKerjaId = item.LoginUserUnitKerjaId,
                        PejabatRoleName = item.PejabatRoleName,
                        PejabatId = item.PejabatId,
                        PejabatName = ti.ToTitleCase(item.PejabatName.ToLower()),
                        PejabatJabatanId = item.PejabatJabatanId,
                        PejabatJabatanName = item.PejabatJabatanName,
                        PejabatBODLEvel = item.PejabatBODLEvel,
                        isActive = item.isActive,
                        SecretaryActive = item.SecretaryActive,
                        DelegationStartDate = item.DelegationStartDate,
                        DelegationEndDate = item.DelegationEndDate,
                        PrivilegedList = GetListRoles(item.PejabatId),
                        Type = item.Type,
                        TypeId = item.TypeId
                    };
                    if (uravm.PejabatJabatanName != null)
                    {
                        if (uravm.PejabatJabatanName.Contains("Skr."))
                        {
                            uravm.DelegationType = "Skr";
                        }
                        else if (uravm.PejabatJabatanName.Contains("Pgs."))
                        {
                            uravm.DelegationType = "Pgs";
                        }
                    }
                    listRoles.Add(uravm);
                }
                else if (item.Type == "Secretary" && item.SecretaryActive == true && item.IsDeleted == false)
                {
                    UserRolesAdditionalViewModels uravm = new UserRolesAdditionalViewModels()
                    {
                        Id = item.Id,
                        LoginUserPersonId = item.LoginUserPersonId,
                        LoginUserUnitKerja = item.LoginUserUnitKerja,
                        LoginUserUnitKerjaId = item.LoginUserUnitKerjaId,
                        PejabatRoleName = item.PejabatRoleName,
                        PejabatId = item.PejabatId,
                        PejabatName = ti.ToTitleCase(item.PejabatName.ToLower()),
                        PejabatJabatanId = item.PejabatJabatanId,
                        PejabatJabatanName = item.PejabatJabatanName,
                        PejabatBODLEvel = item.PejabatBODLEvel,
                        isActive = item.isActive,
                        SecretaryActive = item.SecretaryActive,
                        DelegationStartDate = item.DelegationStartDate,
                        DelegationEndDate = item.DelegationEndDate,
                        PrivilegedList = GetListRoles(item.PejabatId),
                        Type = item.Type,
                        TypeId = item.TypeId
                    };
                    if (uravm.PejabatJabatanName != null)
                    {
                        if (uravm.PejabatJabatanName.Contains("Skr."))
                        {
                            uravm.DelegationType = "Skr";
                        }
                        else if (uravm.PejabatJabatanName.Contains("Pgs."))
                        {
                            uravm.DelegationType = "Pgs";
                        }
                    }
                    listRoles.Add(uravm);
                }
                else if (item.Type == "User Main Role")
                {
                    UserRolesAdditionalViewModels uravm = new UserRolesAdditionalViewModels()
                    {
                        Id = item.Id,
                        LoginUserPersonId = item.LoginUserPersonId,
                        LoginUserUnitKerja = item.LoginUserUnitKerja,
                        LoginUserUnitKerjaId = item.LoginUserUnitKerjaId,
                        PejabatRoleName = ti.ToTitleCase(item.PejabatRoleName.ToLower()),
                        PejabatId = item.PejabatId,
                        PejabatName = item.PejabatName,
                        PejabatJabatanId = item.PejabatJabatanId,
                        PejabatJabatanName = item.PejabatJabatanName,
                        PejabatBODLEvel = item.PejabatBODLEvel,
                        isActive = item.isActive,
                        SecretaryActive = item.SecretaryActive,
                        DelegationStartDate = item.DelegationStartDate,
                        DelegationEndDate = item.DelegationEndDate,
                        PrivilegedList = GetListRoles(item.PejabatId),
                        Type = item.Type,
                        TypeId = item.TypeId
                    };
                    if (uravm.PejabatJabatanName != null)
                    {
                        if (uravm.PejabatJabatanName.Contains("Skr."))
                        {
                            uravm.DelegationType = "Skr";
                        }
                        else if (uravm.PejabatJabatanName.Contains("Pgs."))
                        {
                            uravm.DelegationType = "Pgs";
                        }
                    }
                    listRoles.Add(uravm);
                }
            }
            return listRoles;
        }
        public List<UserRolesAdditionalViewModels> GetListUserRolesAdditionalsV2(string id)
        {
            string parentId = string.Empty;
            var user = dbContext.Users.Where(a => a.Id == id).FirstOrDefault();
            if (user.ParentUserId == null && (user.IsDelegation == null || user.IsDelegation == false))
            {
                parentId = user.Id;
            }
            else if (user.ParentUserId != null && (user.IsDelegation == null || user.IsDelegation == false))
            {
                parentId = user.ParentUserId;
            }
            else if (user.IsDelegation == true && user.ParentUserId == null)
            {
                var userAdditional = dbContext.UserRolesAdditionals.Where(a => a.LoginUserPersonId == id).FirstOrDefault();
                parentId = userAdditional.ParentUserId;
            }
            var listModel = dbContext.UserRolesAdditionals.Where(a => a.ParentUserId == parentId && a.IsDeleted == false).OrderBy(a => a.CreatedDate).ToList();
            //int countMainRole = listModel.Where(a => a.Type == "User Main Role").ToList().Count();
            //int countDelegation = listModel.Where(a => a.Type == "User Main Role").ToList().Count();
            //int countSecretary = listModel.Where(a => a.Type == "User Main Role").ToList().Count();
            //int countAll = listModel.ToList().Count();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;
            var listRoles = new List<UserRolesAdditionalViewModels>();
            foreach (var item in listModel)
            {
                DateTime? start = null;
                DateTime? end = null;
                if (item.DelegationStartDate != null)
                {
                    start = item.DelegationStartDate.Value.StartOfDay();
                }
                if (item.DelegationEndDate != null)
                {
                    end = item.DelegationEndDate.Value.EndOfDay();
                }
                if (item.Type == "Delegation" && DateTime.Now >= start && DateTime.Now <= end && item.IsDeleted == false)
                {
                    UserRolesAdditionalViewModels uravm = new UserRolesAdditionalViewModels()
                    {
                        Id = item.Id,
                        LoginUserPersonId = item.LoginUserPersonId,
                        LoginUserUnitKerja = item.LoginUserUnitKerja,
                        LoginUserUnitKerjaId = item.LoginUserUnitKerjaId,
                        PejabatRoleName = item.PejabatRoleName,
                        PejabatId = item.PejabatId,
                        PejabatName = ti.ToTitleCase(item.PejabatName.ToLower()),
                        PejabatJabatanId = item.PejabatJabatanId,
                        PejabatJabatanName = item.PejabatJabatanName,
                        PejabatBODLEvel = item.PejabatBODLEvel,
                        isActive = item.isActive,
                        SecretaryActive = item.SecretaryActive,
                        DelegationStartDate = item.DelegationStartDate,
                        DelegationEndDate = item.DelegationEndDate,
                        PrivilegedList = GetListRoles(item.PejabatId),
                        Type = item.Type,
                        TypeId = item.TypeId
                    };
                    if (uravm.PejabatJabatanName != null)
                    {
                        if (uravm.PejabatJabatanName.Contains("Skr."))
                        {
                            uravm.DelegationType = "Skr";
                        }
                        else if (uravm.PejabatJabatanName.Contains("Pgs."))
                        {
                            uravm.DelegationType = "Pgs";
                        }
                    }
                    listRoles.Add(uravm);
                }
                else if (item.Type == "Secretary" && item.SecretaryActive == true && item.IsDeleted == false)
                {
                    UserRolesAdditionalViewModels uravm = new UserRolesAdditionalViewModels()
                    {
                        Id = item.Id,
                        LoginUserPersonId = item.LoginUserPersonId,
                        LoginUserUnitKerja = item.LoginUserUnitKerja,
                        LoginUserUnitKerjaId = item.LoginUserUnitKerjaId,
                        PejabatRoleName = item.PejabatRoleName,
                        PejabatId = item.PejabatId,
                        PejabatName = ti.ToTitleCase(item.PejabatName.ToLower()),
                        PejabatJabatanId = item.PejabatJabatanId,
                        PejabatJabatanName = item.PejabatJabatanName,
                        PejabatBODLEvel = item.PejabatBODLEvel,
                        isActive = item.isActive,
                        SecretaryActive = item.SecretaryActive,
                        DelegationStartDate = item.DelegationStartDate,
                        DelegationEndDate = item.DelegationEndDate,
                        PrivilegedList = GetListRolesSecretary(item.PejabatId),
                        Type = item.Type,
                        TypeId = item.TypeId
                    };
                    if (uravm.PejabatJabatanName != null)
                    {
                        if (uravm.PejabatJabatanName.Contains("Skr."))
                        {
                            uravm.DelegationType = "Skr";
                        }
                        else if (uravm.PejabatJabatanName.Contains("Pgs."))
                        {
                            uravm.DelegationType = "Pgs";
                        }
                    }
                    listRoles.Add(uravm);
                }
                else if (item.Type == "User Main Role")
                {
                    UserRolesAdditionalViewModels uravm = new UserRolesAdditionalViewModels()
                    {
                        Id = item.Id,
                        LoginUserPersonId = item.LoginUserPersonId,
                        LoginUserUnitKerja = item.LoginUserUnitKerja,
                        LoginUserUnitKerjaId = item.LoginUserUnitKerjaId,
                        PejabatRoleName = ti.ToTitleCase(item.PejabatRoleName.ToLower()),
                        PejabatId = item.PejabatId,
                        PejabatName = item.PejabatName,
                        PejabatJabatanId = item.PejabatJabatanId,
                        PejabatJabatanName = item.PejabatJabatanName,
                        PejabatBODLEvel = item.PejabatBODLEvel,
                        isActive = item.isActive,
                        SecretaryActive = item.SecretaryActive,
                        DelegationStartDate = item.DelegationStartDate,
                        DelegationEndDate = item.DelegationEndDate,
                        PrivilegedList = GetListRoles(item.PejabatId),
                        Type = item.Type,
                        TypeId = item.TypeId
                    };
                    if (uravm.PejabatJabatanName != null)
                    {
                        if (uravm.PejabatJabatanName.Contains("Skr."))
                        {
                            uravm.DelegationType = "Skr";
                        }
                        else if (uravm.PejabatJabatanName.Contains("Pgs."))
                        {
                            uravm.DelegationType = "Pgs";
                        }
                        else
                        {
                            uravm.DelegationType = "";
                        }
                    }
                    listRoles.Add(uravm);
                }
            }

            var mainrole = listRoles.Where(a => a.Type == "User Main Role" && a.LoginUserPersonId == parentId).FirstOrDefault();
            var rangkapJabatan = listRoles.Where(a => a.Type == "User Main Role" && a.LoginUserPersonId != parentId).ToList();
            var delegasiRole = listRoles.Where(a => a.Type == "Delegation").ToList();
            var secretaryRole = listRoles.Where(a => a.Type == "Secretary").ToList();

            var orderRoles = new List<UserRolesAdditionalViewModels>();
            orderRoles.Add(mainrole);
            if (rangkapJabatan.Count() > 0)
            {
                orderRoles.AddRange(rangkapJabatan);
            }
            if (delegasiRole.Count() > 0)
            {
                orderRoles.AddRange(delegasiRole);
            }
            if (secretaryRole.Count() > 0)
            {
                orderRoles.AddRange(secretaryRole);
            }

            return orderRoles;
        }
        public PositionViewModels GetPosition(string name)
        {
            var model = new PositionViewModels();
            string connStr = _appSettings.DataConnString;
            string table = "\"JobMappings\"";
            string position = "\"PositionName\"";
            var sql = $"Select * from {table} where {position} = '{name}' limit 1;";
            using (NpgsqlConnection con = new NpgsqlConnection(connStr))
            {
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                con.Open();
                NpgsqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        model.PositionName = reader["PositionName"].ToString();
                        model.RecentPositionId = Convert.ToDouble(reader["PositionId"]);
                    }
                }
                catch (Exception e)
                {

                }
                con.Close();
            }
            return model;
        }
        public int CheckCount(double? orgId)
        {
            string connStr = _appSettings.DataConnString;
            string table = "\"JobMappings\"";
            string org = "\"OrgId\"";
            var sql = $"Select Count (*) from {table} where {org} = '{orgId}';";
            int count = 0;
            using (NpgsqlConnection con = new NpgsqlConnection(connStr))
            {
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                con.Open();
                NpgsqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        count = Convert.ToInt32(reader["count"]);
                    }
                }
                catch (Exception e)
                {

                }
                con.Close();
            }
            return count;
        }
        public void InsertPosition(double? orgId, string orgName, double? parentOrgId, string positionName)
        {
            var token = GetUserToken();
            int count = CheckCount(orgId);
            double? newId = orgId + count;
            string url = _apiEndpoint.JobMapping;
            try
            {
                string jsonData = "{\"orgId\":" + JsonConvert.SerializeObject(orgId) + "," +
                "\"orgName\":" + JsonConvert.SerializeObject(orgName) + "," +
                "\"parentOrgId\":" + JsonConvert.SerializeObject(parentOrgId) + "," +
                "\"positionId\":" + JsonConvert.SerializeObject(newId) + "," +
                "\"positionName\":" + JsonConvert.SerializeObject(positionName) + "}";

                IRestClient client = new RestClient();
                IRestRequest req = new RestRequest()
                {
                    Resource = url
                };
                req.AddHeader("Authorization", $"Bearer {token}");
                client.AddDefaultHeader("accept", "application/json");
                client.AddDefaultHeader("Content-type", "application/json");
                req.AddJsonBody(jsonData);

                IRestResponse responseApi = client.Post(req);

                var position = GetPosition(positionName);
                string a = position.PositionName;
            }
            catch (Exception e)
            {

            }
        }
        public void RolesFromUser(string Id)
        {
            var user = dbContext.Users.Where(a => a.Id == Id).FirstOrDefault();
            if (user != null)
            {
                var exist = dbContext.UserRolesAdditionals.Where(a => a.Type == "User Main Role" && a.TypeId == Id).FirstOrDefault();
                if (exist != null)
                {
                    exist.PejabatBODLEvel = user.BODLevel;
                    exist.PejabatRoleId = user.RoleId;
                    exist.PejabatRoleName = user.RoleName;
                    exist.PejabatJabatanId = user.RecentPositionId;
                    exist.PejabatJabatanName = user.PositionName;
                    exist.LoginUserUnitKerja = user.UnitKerja;
                    exist.LoginUserUnitKerjaId = user.UnitKerjaId;
                    exist.UpdatedDate = DateTime.Now;
                    dbContext.UserRolesAdditionals.Update(exist);
                    dbContext.SaveChanges();
                }
                else
                {
                    string parentId = user.ParentUserId == null ? user.Id : user.ParentUserId;

                    var userRoles = new UserRolesAdditional()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Type = "User Main Role",
                        TypeId = Id,
                        LoginUserPersonId = user.Id,
                        LoginUserPersonName = user.PersonName,
                        LoginUserUnitKerja = user.UnitKerja,
                        LoginUserUnitKerjaId = user.UnitKerjaId,
                        PejabatId = user.Id,
                        PejabatBODLEvel = user.BODLevel,
                        PejabatName = user.PersonName,
                        PejabatJabatanId = user.RecentPositionId,
                        PejabatJabatanName = user.PositionName,
                        PejabatRoleId = user.RoleId,
                        PejabatRoleName = user.RoleName,
                        SecretaryActive = false,
                        isActive = false,
                        DelegationEndDate = null,
                        DelegationStartDate = null,
                        ParentUserId = parentId,
                        CreatedDate = DateTime.Now
                    };
                    dbContext.UserRolesAdditionals.Add(userRoles);
                    dbContext.SaveChanges();
                }
            }
        }
        public async Task<NotificationsCountViewModels> GetNotificationList(string delegationId)
        {
            try
            {
                NotificationsCountViewModels vm = new NotificationsCountViewModels();
                var token = GetUserToken();
                string url = _apiEndpoint.Notification + delegationId;
                var client = new RestClient($"{url}");
                var req = new RestRequest(Method.GET);
                req.AddHeader("Authorization", $"Bearer {token}");
                var responseApi = await client.ExecuteAsync(req);
                var value = JObject.Parse(responseApi.Content);
                //var item = value.GetValue("data");
                vm.Count = (int?)value["count"];
                vm.UnRead = (bool)value["unRead"];
                return vm;
            }
            catch (Exception e)
            {
                return null;
            }
            
        }
        public List<UserRolesAdditionalViewModels> GetSecretaryReceiver(string id)
        {
            DateTime? start = null;
            DateTime? end = null;

            List<UserRolesAdditionalViewModels> listvm = new List<UserRolesAdditionalViewModels>();
            //string id = GetUserId();
            var activeSecretary = dbContext.UserRolesAdditionals.Where(a =>
            a.PejabatId == id && a.Type.ToLower() == "Secretary".ToLower()
            && a.IsDeleted == false && a.SecretaryActive == true).ToList();
            if (activeSecretary == null)
            {
                return null;
            }

            foreach (var item in activeSecretary)
            {
                if (item.SecretaryActive == true)
                {
                    var user = dbContext.Users.Where(a => a.Id == item.LoginUserPersonId).FirstOrDefault();
                    UserRolesAdditionalViewModels vm = new UserRolesAdditionalViewModels();
                    vm = mapper.Map<UserRolesAdditionalViewModels>(item);
                    vm.LoginUserPersonName = user.PersonName;
                    vm.LoginUserJabatanName = user.PositionName;
                    listvm.Add(vm);
                }
            }
            return listvm;
        }
        public List<UserRolesAdditionalViewModels> GetDelegationReceiver(string id)
        {
            DateTime? start = null;
            DateTime? end = null;

            List<UserRolesAdditionalViewModels> listvm = new List<UserRolesAdditionalViewModels>();
            //string id = GetUserId();
            var activeDelegation = dbContext.UserRolesAdditionals.Where(a => a.DelegationUserId == id
            && a.Type.ToLower() == "Delegation".ToLower() && a.IsDeleted == false).OrderBy(a => a.Order).ToList();
            if (activeDelegation == null)
            {
                return null;
            }

            foreach (var item in activeDelegation)
            {
                if (item.DelegationStartDate != null)
                {
                    start = item.DelegationStartDate.Value.StartOfDay();
                }
                if (item.DelegationEndDate != null)
                {
                    end = item.DelegationEndDate.Value.EndOfDay();
                }
                if (DateTime.Now >= start && DateTime.Now <= end)
                {
                    var user = dbContext.Users.Where(a => a.Id == item.LoginUserPersonId).FirstOrDefault();
                    UserRolesAdditionalViewModels vm = new UserRolesAdditionalViewModels();
                    vm = mapper.Map<UserRolesAdditionalViewModels>(item);
                    vm.LoginUserPersonName = user.PersonName;
                    vm.LoginUserJabatanName = user.PositionName;
                    listvm.Add(vm);
                }
            }
            return listvm;
        }

        public List<UserRolesAdditionalViewModels> GetUserDelegated(string id)
        {
            DateTime? start = null;
            DateTime? end = null;

            List<UserRolesAdditionalViewModels> listvm = new List<UserRolesAdditionalViewModels>();
            //string id = GetUserId();
            var activeDelegation = dbContext.UserRolesAdditionals.Where(a => a.LoginUserPersonId == id
            && a.Type.ToLower() == "Delegation".ToLower() && a.IsDeleted == false).OrderBy(a => a.Order).ToList();
            if (activeDelegation == null)
            {
                return null;
            }

            foreach (var item in activeDelegation)
            {
                if (item.DelegationStartDate != null)
                {
                    start = item.DelegationStartDate.Value.StartOfDay();
                }
                if (item.DelegationEndDate != null)
                {
                    end = item.DelegationEndDate.Value.EndOfDay();
                }
                if (DateTime.Now >= start && DateTime.Now <= end)
                {
                    var user = dbContext.Users.Where(a => a.Id == item.PejabatId).FirstOrDefault();
                    UserRolesAdditionalViewModels vm = new UserRolesAdditionalViewModels();
                    //vm = mapper.Map<UserRolesAdditionalViewModels>(item);
                    vm.LoginUserPersonId = user.Id;
                    vm.LoginUserPersonName = user.PersonName;
                    vm.LoginUserJabatanName = user.PositionName;
                    listvm.Add(vm);
                }
            }
            return listvm;
        }
        public List<UserRolesAdditionalViewModels> GetDelegatedFrom(string id)
        {
            DateTime? start = null;
            DateTime? end = null;

            List<UserRolesAdditionalViewModels> listvm = new List<UserRolesAdditionalViewModels>();
            //string id = GetUserId();
            var activeDelegation = dbContext.UserRolesAdditionals.Where(a => a.LoginUserPersonId == id
            && a.Type.ToLower() == "Delegation".ToLower() && a.IsDeleted == false).OrderBy(a => a.Order).ToList();
            if (activeDelegation == null)
            {
                return null;
            }

            foreach (var item in activeDelegation)
            {
                if (item.DelegationStartDate != null)
                {
                    start = item.DelegationStartDate.Value.StartOfDay();
                }
                if (item.DelegationEndDate != null)
                {
                    end = item.DelegationEndDate.Value.EndOfDay();
                }
                if (DateTime.Now >= start && DateTime.Now <= end)
                {
                    var user = dbContext.Users.Where(a => a.Id == item.LoginUserPersonId).FirstOrDefault();
                    UserRolesAdditionalViewModels vm = new UserRolesAdditionalViewModels();
                    vm = mapper.Map<UserRolesAdditionalViewModels>(item);
                    vm.LoginUserPersonName = user.PersonName;
                    vm.LoginUserJabatanName = user.PositionName;
                    listvm.Add(vm);
                }
            }
            return listvm;
        }
        public List<UserAdditionalViewModels> GetUserAdditional(string id)
        {
            List<UserAdditionalViewModels> lvm = new List<UserAdditionalViewModels>();
            List<UserRolesAdditionalViewModels> list = new List<UserRolesAdditionalViewModels>();

            var delegation = GetDelegationReceiver(id);
            list.AddRange(delegation);
            //var secretary = getSecretaryReceiver(id);
            //list.AddRange(secretary);

            foreach (var item in list)
            {
                UserAdditionalViewModels userAdditional = new UserAdditionalViewModels();
                var user = dbContext.Users.Where(a => a.Id == item.LoginUserPersonId).FirstOrDefault();
                userAdditional = mapper.Map<UserAdditionalViewModels>(user);
                userAdditional.EmployeeNumber = user.EmployeeNumber;
                if (userAdditional.PositionName != null)
                {
                    userAdditional.PositionName = item.PejabatJabatanName;
                    if (item.PejabatJabatanName.Contains("Pgs."))
                    {
                        userAdditional.Type = "Pgs";
                        userAdditional.DelegationType = "Pgs";
                    }
                }
                lvm.Add(userAdditional);
            }
            return lvm;
        }

        public List<UserAdditionalViewModels> GetUserAdditionalDelegation(string id)
        {
            List<UserAdditionalViewModels> lvm = new List<UserAdditionalViewModels>();
            List<UserRolesAdditionalViewModels> list = new List<UserRolesAdditionalViewModels>();

            var delegation = GetUserDelegated(id);
            list.AddRange(delegation);
            //var secretary = getSecretaryReceiver(id);
            //list.AddRange(secretary);

            foreach (var item in list)
            {
                UserAdditionalViewModels userAdditional = new UserAdditionalViewModels();
                var user = dbContext.Users.Where(a => a.Id == item.LoginUserPersonId).FirstOrDefault();
                userAdditional = mapper.Map<UserAdditionalViewModels>(user);
                if (userAdditional.PositionName != null)
                {
                    userAdditional.PositionName = item.PejabatJabatanName;
                    userAdditional.Type = "Pgs";
                    userAdditional.DelegationType = "Pgs";
                }
                lvm.Add(userAdditional);
            }
            return lvm;
        }
        public ResponseViewModels<bool> UpdateUserAdditionaMainRole()
        {
            try
            {
                var models = dbContext.UserRolesAdditionals.ToList();
                foreach (var item in models)
                {
                    var user = dbContext.Users.Where(a => a.Id == item.LoginUserPersonId).FirstOrDefault();
                    if (user != null)
                    {
                        item.LoginUserPersonName = user.PersonName;
                        dbContext.UserRolesAdditionals.Update(item);
                    }
                    else
                    {
                        continue;
                    }
                }
                dbContext.SaveChanges();
            }
            catch (Exception e)
            {
                return new ResponseViewModels<bool>()
                {
                    StatusCode = 400,
                    Message = "Failed Update Data",
                    Data = false
                };
            }
            return new ResponseViewModels<bool>()
            {
                StatusCode = 200,
                Message = "Succesfully Update Data",
                Data = true
            };
        }
        public List<UnitViewModels> GetAllUnit()
        {
            List<UnitViewModels> listModel = new List<UnitViewModels>();
            //string connStr = configuration.GetConnectionString("ConnStr");
            //string table = "jm_org_hierarchy";
            string connStr = _appSettings.ServerConnString;
            var sql = "SELECT id, nama_unit FROM public.jm_unit where isdeleted is null;"/* + table*/;
            using (NpgsqlConnection con = new NpgsqlConnection(connStr))
            {
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                con.Open();
                NpgsqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        var model = new UnitViewModels();
                        model.Id = Convert.ToInt64(reader[("id")]);
                        model.NamaUnit = reader["nama_unit"].ToString();
                        listModel.Add(model);
                    }
                }
                catch (Exception e)
                {

                }
            }
            return listModel;
        }
        public void UpdateUserMainRole(string id, double? jabatanId, string jabatanName)
        {

            var userroles = dbContext.UserRolesAdditionals.Where(a => a.PejabatId == id).ToList();
            foreach (var item in userroles)
            {
                item.PejabatJabatanId = jabatanId;
                item.PejabatJabatanName = jabatanName;
                dbContext.UserRolesAdditionals.Update(item);
                dbContext.SaveChanges();
            }
        }
        public void UpdateUserAdditional(string id, double? jabatanId, string jabatanName, string roleId, string roleName, bool isActive)
        {

            var userroles = dbContext.UserRolesAdditionals.Where(a => a.PejabatId == id).ToList();
            foreach (var item in userroles)
            {
                item.PejabatJabatanId = jabatanId;
                item.PejabatJabatanName = jabatanName;
                item.PejabatRoleId = roleId;
                item.PejabatRoleName = roleName;
                if (isActive == true)
                {
                    item.IsDeleted = false;
                }
                else
                {
                    item.IsDeleted = true;
                }
                dbContext.UserRolesAdditionals.Update(item);
                dbContext.SaveChanges();
            }
        }
        public ApplicationUser CreateUserDelegation(DelegationViewModels vm)
        {
            var pejabat = dbContext.Users.Where(a => a.Id == vm.DelegateUserId).FirstOrDefault();
            var pgs1 = dbContext.Users.Where(a => a.Id == vm.Replacement1UserId).FirstOrDefault();
            string jabatanId = pejabat.RecentPositionId.ToString();
            jabatanId = jabatanId + "01";
            //var pengganti = dbContext.Users.Where(a => a.Id == vm.re).FirstOrDefault();
            var exist = dbContext.Users.Where(a => a.Id == vm.Id).FirstOrDefault();
            if (exist == null)
            {
                string password = _appSettings.DefaultPassword;
                var model = new ApplicationUser();
                model.Id = vm.Id;
                model.PersonName = vm.Replacement1UserName;
                model.Email = pgs1?.Email;
                model.CreatedDate = DateTime.Now;
                model.UpdatedDate = DateTime.Now;
                model.IsDelegation = true;
                model.BODLevel = pejabat.BODLevel;
                model.DelegationStart = vm.startDate;
                model.DelegationEnd = vm.endDate;
                model.RecentPositionId = Convert.ToDouble(jabatanId);
                model.PositionName = "Pgs. " + pejabat.PositionName;
                model.IsActive = true;
                model.IsGroup = false;
                model.RoleId = "f4a3bfb3-511c-40f3-8e26-b873561d4d81";
                model.RoleName = "Public";
                model.UserName = "pgs_" + vm.Id;
                IdentityResult result = _userManager.CreateAsync(model, password).Result;

                return model;
            }
            else
            {
                exist.PersonName = vm.Replacement1UserName;
                exist.Email = pgs1?.Email;
                exist.RecentPositionId = Convert.ToDouble(jabatanId);
                exist.PositionName = "Pgs. " + pejabat.PositionName;
                exist.DelegationStart = vm.startDate;
                exist.DelegationEnd = vm.endDate;
                dbContext.Users.Update(exist);
                dbContext.SaveChanges();
                return exist;
            }
        }
        public List<UsersViewModel> GetDelegasiUser(string firstChar)
        {
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;
            var vm = new List<UsersViewModel>();
            var model = dbContext.Users.Where(a => a.IsDelegation == true).ToList();

            model = model.Where(a => a.PersonName.ToLower().StartsWith(firstChar.ToLower())
                || a.PositionName.ToLower().StartsWith(firstChar.ToLower())).ToList();

            model = model.Where(a => a.IsActive == true).ToList();
            foreach (var item in model)
            {
                DateTime? start = null;
                DateTime? end = null;
                if (item.DelegationStart != null)
                {
                    start = item.DelegationStart.Value.StartOfDay();
                }
                if (item.DelegationEnd != null)
                {
                    end = item.DelegationEnd.Value.EndOfDay();
                }
                if (DateTime.Now >= start && DateTime.Now <= end && item.IsDeleted == false)
                {
                    var uvm = new UsersViewModel();
                    uvm = mapper.Map<UsersViewModel>(item);
                    uvm.PersonName = ti.ToTitleCase(item.PersonName.ToLower());
                    uvm.UserAdditionals = GetUserAdditionalDelegation(item.Id);
                    uvm.ActiveAdditionalRoles = GetActiveAdditionalRolesV2(item.Id);
                    if (uvm.ActiveAdditionalRoles == null)
                    {
                        uvm.Type = "User Main Role";
                    }
                    else
                    {
                        uvm.Type = uvm.ActiveAdditionalRoles.Type;
                    }
                    //uvm.DelegatedFrom = GetDelegatedFrom(item.Id);
                    vm.Add(uvm);
                }
            }
            return vm;
        }
        public List<UsersViewModel> GetDelegasiUserV2(string firstChar, List<string> ids)
        {
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;
            var vm = new List<UsersViewModel>();
            var list = new List<UserRolesAdditional>();
            var model = dbContext.UserRolesAdditionals.Where(a => a.IsDeleted == false && a.Type == "Delegation").ToList();
            model = model.Where(a => a.LoginUserPersonName.ToLower().StartsWith(firstChar.ToLower())
                || a.PejabatJabatanName.ToLower().StartsWith(firstChar.ToLower())).ToList();
            list.AddRange(model);

            foreach (var id in ids)
            {
                var pgs = dbContext.UserRolesAdditionals.Where(a => a.IsDeleted == false && a.Type == "Delegation" && a.LoginUserPersonId == id).ToList();
                if (pgs.Count() > 0)
                {
                    list.AddRange(pgs);
                }
            }
            list = list.Distinct().ToList();
            if (list.Count() > 0)
            {
                foreach (var item in list)
                {
                    var userlogin = dbContext.Users.Where(a => a.Id == item.LoginUserPersonId).FirstOrDefault();
                    DateTime? start = null;
                    DateTime? end = null;
                    if (item.DelegationStartDate != null)
                    {
                        start = item.DelegationStartDate.Value.StartOfDay();
                    }
                    if (item.DelegationEndDate != null)
                    {
                        end = item.DelegationEndDate.Value.EndOfDay();
                    }

                    if (DateTime.Now >= start && DateTime.Now <= end && item.IsDeleted == false)
                    {
                        var uvm = new UsersViewModel();
                        //uvm = mapper.Map<UsersViewModel>(item);
                        uvm.Id = item.Id;
                        uvm.PersonId = item.TypeId;
                        uvm.UserId = item.LoginUserPersonId;
                        uvm.UserPositionId = userlogin.RecentPositionId.ToString();
                        uvm.UserPosition = userlogin.PositionName;
                        uvm.PersonName = ti.ToTitleCase(item.LoginUserPersonName.ToLower());
                        uvm.RecentPositionId = item.PejabatJabatanId;
                        uvm.PositionName = item.PejabatJabatanName;
                        uvm.UserAdditionals = GetUserAdditionalDelegation(item.TypeId);
                        uvm.ActiveAdditionalRoles = GetActiveAdditionalRolesV2(item.TypeId);
                        uvm.Type = "Delegation";
                        uvm.DelegationId = item.TypeId;
                        vm.Add(uvm);
                    }
                }
            }
            return vm;
        }
    }
}
