﻿using AutoMapper;
using Jasamarga.DA.UserManagement.Databases;
using Jasamarga.DA.UserManagement.Databases.Identity;
using Jasamarga.DA.UserManagement.Databases.Models;
using Jasamarga.DA.UserManagement.Services.Contracts;
using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using Npgsql;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using MimeKit;
using MimeKit.Text;
using MailKit.Net.Smtp;

namespace Jasamarga.DA.UserManagement.Services.Repositories
{
    public class AuthenticateServices : TokenServices, IAuthenticateServices
    {
        protected IMapper mapper { set; get; }
        protected ApplicationDbContext dbContext { set; get; }
        protected string includes { set; get; }
        private UserManager<ApplicationUser> _userManager;
        private RoleManager<ApplicationRole> _roleManager;
        private readonly AppSettings _appSettings;
        private readonly ApiEndpoint _apiEndpoint;
        public static string DirName = "User";
        private IHttpContextAccessor accessor;

        public AuthenticateServices(IMapper mapper,
            ApplicationDbContext dbContext,
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            IOptions<AppSettings> appSettings,
            IOptions<ApiEndpoint> apiEndpoint,
            IHttpContextAccessor accessor) : base(accessor, dbContext)
        {
            this.mapper = mapper;
            this.dbContext = dbContext;
            this._userManager = userManager;
            this._roleManager = roleManager;
            this._appSettings = appSettings.Value;
            this._apiEndpoint = apiEndpoint.Value;
            this.accessor = accessor;
        }

        #region token
        public TokenResponse Authenticate(LoginViewModel model, string ipAddress)
        {
            try
            {
                var userLogin = dbContext.Users.Where(a => a.UserName == model.UserName).FirstOrDefault();

                var credentials = GetToken(userLogin);
                var refreshToken = generateRefreshToken(ipAddress);

                userLogin.RefreshTokens.Add(refreshToken);
                dbContext.Update(userLogin);
                dbContext.SaveChanges();

                return new TokenResponse()
                {
                    StatusCode = 200,
                    Message = "Your Login is Successfull",
                    Token = credentials,
                    Type = "Bearer",
                    RefreshToken = refreshToken.Token,
                    UserId = userLogin.Id
                };
            }
            catch
            {
                return new TokenResponse()
                {
                    StatusCode = 401,
                    Message = "Unauthorhized",
                    Token = null,
                    Type = null,
                    RefreshToken = null,
                    UserId = null
                };
            }
        }

        public TokenResponse AuthenticateV2(string id)
        {
            try
            {
                var userLogin = dbContext.Users.Where(a => a.Id == id).FirstOrDefault();

                var credentials = GetToken(userLogin);
                var refreshToken = generateRefreshTokenV2();

                userLogin.RefreshTokens.Add(refreshToken);
                dbContext.Update(userLogin);
                dbContext.SaveChanges();

                return new TokenResponse()
                {
                    StatusCode = 200,
                    Message = "Your Login is Successfull",
                    Token = credentials,
                    Type = "Bearer",
                    RefreshToken = refreshToken.Token,
                    UserId = userLogin.Id
                };
            }
            catch
            {
                return new TokenResponse()
                {
                    StatusCode = 401,
                    Message = "Unauthorhized",
                    Token = null,
                    Type = null,
                    RefreshToken = null,
                    UserId = null
                };
            }
        }

        public TokenResponse RefreshToken(string token, string ipAddress)
        {
            var user = dbContext.Users.Where(u => u.RefreshTokens.Any(t => t.Token == token)).FirstOrDefault();

            // return null if no user found with token
            if (user == null) return null;

            var refreshToken = user.RefreshTokens.Single(x => x.Token == token);
            //string id = GetUserId();
            //var user = dbContext.Users.Where(a => a.Id == id).FirstOrDefault();
            // return null if token is no longer active
            if (!refreshToken.IsActive) return null;
            if (DateTime.Now > refreshToken.Expires)
            {
                return null;
            }
            var jwtToken = GetToken(user);
            // replace old refresh token with a new one and save
            var newRefreshToken = generateRefreshToken(ipAddress);
            refreshToken.Revoked = DateTime.Now;
            refreshToken.RevokedByIp = ipAddress;
            refreshToken.ReplacedByToken = token;
            //var refreshToken = generateRefreshToken(ipAddress);
            user.RefreshTokens.Add(newRefreshToken);
            dbContext.Update(user);
            dbContext.SaveChanges();

            // generate new jwt


            return new TokenResponse()
            {
                StatusCode = 200,
                Message = "Refresh Token is Successfull",
                Token = jwtToken,
                Type = "Bearer",
                RefreshToken = newRefreshToken.Token,
                UserId = user.Id
            };
        }
        public bool RevokeToken(string token, string ipAddress)
        {
            var user = dbContext.Users.Where(u => u.RefreshTokens.Any(t => t.Token == token)).FirstOrDefault();

            // return false if no user found with token
            if (user == null) return false;
            var refreshToken = user.RefreshTokens.Single(x => x.Token == token);
            // return false if token is not active
            if (!refreshToken.IsActive) return false;
            // revoke token and save
            refreshToken.Revoked = DateTime.UtcNow;
            refreshToken.RevokedByIp = ipAddress;
            dbContext.Update(refreshToken);
            dbContext.SaveChanges();
            return true;
        }
        public void DeleteToken(ApplicationUser user, string token)
        {
            var refreshToken = user.RefreshTokens.Single(x => x.Token == token);
            if (refreshToken != null)
            {
                dbContext.Remove(refreshToken);
                dbContext.SaveChanges();
            }
        }
        public TokenResponse Revoke(/*string token, string ipAddress*/)
        {
            //var user = dbContext.Users.Where(u => u.RefreshTokens.Any(t => t.Token == token)).FirstOrDefault();

            //if (user == null) return new TokenResponse()
            //{
            //    StatusCode = 401,
            //    Message = "User Not Found",
            //};
            string id = GetUserId();
            var user = dbContext.Users.Where(a => a.Id == id).FirstOrDefault();
            var userLogin = user.RefreshTokens.ToList();
            dbContext.RemoveRange(userLogin);
            dbContext.SaveChanges();
            //var refreshToken = user.RefreshTokens.Single(x => x.Token == token);
            // return false if token is not active
            //if (!refreshToken.IsActive) return new TokenResponse()
            //{
            //    StatusCode = 401,
            //    Message = "User Not Found",
            //};
            // revoke token and save
            //refreshToken.Revoked = DateTime.UtcNow;
            //refreshToken.RevokedByIp = ipAddress;
            //dbContext.Update(refreshToken);
            //dbContext.SaveChanges();

            //var userLogin = user.RefreshTokens.ToList();
            //dbContext.RemoveRange(userLogin);
            //dbContext.SaveChanges();

            return new TokenResponse()
            {
                StatusCode = 200,
                Message = "Revoke Token is Succesfully",
            };
        }
        public TokenResponse RevokeV2(string token, string ipAddress)
        {
            var user = dbContext.Users.Where(u => u.RefreshTokens.Any(t => t.Token == token)).FirstOrDefault();

            if (user == null) return new TokenResponse()
            {
                StatusCode = 401,
                Message = "User Not Found",
            };
            //var refreshToken = user.RefreshTokens.Single(x => x.Token == token);
            //// return false if token is not active
            //if (!refreshToken.IsActive) if (user == null) return new TokenResponse()
            //{
            //    StatusCode = 401,
            //    Message = "Token is inactive",
            //};
            //dbContext.Remove(refreshToken);
            //dbContext.SaveChanges();
            DeleteToken(user, token);

            var credentials = GetTokenRevoke(user);
            var newToken = generateRefreshToken(ipAddress);

            user.RefreshTokens.Add(newToken);
            dbContext.Update(user);
            dbContext.SaveChanges();


            DeleteToken(user, credentials);
            return new TokenResponse()
            {
                StatusCode = 200,
                Message = "Revoke Token is Succesfully",
                Token = credentials,
                Type = "Bearer",
                UserId = user.Id
            };
        }
        private string GetTokenRevoke(ApplicationUser User)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.Secret));
            var credentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                        new Claim(JwtRegisteredClaimNames.Sub, User.Id),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                }),
                Expires = DateTime.Now,
                SigningCredentials = credentials
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);

        }
        private string GetToken(ApplicationUser User)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.Secret));
            var credentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                        new Claim(JwtRegisteredClaimNames.Sub, User.Id),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                }),
                Expires = DateTime.Now.AddMinutes(_appSettings.Expires),
                SigningCredentials = credentials,
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);

        }
        private RefreshToken generateRefreshToken(string ipAddress)
        {
            using (var rngCryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                var randomBytes = new byte[64];
                rngCryptoServiceProvider.GetBytes(randomBytes);
                return new RefreshToken
                {
                    Id = Guid.NewGuid().ToString(),
                    Token = Convert.ToBase64String(randomBytes),
                    Expires = DateTime.Now.AddMinutes(_appSettings.ExpiresRefreshToken),
                    CreatedDate = DateTime.UtcNow,
                    CreatedByIp = ipAddress
                };
            }
        }
        private RefreshToken generateRefreshTokenV2()
        {
            using (var rngCryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                var randomBytes = new byte[64];
                rngCryptoServiceProvider.GetBytes(randomBytes);
                return new RefreshToken
                {
                    Id = Guid.NewGuid().ToString(),
                    Token = Convert.ToBase64String(randomBytes),
                    Expires = DateTime.Now.AddHours(_appSettings.ExpiresRefreshToken),
                    CreatedDate = DateTime.UtcNow
                };
            }
        }
        private RefreshToken generateRefreshTokenV2(string ipAddress, string token)
        {
            //using (var rngCryptoServiceProvider = new RNGCryptoServiceProvider())
            //{
            //    var randomBytes = new byte[64];
            //    rngCryptoServiceProvider.GetBytes(randomBytes);
            //    return new RefreshToken
            //    {
            //        Id = Guid.NewGuid().ToString(),
            //        Token = Convert.ToBase64String(randomBytes),
            //        Expires = DateTime.UtcNow.AddDays(7),
            //        CreatedDate = DateTime.UtcNow,
            //        CreatedByIp = ipAddress
            //    };
            //}

            return new RefreshToken
            {
                Id = Guid.NewGuid().ToString(),
                Token = token,
                Expires = DateTime.UtcNow.AddMinutes(1),
                CreatedDate = DateTime.UtcNow,
                CreatedByIp = ipAddress
            };
        }
        public void SaveUserDevice(string userId, string DeviceId, string DeviceType)
        {
            var current = dbContext.UserDevices.Where(a => a.DeviceId == DeviceId).FirstOrDefault();

            if (current == null)
            {
                var model = new UserDevice();
                model.SetCreated();
                model.UserId = userId;
                model.DeviceId = DeviceId;
                model.DeviceType = DeviceType;
                dbContext.Add(model);
                dbContext.SaveChanges();
            }
        }
        #endregion
    }
}
