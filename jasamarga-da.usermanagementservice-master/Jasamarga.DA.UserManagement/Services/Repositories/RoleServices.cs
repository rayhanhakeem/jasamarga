﻿using AutoMapper;
using Jasamarga.DA.UserManagement.Databases;
using Jasamarga.DA.UserManagement.Databases.Identity;
using Jasamarga.DA.UserManagement.Databases.Models;
using Jasamarga.DA.UserManagement.Services.Contracts;
using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using Npgsql;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using MimeKit;
using MimeKit.Text;
using MailKit.Net.Smtp;

namespace Jasamarga.DA.UserManagement.Services.Repositories
{
    public class RoleServices : TokenServices, IRoleServices
    {
        protected IMapper mapper { set; get; }
        protected ApplicationDbContext dbContext { set; get; }
        protected string includes { set; get; }
        private UserManager<ApplicationUser> _userManager;
        private RoleManager<ApplicationRole> _roleManager;
        private readonly AppSettings _appSettings;
        private readonly ApiEndpoint _apiEndpoint;
        public static string DirName = "User";
        private IHttpContextAccessor accessor;
        protected IHelperServices helperSvc;
        protected IAuthenticateServices authSvc;
        protected IAccountServices accSvc;

        public RoleServices(IMapper mapper,
            ApplicationDbContext dbContext,
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            IOptions<AppSettings> appSettings,
            IOptions<ApiEndpoint> apiEndpoint,
            IHttpContextAccessor accessor,
            IHelperServices helperSvc,
            IAuthenticateServices authSvc,
            IAccountServices accSvc) : base(accessor, dbContext)
        {
            this.mapper = mapper;
            this.dbContext = dbContext;
            this._userManager = userManager;
            this._roleManager = roleManager;
            this._appSettings = appSettings.Value;
            this._apiEndpoint = apiEndpoint.Value;
            this.accessor = accessor;
            this.helperSvc = helperSvc;
            this.accSvc = accSvc;
            this.authSvc = authSvc;
        }

        #region Role
        public ResponseViewModels<RoleViewModel> DeleteRole(string id)
        {
            var model = dbContext.Roles.Include("ApplicationRoleActionAuthorizations").Where(a => a.Id == id).FirstOrDefault();
            var users = dbContext.Users.Where(a => a.RoleId == id).ToList();
            var useradditional = dbContext.UserRolesAdditionals.Where(a => a.PejabatRoleId == id).ToList();
            if (model == null)
            {
                return new ResponseViewModels<RoleViewModel>
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }
            try
            {
                if (model != null)
                {
                    model.IsDeleted = true;
                    dbContext.Roles.Update(model);
                    dbContext.SaveChanges();

                    var listUserRoles = dbContext.UserRoles.Where(a => a.RoleId == id).ToList();
                    foreach (var item in listUserRoles)
                    {
                        dbContext.UserRoles.Remove(item);
                    }
                    dbContext.SaveChanges();
                    foreach (var item in users)
                    {
                        item.RoleId = "f4a3bfb3-511c-40f3-8e26-b873561d4d81";
                        item.RoleName = "Public";
                        dbContext.Users.Update(item);
                        IdentityResult updateRoles = _userManager.AddToRoleAsync(item, item.RoleName).Result;
                    }
                    foreach (var item in useradditional)
                    {
                        item.PejabatRoleId = "f4a3bfb3-511c-40f3-8e26-b873561d4d81";
                        item.PejabatRoleName = "Public";
                        dbContext.UserRolesAdditionals.Update(item);
                    }
                    dbContext.SaveChanges();
                }
                return new ResponseViewModels<RoleViewModel>
                {
                    StatusCode = 200,
                    Message = "Data Successfully Deleted",
                    Data = null
                };
            }
            catch (Exception e)
            {
                return new ResponseViewModels<RoleViewModel>
                {
                    StatusCode = 400,
                    Message = "Failed To Delete Data",
                    Data = null
                };
            }
        }
        public ResponseViewModels<RoleViewModel> RestoreRole(string id)
        {
            var model = dbContext.Roles.Include("ApplicationRoleActionAuthorizations").Where(a => a.Id == id).FirstOrDefault();
            if (model == null)
            {
                return new ResponseViewModels<RoleViewModel>
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }
            try
            {
                if (model != null)
                {
                    model.IsDeleted = false;
                    dbContext.Roles.Update(model);
                    dbContext.SaveChanges();
                }
                return new ResponseViewModels<RoleViewModel>
                {
                    StatusCode = 200,
                    Message = "Data Successfully Restored",
                    Data = null
                };
            }
            catch (Exception e)
            {
                return new ResponseViewModels<RoleViewModel>
                {
                    StatusCode = 400,
                    Message = "Failed To Delete Data",
                    Data = null
                };
            }
        }
        public async Task<ResponseViewModels<RoleViewModel>> InsertRole(RoleViewModel model)
        {
            try
            {

                var roleExist = dbContext.Roles.Where(a => a.Name.ToLower() == model.Name.ToLower() && a.IsDeleted == false).FirstOrDefault();
                if (roleExist == null)
                {
                    List<ApplicationRoleActionAuthorizations> listAuth = new List<ApplicationRoleActionAuthorizations>();
                    var role = new ApplicationRole();
                    role.SetCreated();
                    role.CreatedBy = GetPersonName();
                    role.UpdatedDate = DateTime.Now;
                    role.Name = model.Name;
                    var actions = model.ApplicationRoleActionAuthorizations.ToList();
                    model.Id = role.Id;
                    foreach (var item in actions)
                    {
                        ApplicationRoleActionAuthorizations actionAuthorizations = new ApplicationRoleActionAuthorizations()
                        {
                            ApplicationRoleId = role.Id,
                            PermissionId = item.PermissionId,
                            forCreate = item.forCreate,
                            forEdit = item.forEdit,
                            forView = item.forView,
                            forDelete = item.forDelete,
                            forRestore = item.forRestore
                        };
                        if (actionAuthorizations.forCreate == true || actionAuthorizations.forView == true ||
                            actionAuthorizations.forEdit == true || actionAuthorizations.forDelete == true ||
                            actionAuthorizations.forRestore == true)
                        {
                            listAuth.Add(actionAuthorizations);
                        }
                    }
                    if (listAuth.Count == 0)
                    {
                        return new ResponseViewModels<RoleViewModel>
                        {
                            StatusCode = 400,
                            Message = "Hak Akses Tidak boleh kosong",
                            Data = null
                        };
                    }
                    //role.UpdatedDate = DateTime.Now;
                    var checkExist = await _roleManager.RoleExistsAsync(model.Name);
                    if (checkExist != false)
                    {
                        var existToDelete = dbContext.Roles.Where(a => a.Name.ToLower() == model.Name.ToLower()).FirstOrDefault();
                        IdentityResult roleResult = _roleManager.DeleteAsync(existToDelete).Result;
                        var users = dbContext.Users.Where(a => a.RoleName.ToLower() == model.Name.ToLower()).ToList();
                        UpdateUserRole(users);
                        var userAdditional = dbContext.UserRolesAdditionals.Where(a => a.PejabatRoleName.ToLower() == model.Name.ToLower()).ToList();
                        UpdateUserAdditionalRole(userAdditional);
                    }
                    IdentityResult createResult = _roleManager.CreateAsync(role).Result;
                    dbContext.ApplicationRoleActionAuthorizations.AddRange(listAuth);
                    dbContext.SaveChanges();

                    var rolePermission = dbContext.Roles.Include("ApplicationRoleActionAuthorizations").Where(a => a.Id == model.Id).FirstOrDefault();
                    return new ResponseViewModels<RoleViewModel>
                    {
                        StatusCode = 201,
                        Message = "Data Successfully Saved",
                        Data = mapper.Map<RoleViewModel>(rolePermission)
                    };
                }
                else
                {
                    return new ResponseViewModels<RoleViewModel>
                    {
                        StatusCode = 409,
                        Message = "Nama Role Tidak Boleh Duplikat",
                        Data = null
                    };
                }

            }
            catch (Exception e)
            {
                return new ResponseViewModels<RoleViewModel>
                {
                    StatusCode = 400,
                    Message = "Failed To Save Data",
                    Data = null
                };
            }
        }

        public void UpdateUserRole(List<ApplicationUser> users)
        {
            foreach (var item in users)
            {
                item.RoleId = "f4a3bfb3-511c-40f3-8e26-b873561d4d81";
                item.RoleName = "Public";
                dbContext.Users.Update(item);
                IdentityResult updateRoles = _userManager.AddToRoleAsync(item, item.RoleName).Result;
            }
            dbContext.SaveChanges();
        }
        public void UpdateUserAdditionalRole(List<UserRolesAdditional> userAdditionals)
        {
            foreach (var item in userAdditionals)
            {
                item.PejabatRoleId = "f4a3bfb3-511c-40f3-8e26-b873561d4d81";
                item.PejabatRoleName = "Public";
                dbContext.UserRolesAdditionals.Update(item);
            }
            dbContext.SaveChanges();
        }
        public ResponseViewModels<RoleViewModel> GetRoleViewModel(string id)
        {
            var role = dbContext.Roles/*.Include("ApplicationRoleActionAuthorizations")*/.Where(a => a.Id == id).FirstOrDefault();
            if (role == null)
            {
                return new ResponseViewModels<RoleViewModel>
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }

            var roleAuth = dbContext.ApplicationRoleActionAuthorizations.Where(a => a.ApplicationRoleId == id
            && (a.forCreate == true || a.forDelete == true || a.forEdit == true || a.forView == true || a.forRestore == true)).ToList();
            var result = new RoleViewModel();
            result.Id = role.Id;
            result.Name = role.Name;
            result.ApplicationRoleActionAuthorizations = roleAuth;

            return new ResponseViewModels<RoleViewModel>
            {
                StatusCode = 200,
                Message = "Data Retrieved",
                Data = mapper.Map<RoleViewModel>(result)
            };
        }
        public ResponseViewModels<RoleViewModel> EditRole(RoleViewModel model)
        {
            List<ApplicationRoleActionAuthorizations> listAuth = new List<ApplicationRoleActionAuthorizations>();
            var current = dbContext.Roles.Where(a => a.Id == model.Id).FirstOrDefault();
            if (current == null)
            {
                return new ResponseViewModels<RoleViewModel>
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }
            var roleExist = dbContext.Roles.Where(a => a.Name == model.Name && a.IsDeleted == false && a.Id != model.Id).FirstOrDefault();
            if (roleExist != null)
            {
                return new ResponseViewModels<RoleViewModel>
                {
                    StatusCode = 409,
                    Message = "Nama Role Tidak Boleh Duplikat",
                    Data = null
                };
            }
            current.Name = model.Name;
            current.NormalizedName = model.Name.ToUpper();
            current.UpdatedDate = DateTime.Now;


            try
            {
                var roleActions = dbContext.ApplicationRoleActionAuthorizations.Where(a => a.ApplicationRoleId == model.Id).ToList();

                var actions = model.ApplicationRoleActionAuthorizations.ToList();
                foreach (var item in actions)
                {
                    ApplicationRoleActionAuthorizations actionAuthorizations = new ApplicationRoleActionAuthorizations()
                    {
                        ApplicationRoleId = current.Id,
                        PermissionId = item.PermissionId,
                        forCreate = item.forCreate,
                        forEdit = item.forEdit,
                        forView = item.forView,
                        forDelete = item.forDelete,
                        forRestore = item.forRestore
                    };
                    if (actionAuthorizations.forCreate == true || actionAuthorizations.forView == true ||
                            actionAuthorizations.forEdit == true || actionAuthorizations.forDelete == true ||
                            actionAuthorizations.forRestore == true)
                    {
                        listAuth.Add(actionAuthorizations);
                    }
                    //dbContext.Add(actionAuthorizations);
                    //dbContext.SaveChanges();
                };
                if (listAuth.Count == 0)
                {
                    return new ResponseViewModels<RoleViewModel>
                    {
                        StatusCode = 400,
                        Message = "Hak Akses Tidak boleh kosong",
                        Data = null
                    };
                }
                foreach (var item in roleActions)
                {
                    dbContext.Remove(item);
                }
                dbContext.SaveChanges();
                dbContext.Roles.Update(current);
                dbContext.ApplicationRoleActionAuthorizations.AddRange(listAuth);
                dbContext.SaveChanges();

                var userAdditional = dbContext.UserRolesAdditionals.Where(a => a.PejabatRoleId == model.Id).ToList();
                foreach (var item in userAdditional)
                {
                    item.PejabatRoleId = model.Id;
                    item.PejabatRoleName = model.Name;
                    dbContext.UserRolesAdditionals.Update(item);
                    dbContext.SaveChanges();
                }

                var rolePermission = dbContext.Roles.Include("ApplicationRoleActionAuthorizations").Where(a => a.Id == model.Id).FirstOrDefault();
                return new ResponseViewModels<RoleViewModel>
                {
                    StatusCode = 201,
                    Message = "Data Successfully Saved",
                    Data = mapper.Map<RoleViewModel>(rolePermission)
                };
            }
            catch (Exception e)
            {
                return new ResponseViewModels<RoleViewModel>
                {
                    StatusCode = 400,
                    Message = "Failed To Save Data",
                    Data = null
                };
            }
        }
        public ResponseViewModels<PaginatedList<RoleViewModel>> GetAllRole(RoleViewModel vm, int? page = 1, int? perPage = 10, string orderBy = null, string order = null, string searchField = null, string queryString = null, bool? IncludeDeleted = null)
        {
            var response = new ResponseViewModels<PaginatedList<RoleViewModel>>();
            var query = dbContext.Set<ApplicationRole>().Include("ApplicationRoleActionAuthorizations").AsQueryable();
            if (IncludeDeleted == false || IncludeDeleted == null)
            {
                query = query.Where(a => a.IsDeleted == false);
            }
            if (IncludeDeleted == false)
            {
                query = query.Where(a => a.IsDeleted == true || a.IsDeleted == false);
            }
            if (!string.IsNullOrEmpty(queryString))
            {
                if (!string.IsNullOrEmpty(searchField))
                {
                    query = query.Where($"{searchField}.ToLower().Contains(@0)", queryString);
                }
                else
                {
                    string searchTemplate = "";
                    var fields = vm.GetSearchField();
                    foreach (var field in fields)
                    {
                        searchTemplate = string.IsNullOrEmpty(searchTemplate) ? $"{field}.ToLower().Contains(@0)" : $"{searchTemplate} or {field}.ToLower().Contains(@0)";
                    }

                    query = query.Where(searchTemplate, queryString);
                }
            }

            if (!string.IsNullOrEmpty(includes))
            {
                var includeList = includes.Split(',');
                foreach (var inc in includeList)
                {
                    query = query.Include(inc.Trim());
                }
            }

            var data = new List<RoleViewModel>();
            if (!string.IsNullOrEmpty(orderBy) && vm.GetProperties().Contains(orderBy))
            {
                data = mapper.Map<List<RoleViewModel>>(query.OrderBy(orderBy + " " + (order ?? "ascending")).ToDynamicList());
            }
            else
            {
                data = mapper.Map<List<RoleViewModel>>(query.OrderBy("CreatedDate " + (order ?? "ascending")).ToDynamicList());
            }

            if (data.Count == 0)
            {
                response.StatusCode = 204;
                response.Message = "No Data Retrieved";
            }
            else
            {
                response.StatusCode = 200;
                response.Message = "Data Retrieved";
            }

            response.Data = new PaginatedList<RoleViewModel>(data, data.Count, page ?? 1, perPage ?? 10);
            return response;
        }
        public async Task<ResponseViewModels<UserViewModel>> SwitchRoles(string Id)
        {
            string userId = GetUserId();
            var model = dbContext.UserRolesAdditionals.Where(a => a.LoginUserPersonId == userId).ToList();
            foreach (var item in model)
            {
                if (item.Id == Id)
                {
                    var vm = dbContext.UserRolesAdditionals.Where(a => a.Id == Id).FirstOrDefault();
                    if (vm != null)
                    {
                        vm.isActive = true;
                        dbContext.UserRolesAdditionals.Update(vm);
                        dbContext.SaveChanges();
                    }
                }
                else
                {
                    var uvm = dbContext.UserRolesAdditionals.Where(a => a.Id == item.Id).FirstOrDefault();
                    uvm.isActive = false;
                    dbContext.UserRolesAdditionals.Update(uvm);
                    dbContext.SaveChanges();
                }
            }
            ResponseViewModels<UserViewModel> response = new ResponseViewModels<UserViewModel>();
            response = await accSvc.GetUserByToken();
            return response;
        }
        public TokenResponse SwitchRolesV2(string Id)
        {
            string userId = GetUserId();
            var user = dbContext.Users.Where(a => a.Id == userId).FirstOrDefault();
            var listModel = dbContext.UserRolesAdditionals.Where(a => a.ParentUserId == user.Id && a.IsDeleted == false).ToList();
            string userLoginId = string.Empty;
            string userLoginToken = string.Empty;
            string userLoginRefreshToken = string.Empty;
            var switchRoles = dbContext.UserRolesAdditionals.Where(a => a.Id == Id).FirstOrDefault();

            foreach (var item in listModel)
            {
                if (item.Id == Id)
                {
                    var vm = dbContext.UserRolesAdditionals.Where(a => a.Id == Id).FirstOrDefault();
                    if (vm != null)
                    {
                        vm.isActive = true;
                        dbContext.UserRolesAdditionals.Update(vm);
                        dbContext.SaveChanges();
                    }
                }
                else
                {
                    var uvm = dbContext.UserRolesAdditionals.Where(a => a.Id == item.Id).FirstOrDefault();
                    uvm.isActive = false;
                    dbContext.UserRolesAdditionals.Update(uvm);
                    dbContext.SaveChanges();
                }
            }
            var revoke = authSvc.Revoke();
            var login = authSvc.AuthenticateV2(switchRoles.LoginUserPersonId);
            userLoginId = login.UserId;
            userLoginToken = login.Token;
            userLoginRefreshToken = login.RefreshToken;

            return new TokenResponse()
            {
                StatusCode = 200,
                Message = "Your Login is Successfull",
                Token = userLoginToken,
                Type = "Bearer",
                RefreshToken = userLoginRefreshToken,
                UserId = userLoginId
            };

        }
        public async Task<ResponseViewModels<UserRolesAdditionalViewModels>> RolesFromDelegation(string id)
        {
            var currents = dbContext.UserRolesAdditionals.Where(a => a.TypeId == id).ToList();

            //if (currents != null)
            //{
            //    dbContext.UserRolesAdditionals.RemoveRange(currents);
            //}
            UserRolesAdditionalViewModels model = new UserRolesAdditionalViewModels();
            var item = new DelegationViewModels();
            item = await getDelegation(id);
            var delegateUser = helperSvc.CreateUserDelegation(item);
            if (currents.Count() > 0) //jika sudah exist
            {
                try
                {
                    var pejabat = dbContext.Users.Where(a => a.Id == item.DelegateUserId).FirstOrDefault();
                    var user = dbContext.Users.Where(a => a.Id == item.Replacement1UserId).FirstOrDefault();
                    model.DelegationStartDate = item.startDate;
                    model.DelegationEndDate = item.endDate;
                    model.PejabatJabatanName = pejabat.PersonName;
                    model.PejabatRoleName = pejabat.RoleName;
                    model.PejabatJabatanName = pejabat.PositionName;

                    if (pejabat != null && user != null)
                    {
                        var vm = dbContext.UserRolesAdditionals.Where(a => a.TypeId == id && a.Order == 1).FirstOrDefault();
                        //vm.Id = Guid.NewGuid().ToString();
                        vm.LoginUserPersonId = user.Id;
                        vm.LoginUserPersonName = user.PersonName;
                        vm.LoginUserUnitKerja = user.UnitKerja;
                        vm.LoginUserUnitKerjaId = user.UnitKerjaId;
                        vm.PejabatRoleId = pejabat.RoleId;
                        vm.PejabatRoleName = pejabat.RoleName;
                        vm.PejabatId = delegateUser.Id;
                        vm.PejabatName = delegateUser.PersonName;
                        vm.PejabatJabatanId = delegateUser.RecentPositionId;
                        vm.PejabatJabatanName = "Pgs. " + item.DelegateJabatanName;
                        vm.PejabatBODLEvel = pejabat.BODLevel;
                        vm.DelegationStartDate = item.startDate;
                        vm.DelegationEndDate = item.endDate;
                        vm.SecretaryActive = false;
                        vm.Type = "Delegation";
                        vm.TypeId = item.Id;
                        vm.isActive = false;
                        vm.IsDeleted = false;
                        vm.Order = 1;
                        vm.ParentUserId = user.Id;
                        vm.DelegationUserId = pejabat.Id;
                        dbContext.UserRolesAdditionals.Update(vm);
                        dbContext.SaveChanges();

                        // delete pgs 2 jika awalnya ada  menjadi tidak ada
                        if (item.Replacement2UserId != string.Empty)
                        {
                            var exist2 = dbContext.UserRolesAdditionals.Where(a => a.TypeId == id && a.Order == 2).FirstOrDefault();
                            if (exist2 != null)
                            {
                                dbContext.UserRolesAdditionals.Remove(exist2);
                                dbContext.SaveChanges();
                            }
                        }

                        //jika pgs 2 ada
                        if (item.Replacement2UserId != string.Empty)
                        {
                            // cek exist
                            var user1 = dbContext.Users.Where(a => a.Id == item.Replacement2UserId).FirstOrDefault();
                            if (user1 != null)
                            {
                                var vm1 = dbContext.UserRolesAdditionals.Where(a => a.TypeId == id && a.Order == 2).FirstOrDefault();
                                if (vm1 != null) //jika exist maka update
                                {
                                    vm1.LoginUserPersonId = user1.Id;
                                    vm1.LoginUserPersonName = user1.PersonName;
                                    vm1.LoginUserUnitKerja = user1.UnitKerja;
                                    vm1.LoginUserUnitKerjaId = user1.UnitKerjaId;
                                    vm1.PejabatRoleId = pejabat.RoleId;
                                    vm1.PejabatRoleName = pejabat.RoleName;
                                    vm1.PejabatId = delegateUser.Id;
                                    vm1.PejabatName = delegateUser.PersonName;
                                    vm1.PejabatJabatanId = delegateUser.RecentPositionId;
                                    vm1.PejabatJabatanName = "Pgs. " + item.DelegateJabatanName;
                                    vm1.PejabatBODLEvel = pejabat.BODLevel;
                                    vm1.DelegationStartDate = item.startDate;
                                    vm1.DelegationEndDate = item.endDate;
                                    vm1.isActive = false;
                                    vm1.IsDeleted = false;
                                    vm1.SecretaryActive = false;
                                    vm1.Type = "Delegation";
                                    vm1.TypeId = item.Id;
                                    vm1.Order = 2;
                                    vm1.ParentUserId = user1.Id;
                                    vm1.DelegationUserId = pejabat.Id;
                                    dbContext.UserRolesAdditionals.Update(vm1);
                                    dbContext.SaveChanges();
                                }
                                else // jika null maka buat baru
                                {
                                    var newVm1 = new UserRolesAdditional();
                                    newVm1.Id = Guid.NewGuid().ToString();
                                    newVm1.LoginUserPersonId = user1.Id;
                                    newVm1.LoginUserPersonName = user1.PersonName;
                                    newVm1.LoginUserUnitKerja = user1.UnitKerja;
                                    newVm1.LoginUserUnitKerjaId = user1.UnitKerjaId;
                                    newVm1.PejabatRoleId = pejabat.RoleId;
                                    newVm1.PejabatRoleName = pejabat.RoleName;
                                    newVm1.PejabatId = delegateUser.Id;
                                    newVm1.PejabatName = delegateUser.PersonName;
                                    newVm1.PejabatJabatanId = delegateUser.RecentPositionId;
                                    newVm1.PejabatJabatanName = "Pgs. " + item.DelegateJabatanName;
                                    newVm1.PejabatBODLEvel = pejabat.BODLevel;
                                    newVm1.DelegationStartDate = item.startDate;
                                    newVm1.DelegationEndDate = item.endDate;
                                    newVm1.isActive = false;
                                    newVm1.IsDeleted = false;
                                    newVm1.SecretaryActive = false;
                                    newVm1.Type = "Delegation";
                                    newVm1.TypeId = item.Id;
                                    newVm1.Order = 2;
                                    newVm1.ParentUserId = user1.Id;
                                    newVm1.DelegationUserId = pejabat.Id;
                                    dbContext.UserRolesAdditionals.Add(newVm1);
                                    dbContext.SaveChanges();
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    return new ResponseViewModels<UserRolesAdditionalViewModels>()
                    {
                        StatusCode = 400,
                        Message = "Failed to Save Data",
                        Data = null
                    };
                }
            }
            else //jika belum ada maka buat baru
            {
                try
                {
                    var pejabat = dbContext.Users.Where(a => a.Id == item.DelegateUserId).FirstOrDefault();
                    var user = dbContext.Users.Where(a => a.Id == item.Replacement1UserId).FirstOrDefault();
                    model.DelegationStartDate = item.startDate;
                    model.DelegationEndDate = item.endDate;
                    model.PejabatJabatanName = pejabat.PersonName;
                    model.PejabatRoleName = pejabat.RoleName;
                    model.PejabatJabatanName = pejabat.PositionName;

                    if (pejabat != null && user != null)
                    {
                        var vm = new UserRolesAdditional();
                        vm.Id = Guid.NewGuid().ToString();
                        vm.LoginUserPersonId = user.Id;
                        vm.LoginUserPersonName = user.PersonName;
                        vm.LoginUserUnitKerja = user.UnitKerja;
                        vm.LoginUserUnitKerjaId = user.UnitKerjaId;
                        vm.PejabatRoleId = pejabat.RoleId;
                        vm.PejabatRoleName = pejabat.RoleName;
                        vm.PejabatId = delegateUser.Id;
                        vm.PejabatName = delegateUser.PersonName;
                        vm.PejabatJabatanId = delegateUser.RecentPositionId;
                        vm.PejabatJabatanName = "Pgs. " + item.DelegateJabatanName;
                        vm.PejabatBODLEvel = pejabat.BODLevel;
                        vm.DelegationStartDate = item.startDate;
                        vm.DelegationEndDate = item.endDate;
                        vm.SecretaryActive = false;
                        vm.Type = "Delegation";
                        vm.TypeId = item.Id;
                        vm.isActive = false;
                        vm.IsDeleted = false;
                        vm.Order = 1;
                        vm.ParentUserId = user.Id;
                        vm.DelegationUserId = pejabat.Id;
                        dbContext.UserRolesAdditionals.Add(vm);

                        dbContext.SaveChanges();

                        if (item.Replacement2UserId != string.Empty)
                        {
                            var user1 = dbContext.Users.Where(a => a.Id == item.Replacement2UserId).FirstOrDefault();
                            if (user1 != null)
                            {
                                var vm1 = new UserRolesAdditional();
                                vm1.Id = Guid.NewGuid().ToString();
                                vm1.LoginUserPersonId = user1.Id;
                                vm1.LoginUserPersonName = user1.PersonName;
                                vm1.LoginUserUnitKerja = user1.UnitKerja;
                                vm1.LoginUserUnitKerjaId = user1.UnitKerjaId;
                                vm1.PejabatRoleId = pejabat.RoleId;
                                vm1.PejabatRoleName = pejabat.RoleName;
                                vm1.PejabatId = delegateUser.Id;
                                vm1.PejabatName = delegateUser.PersonName;
                                vm1.PejabatJabatanId = delegateUser.RecentPositionId;
                                vm1.PejabatJabatanName = "Pgs. " + item.DelegateJabatanName;
                                vm1.PejabatBODLEvel = pejabat.BODLevel;
                                vm1.DelegationStartDate = item.startDate;
                                vm1.DelegationEndDate = item.endDate;
                                vm1.isActive = false;
                                vm1.IsDeleted = false;
                                vm1.SecretaryActive = false;
                                vm1.Type = "Delegation";
                                vm1.TypeId = item.Id;
                                vm1.Order = 2;
                                vm1.ParentUserId = user1.Id;
                                vm1.DelegationUserId = pejabat.Id;
                                dbContext.UserRolesAdditionals.Add(vm1);
                                dbContext.SaveChanges();
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    return new ResponseViewModels<UserRolesAdditionalViewModels>()
                    {
                        StatusCode = 400,
                        Message = "Failed to Save Data",
                        Data = null
                    };
                }
            }

            return new ResponseViewModels<UserRolesAdditionalViewModels>()
            {
                StatusCode = 200,
                Message = "Data Retrieved",
                Data = model
            };
        }
        public async Task<DelegationViewModels> getDelegation(string id)
        {
            var token = GetUserToken();
            string url = _apiEndpoint.EntryDelegation + id;
            var client = new RestClient($"{url}");
            var req = new RestRequest(Method.GET);
            req.AddHeader("Authorization", $"Bearer {token}");
            var responseApi = await client.ExecuteAsync(req);
            var value = JObject.Parse(responseApi.Content);
            var item = value.GetValue("data");
            DelegationViewModels vm = new DelegationViewModels
            {
                Id = item["id"].ToString(),
                DelegateUserId = item["delegateUserId"].ToString(),
                DelegateUserName = item["delegateUserName"].ToString(),
                DelegateJabatanId = (double?)item["delegateJabatanId"],
                DelegateJabatanName = item["delegateJabatanName"].ToString(),
                Replacement1UserId = item["replacement1UserId"].ToString(),
                Replacement1UserName = item["replacement1UserName"].ToString(),
                Replacement1JabatanId = (double?)item["replacement1JabatanId"],
                Replacement1JabatanName = item["replacement1JabatanName"].ToString(),
                Replacement2UserId = item["replacement2UserId"].ToString(),
                Replacement2UserName = item["replacement2UserName"].ToString(),
                Replacement2JabatanId = (double?)item["replacement2JabatanId"],
                Replacement2JabatanName = item["replacement2JabatanName"].ToString(),
                startDate = DateTime.Parse(item["startDate"].ToString()),
                endDate = DateTime.Parse(item["endDate"].ToString())
            };
            return vm;
        }
        public ResponseViewModels<UserRolesAdditionalViewModels> RolesFromSecretary(string Id, string userPejabatId, string secretaryId, bool isActive)
        {
            var model = dbContext.UserRolesAdditionals.Where(a => a.TypeId == Id).FirstOrDefault();

            UserRolesAdditionalViewModels uservm = new UserRolesAdditionalViewModels();
            var pejabat = dbContext.Users.Where(a => a.Id == userPejabatId).FirstOrDefault();
            uservm.PejabatJabatanName = pejabat.PositionName;
            uservm.PejabatName = pejabat.PersonName;
            uservm.PejabatRoleName = pejabat.RoleName;
            try
            {
                if (model != null)
                {
                    var user = dbContext.Users.Where(a => a.Id == secretaryId).FirstOrDefault();
                    model.LoginUserPersonId = user.Id;
                    model.LoginUserPersonName = user.PersonName;
                    model.LoginUserUnitKerja = user.UnitKerja;
                    model.LoginUserUnitKerjaId = user.UnitKerjaId;
                    model.PejabatRoleId = pejabat.RoleId;
                    model.PejabatRoleName = pejabat.RoleName;
                    model.PejabatId = pejabat.Id;
                    model.PejabatName = pejabat.PersonName;
                    model.PejabatJabatanId = pejabat.RecentPositionId;
                    model.PejabatJabatanName = "Skr. " + pejabat.PositionName;
                    model.PejabatBODLEvel = pejabat.BODLevel;
                    model.DelegationStartDate = null;
                    model.DelegationEndDate = null;
                    model.isActive = false;
                    model.SecretaryActive = isActive;
                    model.UpdatedDate = DateTime.Now;
                    model.ParentUserId = model.LoginUserPersonId;
                    dbContext.UserRolesAdditionals.Update(model);
                    dbContext.SaveChanges();
                }
                else
                {
                    var user = dbContext.Users.Where(a => a.Id == secretaryId).FirstOrDefault();
                    var vm = new UserRolesAdditional();
                    vm.Id = Guid.NewGuid().ToString();
                    vm.TypeId = Id;
                    vm.Type = "Secretary";
                    vm.LoginUserPersonId = user.Id;
                    vm.LoginUserPersonName = user.PersonName;
                    vm.LoginUserUnitKerja = user.UnitKerja;
                    vm.LoginUserUnitKerjaId = user.UnitKerjaId;
                    vm.PejabatRoleId = pejabat.RoleId;
                    vm.PejabatRoleName = pejabat.RoleName;
                    vm.PejabatId = pejabat.Id;
                    vm.PejabatName = pejabat.PersonName;
                    vm.PejabatJabatanId = pejabat.RecentPositionId;
                    vm.PejabatJabatanName = "Skr. " + pejabat.PositionName;
                    vm.PejabatBODLEvel = pejabat.BODLevel;
                    vm.DelegationStartDate = null;
                    vm.DelegationEndDate = null;
                    vm.isActive = false;
                    vm.SecretaryActive = isActive;
                    vm.CreatedDate = DateTime.Now;
                    vm.ParentUserId = user.Id;
                    dbContext.UserRolesAdditionals.Add(vm);
                    dbContext.SaveChanges();
                }
            }
            catch (Exception e)
            {
                return new ResponseViewModels<UserRolesAdditionalViewModels>()
                {
                    StatusCode = 400,
                    Message = "Failed to Save Data",
                    Data = null
                };
            }

            return new ResponseViewModels<UserRolesAdditionalViewModels>()
            {
                StatusCode = 200,
                Message = "Data Retrieved",
                Data = uservm
            };

        }
        public ResponseViewModels<UserRolesAdditionalViewModels> DeleteUserRolesAdditional(string Id)
        {
            try
            {
                var model = dbContext.UserRolesAdditionals.Where(a => a.TypeId == Id).ToList();
                if (model.Count > 0)
                {
                    foreach (var item in model)
                    {
                        //var users = dbContext.Users.Where(a => a.Id == item.LoginUserPersonId).FirstOrDefault();
                        //users.IsActive = false;
                        //dbContext.Users.Update(users);
                        item.isActive = false;
                        item.IsDeleted = true;
                        dbContext.UserRolesAdditionals.Update(item);
                        dbContext.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                return new ResponseViewModels<UserRolesAdditionalViewModels>()
                {
                    StatusCode = 400,
                    Message = "Failed to Save Data",
                    Data = null
                };
            }
            return new ResponseViewModels<UserRolesAdditionalViewModels>()
            {
                StatusCode = 201,
                Message = "Deleted Data Successfuly",
                Data = null
            };
        }
        public ResponseViewModels<UserRolesAdditionalViewModels> RestoreUserRolesAdditional(string Id)
        {
            try
            {
                var model = dbContext.UserRolesAdditionals.Where(a => a.TypeId == Id).ToList();
                if (model.Count > 0)
                {
                    foreach (var item in model)
                    {
                        item.IsDeleted = false;
                        dbContext.UserRolesAdditionals.Update(item);
                        dbContext.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                return new ResponseViewModels<UserRolesAdditionalViewModels>()
                {
                    StatusCode = 400,
                    Message = "Failed to Save Data",
                    Data = null
                };
            }
            return new ResponseViewModels<UserRolesAdditionalViewModels>()
            {
                StatusCode = 201,
                Message = "Deleted Data Successfuly",
                Data = null
            };
        }
        #endregion
    }
}
