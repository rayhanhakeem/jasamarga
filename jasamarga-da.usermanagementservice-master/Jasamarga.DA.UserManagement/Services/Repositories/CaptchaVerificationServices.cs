﻿using Jasamarga.DA.UserManagement.Services.Contracts;
using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Services
{
    public class CaptchaVerificationServices : ICaptchaVerificationServices
    {
        private CaptchaSettings captchaSettings;
        private ILogger<CaptchaVerificationServices> logger;

        public string ClientKey => captchaSettings.SecretKey;

        public CaptchaVerificationServices( IOptions<CaptchaSettings> captchaSettings, ILogger<CaptchaVerificationServices> logger)
        {
            this.captchaSettings = captchaSettings.Value;
            this.logger = logger;
        }

        public dynamic IsCaptchaValid(string token)
        {
            HttpClient httpClient = new HttpClient();
            var httpResponse = httpClient.GetAsync($"https://www.google.com/recaptcha/api/siteverify?secret={captchaSettings.SecretKey}&response={token}").Result;
            if (httpResponse.StatusCode != HttpStatusCode.OK)
            {
                return false;
            }
            string jsonResponse = httpResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonData = JObject.Parse(jsonResponse);
            if (jsonData.success != true.ToString().ToLower())
            {
                return jsonData;
            }
            return jsonData;
        }
    }
}
