﻿using Jasamarga.DA.UserManagement.Databases;
using Jasamarga.DA.UserManagement.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Services.Repositories
{
    public class TokenServices : ITokenServices
    {
        private string _token { set; get; }
        private readonly IHttpContextAccessor _httpContextAccessor;
        private static HttpClient client = new HttpClient();
        protected ApplicationDbContext dbContext { set; get; }

        public TokenServices(IHttpContextAccessor httpContextAccessor, ApplicationDbContext dbContext)
        {
            _httpContextAccessor = httpContextAccessor;
            this.dbContext = dbContext;
        }

        public string GetUserId()
        {
            var tokenString = GetUserToken();
            if (string.IsNullOrEmpty(tokenString))
            {
                return "unauthorized";
            }
            var token = new JwtSecurityTokenHandler().ReadJwtToken(tokenString);
            return token?.Subject;
        }

        public string GetPersonName()
        {
            var userId = GetUserId();
            if (string.IsNullOrEmpty(userId))
            {
                return null;
            }
            var model = dbContext.Users.Where(a => a.Id == userId).FirstOrDefault();
            return model.PersonName;
            //string personName = string.Empty;
            //HttpResponseMessage response = client.GetAsync("https://localhost:44373/api/account/getpersonname/" + userId).Result;
            //var result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            //string personName = result["personName"].ToString().Trim();
            //return personName;
        }
        public string GetUserToken()
        {
            var context = _httpContextAccessor.HttpContext;
            var authHeader = context.Request.Headers["Authorization"].ToString();
            if (authHeader != null && authHeader.StartsWith("Bearer", StringComparison.OrdinalIgnoreCase))
            {
                var token = authHeader.Substring("Bearer ".Length).Trim();
                return token;
            }

            return null;
        }

        public void SetToken(string token)
        {

        }
    }
}
