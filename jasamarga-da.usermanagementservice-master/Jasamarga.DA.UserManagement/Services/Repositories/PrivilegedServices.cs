﻿using Hanssens.Net;
using Jasamarga.DA.UserManagement.Databases;
using Jasamarga.DA.UserManagement.Databases.Identity;
using Jasamarga.DA.UserManagement.Services.Contracts;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Jasamarga.DA.UserManagement.Services.Repositories
{
    public class PrivilegedServices : TokenServices, IPrivelegedService
    {
        public PrivilegedServices(ApplicationDbContext dbContext, IHttpContextAccessor accessor) : base(accessor, dbContext)
        {
            //this.mapper = mapper;
            this.dbContext = dbContext;
        }

        public void DeletePrivileged()
        {
            var storage = new LocalStorage();
            storage.Clear();
        }

        public List<string> GetPrivileged(/*string url, string id*/)
        {
            SavePrivileged();
            List<string> listPriviliged = new List<string>();
            // initialize, with default settings
            var storage = new LocalStorage();
            var list = storage.Get("listPriviliged");
            //var result = ((IEnumerable)list).Cast<object>().ToList();

            if (typeof(IEnumerable<object>).IsAssignableFrom(list.GetType()))
            {
                List<object> listO = ((IEnumerable<object>)list).ToList();
                // Test it
                foreach (var v in listO)
                {
                    listPriviliged.Add(v.ToString());
                }
            }
            return listPriviliged;
            //listPriviliged.Add(result);
        }

        //public List<string> GetPrivileged(/*string url, string id*/)
        //{
        //    SavePrivileged();
        //    List<string> listPriviliged = new List<string>();
        //    // initialize, with default settings
        //    var storage = new LocalStorage();
        //    var list = storage.Get("listPriviliged");
        //    //var result = ((IEnumerable)list).Cast<object>().ToList();

        //    if (typeof(IEnumerable<object>).IsAssignableFrom(list.GetType()))
        //    {
        //        List<object> listO = ((IEnumerable<object>)list).ToList();
        //        // Test it
        //        foreach (var v in listO)
        //        {
        //            listPriviliged.Add(v.ToString());
        //        }
        //    }
        //    return listPriviliged;
        //    //listPriviliged.Add(result);
        //}

        public void SavePrivileged()
        {
            string Id = GetUserId();
            var model = dbContext.Users.Include("UserRoles").Where(a => a.Id == Id).FirstOrDefault();
            //List<ApplicationRole> roles = null;
            var action = dbContext.ApplicationRoleActionAuthorizations.Include("Permission").Where(a => a.ApplicationRoleId == model.RoleId).ToList();
            //roles = dbContext.Roles.Include("UserRoles").Include("ApplicationRoleActionAuthorizations").Where(a => a.Id == model.RoleId).ToList();
            List<string> priviligedlist = new List<string>();
            foreach (var item in action)
            {
                if (item.forView == true)
                {
                    priviligedlist.Add(item.Permission.Url);
                }
                if (item.forCreate == true)
                {
                    priviligedlist.Add(item.Permission.Url + "/tambah");
                }
                if (item.forEdit == true)
                {
                    priviligedlist.Add(item.Permission.Url + "/edit");
                }
                if (item.forDelete == true)
                {
                    priviligedlist.Add(item.Permission.Url + "/delete");
                }
                if (item.forRestore == true)
                {
                    priviligedlist.Add(item.Permission.Url + "/restore");
                }
            }
            using (var storage = new LocalStorage())
            {
                storage.Store("listPriviliged", priviligedlist);
            }
        }

        public PrivilegedViewModels ListPrivileged()
        {
            var privileged = new PrivilegedViewModels();
            string Id = GetUserId();
            var model = dbContext.Users.Include("UserRoles").Where(a => a.Id == Id).FirstOrDefault();
            //List<ApplicationRole> roles = null;
            var action = dbContext.ApplicationRoleActionAuthorizations.Include("Permission").Where(a => a.ApplicationRoleId == model.RoleId).ToList();
            //roles = dbContext.Roles.Include("UserRoles").Include("ApplicationRoleActionAuthorizations").Where(a => a.Id == model.RoleId).ToList();
            List<string> priviligedlist = new List<string>();
            foreach (var item in action)
            {
                if (item.forView == true)
                {
                    priviligedlist.Add(item.Permission.Url);
                }
                if (item.forCreate == true)
                {
                    priviligedlist.Add(item.Permission.Url + "/tambah");
                }
                if (item.forEdit == true)
                {
                    priviligedlist.Add(item.Permission.Url + "/edit");
                }
                if (item.forDelete == true)
                {
                    priviligedlist.Add(item.Permission.Url + "/delete");
                }
                if (item.forRestore == true)
                {
                    priviligedlist.Add(item.Permission.Url + "/restore");
                }
            }
            privileged.RoleId = model.RoleId;
            privileged.RoleName = model.RoleName;
            privileged.PrivilegedList = priviligedlist;
            return privileged;
        }
        public ResponsePrivileged Validation(string url)
        {
            Match guids = Regex.Match(url, @"(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}");
            string Match = guids.Value;
            string result = url.Replace(Match,"");
            result = result.Replace("//","/");
            result = result.Replace("?itemId=", "");
            var privileged = ListPrivileged();
            List<string> list = privileged.PrivilegedList;
            //var isContained = 
            if (list.Any(a => a == result))
            {
                return new ResponsePrivileged
                {
                    StatusCode = 201,
                    Message = "Authorized"
                };
            }
            else
            {
                return new ResponsePrivileged
                {
                    StatusCode = 401,
                    Message = "Unauthorized"
                };
            }
        }

        public PrivilegedViewModels ListPrivilegedByRoles(string id)
        {
            var privileged = new PrivilegedViewModels();
            var model = dbContext.Roles.Where(a => a.Id == id).FirstOrDefault();
            var action = dbContext.ApplicationRoleActionAuthorizations.Include("Permission").Where(a => a.ApplicationRoleId == id).ToList();
            List<string> priviligedlist = new List<string>();
            foreach (var item in action)
            {
                if (item.forView == true)
                {
                    priviligedlist.Add(item.Permission.Url);
                }
                if (item.forCreate == true)
                {
                    priviligedlist.Add(item.Permission.Url + "/tambah");
                }
                if (item.forEdit == true)
                {
                    priviligedlist.Add(item.Permission.Url + "/edit");
                }
                if (item.forDelete == true)
                {
                    priviligedlist.Add(item.Permission.Url + "/delete");
                }
                if (item.forRestore == true)
                {
                    priviligedlist.Add(item.Permission.Url + "/restore");
                }
            }
            privileged.RoleId = model.Id;
            privileged.RoleName = model.Name;
            privileged.PrivilegedList = priviligedlist;
            return privileged;
        }
    }
}
