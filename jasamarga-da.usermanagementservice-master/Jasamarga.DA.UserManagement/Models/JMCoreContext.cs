﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Jasamarga.DA.UserManagement.Models
{
    public partial class JMCoreContext : DbContext
    {
        public JMCoreContext()
        {
        }

        public JMCoreContext(DbContextOptions<JMCoreContext> options)
            : base(options)
        {
        }

        public virtual DbSet<JmEmployee> JmEmployee { get; set; }
        public virtual DbSet<JmHistoryJabatan> JmHistoryJabatan { get; set; }
        public virtual DbSet<JmHistoryJabatan2> JmHistoryJabatan2 { get; set; }
        public virtual DbSet<JmOrgHierarchy> JmOrgHierarchy { get; set; }
        public virtual DbSet<JmSapHierarchy> JmSapHierarchy { get; set; }
        public virtual DbSet<JmUnit> JmUnit { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Server=10.1.3.47;Port=5432;Database=jm_core_data;User Id=suitmedia;Password=5U1tM3di4;Timeout=15;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<JmEmployee>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("jm_employee");

                entity.Property(e => e.Baris)
                    .HasColumnName("baris")
                    .HasMaxLength(60);

                entity.Property(e => e.BusinessArea)
                    .HasColumnName("business_area")
                    .HasMaxLength(4);

                entity.Property(e => e.CntSubOrd).HasColumnName("cnt_sub_ord");

                entity.Property(e => e.CompanyCode)
                    .HasColumnName("company_code")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(10);

                entity.Property(e => e.CreatedDate).HasColumnName("created_date");

                entity.Property(e => e.DataSource)
                    .HasColumnName("data_source")
                    .HasMaxLength(10);

                entity.Property(e => e.DateOfBirth).HasColumnName("date_of_birth");

                entity.Property(e => e.EmailAddress)
                    .HasColumnName("email_address")
                    .HasMaxLength(100);

                entity.Property(e => e.EmpStatus)
                    .HasColumnName("emp_status")
                    .HasMaxLength(4);

                entity.Property(e => e.EmpType).HasColumnName("emp_type");

                entity.Property(e => e.EmployeeGroup).HasColumnName("employee_group");

                entity.Property(e => e.EmployeeNumber)
                    .HasColumnName("employee_number")
                    .HasMaxLength(30);

                entity.Property(e => e.EmployeeSubgroup)
                    .HasColumnName("employee_subgroup")
                    .HasMaxLength(2);

                entity.Property(e => e.Golongan)
                    .HasColumnName("golongan")
                    .HasMaxLength(60);

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Isldap).HasColumnName("isldap");

                entity.Property(e => e.JenisNpwp)
                    .HasColumnName("jenis_npwp")
                    .HasMaxLength(150);

                entity.Property(e => e.JobId).HasColumnName("job_id");

                entity.Property(e => e.JobName)
                    .HasColumnName("job_name")
                    .HasMaxLength(700);

                entity.Property(e => e.JobType)
                    .HasColumnName("job_type")
                    .HasMaxLength(150);

                entity.Property(e => e.LastUpdatedBy)
                    .HasColumnName("last_updated_by")
                    .HasMaxLength(10);

                entity.Property(e => e.LastUpdatedDate).HasColumnName("last_updated_date");

                entity.Property(e => e.LocationId).HasColumnName("location_id");

                entity.Property(e => e.LocationName)
                    .HasColumnName("location_name")
                    .HasMaxLength(150);

                entity.Property(e => e.MainRole).HasColumnName("main_role");

                entity.Property(e => e.MaritalStatus)
                    .HasColumnName("marital_status")
                    .HasMaxLength(50);

                entity.Property(e => e.NationalIdentifier)
                    .HasColumnName("national_identifier")
                    .HasMaxLength(30);

                entity.Property(e => e.NomorSk)
                    .HasColumnName("nomor_sk")
                    .HasMaxLength(150);

                entity.Property(e => e.NppAbsen)
                    .HasColumnName("npp_absen")
                    .HasMaxLength(10);

                entity.Property(e => e.Npwp)
                    .HasColumnName("npwp")
                    .HasMaxLength(150);

                entity.Property(e => e.OrgGrandParent).HasColumnName("org_grand_parent");

                entity.Property(e => e.OrgGrandParentName)
                    .HasColumnName("org_grand_parent_name")
                    .HasMaxLength(240);

                entity.Property(e => e.OrgIdParent).HasColumnName("org_id_parent");

                entity.Property(e => e.OrgName)
                    .HasColumnName("org_name")
                    .HasMaxLength(240);

                entity.Property(e => e.OrgParentName)
                    .HasColumnName("org_parent_name")
                    .HasMaxLength(240);

                entity.Property(e => e.OrganizationId).HasColumnName("organization_id");

                entity.Property(e => e.OriginalDateOfHire).HasColumnName("original_date_of_hire");

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(50);

                entity.Property(e => e.PayScaleArea)
                    .HasColumnName("pay_scale_area")
                    .HasMaxLength(2);

                entity.Property(e => e.PayrollGroupId).HasColumnName("payroll_group_id");

                entity.Property(e => e.PersonName)
                    .HasColumnName("person_name")
                    .HasMaxLength(150);

                entity.Property(e => e.PersonNumber).HasColumnName("person_number");

                entity.Property(e => e.PersonNumberApprover).HasColumnName("person_number_approver");

                entity.Property(e => e.PersonalArea).HasColumnName("personal_area");

                entity.Property(e => e.PersonalSubArea)
                    .HasColumnName("personal_sub_area")
                    .HasMaxLength(4);

                entity.Property(e => e.PersonnelNumberSap).HasColumnName("personnel_number_sap");

                entity.Property(e => e.PositionIdAtasan).HasColumnName("position_id_atasan");

                entity.Property(e => e.PositionName)
                    .HasColumnName("position_name")
                    .HasMaxLength(240);

                entity.Property(e => e.PositionNameAlias)
                    .HasColumnName("position_name_alias")
                    .HasMaxLength(150);

                entity.Property(e => e.PositionNameAtasan)
                    .HasColumnName("position_name_atasan")
                    .HasMaxLength(240);

                entity.Property(e => e.RecentPositionId).HasColumnName("recent_position_id");

                entity.Property(e => e.Religion)
                    .HasColumnName("religion")
                    .HasMaxLength(20);

                entity.Property(e => e.Ruang)
                    .HasColumnName("ruang")
                    .HasMaxLength(30);

                entity.Property(e => e.Sex)
                    .HasColumnName("sex")
                    .HasMaxLength(30);

                entity.Property(e => e.StatusNpwp)
                    .HasColumnName("status_npwp")
                    .HasMaxLength(150);

                entity.Property(e => e.StatusSap)
                    .HasColumnName("status_sap")
                    .HasMaxLength(2);

                entity.Property(e => e.StatusSat)
                    .HasColumnName("status_sat")
                    .HasMaxLength(2);

                entity.Property(e => e.TanggalEfektifSk)
                    .HasColumnName("tanggal_efektif_sk")
                    .HasMaxLength(150);

                entity.Property(e => e.TanggalSk)
                    .HasColumnName("tanggal_sk")
                    .HasMaxLength(150);

                entity.Property(e => e.TownOfBirth)
                    .HasColumnName("town_of_birth")
                    .HasMaxLength(90);

                entity.Property(e => e.UnitKerja)
                    .HasColumnName("unit_kerja")
                    .HasMaxLength(60);

                entity.Property(e => e.UnitKerjaId).HasColumnName("unit_kerja_id");

                entity.Property(e => e.UrlImage)
                    .HasColumnName("url_image")
                    .HasMaxLength(500);

                entity.Property(e => e.ValidFrom).HasColumnName("valid_from");

                entity.Property(e => e.ValidTo).HasColumnName("valid_to");
            });

            modelBuilder.Entity<JmHistoryJabatan>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("jm_history_jabatan");

                entity.Property(e => e.AtasanId).HasColumnName("atasan_id");

                entity.Property(e => e.AtasanPositionId).HasColumnName("atasan_position_id");

                entity.Property(e => e.EndDate).HasColumnName("end_date");

                entity.Property(e => e.Grade)
                    .HasColumnName("grade")
                    .HasMaxLength(20);

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.KdComp)
                    .HasColumnName("kd_comp")
                    .HasMaxLength(255);

                entity.Property(e => e.LocationId).HasColumnName("location_id");

                entity.Property(e => e.Npp)
                    .HasColumnName("npp")
                    .HasMaxLength(30);

                entity.Property(e => e.Organization)
                    .HasColumnName("organization")
                    .HasMaxLength(255);

                entity.Property(e => e.OrganizationId)
                    .HasColumnName("organization_id")
                    .HasMaxLength(255);

                entity.Property(e => e.Position)
                    .HasColumnName("POSITION")
                    .HasMaxLength(255);

                entity.Property(e => e.PositionId).HasColumnName("position_id");

                entity.Property(e => e.StartDate).HasColumnName("start_date");

                entity.Property(e => e.SubGrade)
                    .HasColumnName("sub_grade")
                    .HasMaxLength(20);

                entity.Property(e => e.UnitKerja)
                    .HasColumnName("unit_kerja")
                    .HasMaxLength(255);

                entity.Property(e => e.UnitKerjaId)
                    .HasColumnName("unit_kerja_id")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<JmHistoryJabatan2>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("jm_history_jabatan2");

                entity.Property(e => e.AtasanId).HasColumnName("atasan_id");

                entity.Property(e => e.AtasanPositionId).HasColumnName("atasan_position_id");

                entity.Property(e => e.EndDate).HasColumnName("end_date");

                entity.Property(e => e.Grade)
                    .HasColumnName("grade")
                    .HasMaxLength(20);

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("numeric(19,0)");

                entity.Property(e => e.KdComp)
                    .HasColumnName("kd_comp")
                    .HasMaxLength(255);

                entity.Property(e => e.LocationId).HasColumnName("location_id");

                entity.Property(e => e.Npp)
                    .HasColumnName("npp")
                    .HasMaxLength(30);

                entity.Property(e => e.Organization)
                    .HasColumnName("organization")
                    .HasMaxLength(255);

                entity.Property(e => e.OrganizationId)
                    .HasColumnName("organization_id")
                    .HasMaxLength(255);

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasMaxLength(255);

                entity.Property(e => e.PositionId)
                    .HasColumnName("position_id")
                    .HasColumnType("numeric(19,0)");

                entity.Property(e => e.StartDate).HasColumnName("start_date");

                entity.Property(e => e.SubGrade)
                    .HasColumnName("sub_grade")
                    .HasMaxLength(20);

                entity.Property(e => e.UnitKerja)
                    .HasColumnName("unit_kerja")
                    .HasMaxLength(255);

                entity.Property(e => e.UnitKerjaId)
                    .HasColumnName("unit_kerja_id")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<JmOrgHierarchy>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("jm_org_hierarchy");

                entity.Property(e => e.HeadCount).HasColumnName("head_count");

                entity.Property(e => e.HeadFlag)
                    .HasColumnName("head_flag")
                    .HasMaxLength(1);

                entity.Property(e => e.OrgId).HasColumnName("org_id");

                entity.Property(e => e.OrgName)
                    .HasColumnName("org_name")
                    .HasMaxLength(100);

                entity.Property(e => e.ParentOrgId).HasColumnName("parent_org_id");

                entity.Property(e => e.PersonnelNumberSap).HasColumnName("personnel_number_sap");

                entity.Property(e => e.PositionId).HasColumnName("position_id");

                entity.Property(e => e.PositionName)
                    .HasColumnName("position_name")
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<JmSapHierarchy>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("jm_sap_hierarchy");

                entity.Property(e => e.Cnt).HasColumnName("cnt");

                entity.Property(e => e.OrgId)
                    .HasColumnName("org_id")
                    .HasMaxLength(20);

                entity.Property(e => e.OrgName)
                    .HasColumnName("org_name")
                    .HasMaxLength(100);

                entity.Property(e => e.ParentOrgId)
                    .HasColumnName("parent_org_id")
                    .HasMaxLength(20);

                entity.Property(e => e.PersonnelSap)
                    .HasColumnName("personnel_sap")
                    .HasMaxLength(20);

                entity.Property(e => e.PositionId)
                    .HasColumnName("position_id")
                    .HasMaxLength(20);

                entity.Property(e => e.PositionName)
                    .HasColumnName("position_name")
                    .HasMaxLength(100);

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(1);
            });

            modelBuilder.Entity<JmUnit>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("jm_unit");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(10);

                entity.Property(e => e.CreatedDate).HasColumnName("created_date");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdExt)
                    .HasColumnName("id_ext")
                    .HasMaxLength(20);

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.LastUpdatedBy)
                    .HasColumnName("last_updated_by")
                    .HasMaxLength(10);

                entity.Property(e => e.LastUpdatedDate).HasColumnName("last_updated_date");

                entity.Property(e => e.NamaUnit)
                    .HasColumnName("nama_unit")
                    .HasMaxLength(111);

                entity.Property(e => e.UnitType).HasColumnName("unit_type");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
