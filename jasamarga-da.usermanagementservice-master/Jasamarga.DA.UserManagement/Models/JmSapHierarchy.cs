﻿using System;
using System.Collections.Generic;

namespace Jasamarga.DA.UserManagement.Models
{
    public partial class JmSapHierarchy
    {
        public string OrgId { get; set; }
        public string OrgName { get; set; }
        public string ParentOrgId { get; set; }
        public string PositionId { get; set; }
        public string PositionName { get; set; }
        public string PersonnelSap { get; set; }
        public string Type { get; set; }
        public double? Cnt { get; set; }
    }
}
