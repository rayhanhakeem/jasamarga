﻿using System;
using System.Collections.Generic;

namespace Jasamarga.DA.UserManagement.Models
{
    public partial class JmUnit
    {
        public double? Id { get; set; }
        public string NamaUnit { get; set; }
        public string IdExt { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
        public string LastUpdatedBy { get; set; }
        public short? Isdeleted { get; set; }
        public short? UnitType { get; set; }
    }
}
