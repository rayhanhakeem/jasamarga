﻿using System;
using System.Collections.Generic;

namespace Jasamarga.DA.UserManagement.Models
{
    public partial class JmOrgHierarchy
    {
        public long? OrgId { get; set; }
        public string OrgName { get; set; }
        public int? ParentOrgId { get; set; }
        public int? PositionId { get; set; }
        public string PositionName { get; set; }
        public int? PersonnelNumberSap { get; set; }
        public string HeadFlag { get; set; }
        public short? HeadCount { get; set; }
    }
}
