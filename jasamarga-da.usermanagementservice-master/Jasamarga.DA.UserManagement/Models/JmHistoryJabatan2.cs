﻿using System;
using System.Collections.Generic;

namespace Jasamarga.DA.UserManagement.Models
{
    public partial class JmHistoryJabatan2
    {
        public decimal? Id { get; set; }
        public string Npp { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public long? AtasanId { get; set; }
        public long? AtasanPositionId { get; set; }
        public string Grade { get; set; }
        public string SubGrade { get; set; }
        public string UnitKerjaId { get; set; }
        public string UnitKerja { get; set; }
        public long? LocationId { get; set; }
        public string KdComp { get; set; }
        public string Position { get; set; }
        public string Organization { get; set; }
        public string OrganizationId { get; set; }
        public decimal? PositionId { get; set; }
    }
}
