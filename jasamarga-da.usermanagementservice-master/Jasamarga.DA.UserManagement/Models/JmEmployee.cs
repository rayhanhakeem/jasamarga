﻿using System;
using System.Collections.Generic;

namespace Jasamarga.DA.UserManagement.Models
{
    public partial class JmEmployee
    {
        public long? PersonNumber { get; set; }
        public string PersonName { get; set; }
        public string EmployeeNumber { get; set; }
        public DateTime? OriginalDateOfHire { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string TownOfBirth { get; set; }
        public string NationalIdentifier { get; set; }
        public string Sex { get; set; }
        public double? JobId { get; set; }
        public string JobName { get; set; }
        public string JobType { get; set; }
        public double? RecentPositionId { get; set; }
        public string PositionName { get; set; }
        public string PositionNameAlias { get; set; }
        public string PositionNameAtasan { get; set; }
        public string NomorSk { get; set; }
        public string TanggalSk { get; set; }
        public string TanggalEfektifSk { get; set; }
        public double? OrganizationId { get; set; }
        public string OrgName { get; set; }
        public double? OrgIdParent { get; set; }
        public string OrgParentName { get; set; }
        public double? OrgGrandParent { get; set; }
        public string OrgGrandParentName { get; set; }
        public string UnitKerja { get; set; }
        public double? LocationId { get; set; }
        public string LocationName { get; set; }
        public string Golongan { get; set; }
        public string Baris { get; set; }
        public string Ruang { get; set; }
        public string Npwp { get; set; }
        public string JenisNpwp { get; set; }
        public string StatusNpwp { get; set; }
        public string EmailAddress { get; set; }
        public string MaritalStatus { get; set; }
        public string Religion { get; set; }
        public string Password { get; set; }
        public double? Isldap { get; set; }
        public double? Id { get; set; }
        public string StatusSat { get; set; }
        public string StatusSap { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string LastUpdatedBy { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
        public double? MainRole { get; set; }
        public long? UnitKerjaId { get; set; }
        public string PayScaleArea { get; set; }
        public string UrlImage { get; set; }
        public double? PersonNumberApprover { get; set; }
        public long? PersonnelNumberSap { get; set; }
        public short? EmployeeGroup { get; set; }
        public short? CntSubOrd { get; set; }
        public short? PersonalArea { get; set; }
        public int? PositionIdAtasan { get; set; }
        public int? PayrollGroupId { get; set; }
        public string EmployeeSubgroup { get; set; }
        public string BusinessArea { get; set; }
        public string PersonalSubArea { get; set; }
        public string CompanyCode { get; set; }
        public short? EmpType { get; set; }
        public string NppAbsen { get; set; }
        public string EmpStatus { get; set; }
        public string DataSource { get; set; }
    }
}
