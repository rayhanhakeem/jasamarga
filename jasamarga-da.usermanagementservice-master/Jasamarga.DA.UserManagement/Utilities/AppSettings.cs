﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Utilities
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public int Expires { get; set; }
        public int ExpiresRefreshToken { get; set; }
        public string LDAPConnectionString { get; set; }
        public string UserDomainName { get; set; }
        public string LDAPUserName { get; set; }
        public string LDAPPassword { get; set; }
        public string SeedKey { set; get; }
        public string UploadPath { set; get; }
        public string Environtment { set; get; }
        public string AllowOrigins { set; get; }
        public string ServerConnString { set; get; }
        public string DataConnString { set; get; }
        public string DefaultPassword { get; set; }
        public string InaktifDefaultPassword { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public bool UseSsl { get; set; }
        public string DefaultSender { get; set; }
        public bool SendEmail { get; set; }
        public bool SendEmailToDefault { get; set; }
        public string DefaultEmailReceiver { get; set; }
        public string DirectLink { get; set; }
    }
}
