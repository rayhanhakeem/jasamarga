﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Utilities
{
    public class ApiEndpoint
    {
        public string EntryDelegation { get; set; }
        public string Delegation { get; set; }
        public string ActiveSecretary { get; set; }
        public string Secretary { get; set; }
        public string GroupUnit { get; set; }
        public string Notification { get; set; }
        public string JobMapping { get; set; }
        public string SendLog { get; set; }
    }
}
