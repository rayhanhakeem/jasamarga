﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Utilities
{
    public class AppActions
    {
        public const string Dashboard = nameof(Dashboard);
        public const string Naskah_Dinas_Tugas = nameof(Naskah_Dinas_Tugas);

        public const string Naskah_Dinas_Disposisi_Tugas = nameof(Naskah_Dinas_Disposisi_Tugas);
        public const string Naskah_Dinas_Review_Tugas = nameof(Naskah_Dinas_Review_Tugas);

        public const string Naskah_Dinas_Kotak_Masuk = nameof(Naskah_Dinas_Kotak_Masuk);

        public const string Dokumen_Aktif = nameof(Dokumen_Aktif);
        public const string Dokumen_Inaktif = nameof(Dokumen_Inaktif);
        public const string Pelaporan = nameof(Pelaporan);
        public const string Master_Data_Template_Naskah_Dinas = nameof(Master_Data_Template_Naskah_Dinas);
        public const string Master_Data_Jenis_Naskah_Dinas = nameof(Master_Data_Jenis_Naskah_Dinas);
        public const string Manajemen_User_Admin = nameof(Manajemen_User_Admin);
        public const string Manajemen_User_Role = nameof(Manajemen_User_Role);
        public const string Pengaturan = nameof(Pengaturan);
    }
}
