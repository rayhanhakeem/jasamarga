﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Utilities
{
    public class CaptchaSettings
    {
        public string SiteKey { get; set; }
        public string SecretKey { get; set; }
    }
}
