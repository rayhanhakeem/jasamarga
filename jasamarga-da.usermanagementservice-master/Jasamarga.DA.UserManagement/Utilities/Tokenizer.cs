﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Utilities
{
    public class Tokenizer
    {
        private HttpContext context;

        public Tokenizer(HttpContext context)
        {
            this.context = context;
        }

        public string GetTokenId()
        {
            var tokenString = GetTokenString();
            if (string.IsNullOrEmpty(tokenString))
            {
                return "unauthorized";
            }
            var token = new JwtSecurityTokenHandler().ReadJwtToken(tokenString);
            return token?.Id;
        }

        public string GetTokenString()
        {
            var authHeader = context.Request.Headers["Authorization"].ToString();
            if (authHeader != null && authHeader.StartsWith("Bearer", StringComparison.OrdinalIgnoreCase))
            {
                var token = authHeader.Substring("Bearer ".Length).Trim();
                return token;
            }

            return null;
        }
    }
}
