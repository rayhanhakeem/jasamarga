﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Utilities
{
    public class AppPermissions
    {
        public string Pencarian_Arsip = "A. Pencarian Arsip;/search";
        public string Dashboard = "B. Dashboard;/";
        public string Naskah_Dinas_Tugas = "C.01. Naskah Dinas - Tugas;/naskah-dinas/tugas";
        public string Naskah_Dinas_Kotak_Masuk = "C.02. Naskah Dinas - Kotak Masuk;/naskah-dinas/inbox";
        public string Naskah_Dinas_Kotak_Terkirim = "C.03. Naskah Dinas - Terkirim;/naskah-dinas/outbox";
        public string Naskah_Dinas_Kotak_Draft = "C.04. Naskah Dinas - Draft;/naskah-dinas/draft";
        public string Naskah_Dinas_Kelola_Label = "C.05. Naskah Dinas - Kelola Label;/naskah-dinas/kelola-label";
        public string Dokumen_Arsip_Aktif = "D.01. Dokumen - Arsip Aktif;/dokumen/arsip-aktif";
        public string Dokumen_Arsip_Inaktif = "D.02. Dokumen - Arsip Inaktif;/dokumen/arsip-inaktif";
        public string Dokumen_Permintaan_Akses = "D.03. Dokumen - Permintaan Akses;/dokumen/permintaan-akses";
        public string Dokumen_Pemberkasan = "D.04. Dokumen - Pemberkasan;/dokumen/pemberkasan";
        public string Pelaporan_Naskah_Masuk = "E.01. Pelaporan - Naskah Masuk;/pelaporan/naskah-masuk";
        public string Pelaporan_Naskah_Keluar = "E.02. Pelaporan - Naskah Keluar;/pelaporan/naskah-keluar";
        public string Pelaporan_Naskah_Dinas = "E.03. Pelaporan - Naskah Dinas;/pelaporan/naskah-dinas";
        public string Pelaporan_Arsip = "E.04. Pelaporan - Arsip;/pelaporan/arsip";
        public string Master_Data_Jenis_Arsip = "F.01. Master Data - Jenis Arsip;/master-data/jenis-arsip";
        public string Master_Data_Klasifikasi_Keamanan = "F.02. Master Data - Klasifikasi Keamanan;/master-data/klasifikasi-keamanan";
        public string Master_Data_Klasifikasi_Arsip = "F.03. Master Data - Klasifikasi Arsip;/master-data/klasifikasi-arsip";
        public string Master_Data_Jenis_Naskah_Dinas = "F.04. Master Data - Jenis Naskah Dinas;/master-data/jenis-naskah-dinas";
        public string Master_Data_Template_Naskah_Dinas = "F.05. Master Data - Template Naskah Dinas;/master-data/template-naskah-dinas";
        public string Master_Data_Faq = "F.06. Master Data - FAQ;/master-data/faq";
        public string Master_Data_Kode_Nomor_Naskah = "F.07. Master Data - Kode Nomor Naskah;/master-data/kode-nomor-naskah";
        public string Manajemen_User_Admin = "G.01. Manajemen User - Admin;/manajemen-user/admin";
        public string Manajemen_User_User = "G.02. Manajemen User - User;/manajemen-user/user";
        public string Manajemen_User_Role = "G.03. Manajemen User - Role;/manajemen-user/role";
        public string Manajemen_User_Permission = "G.04. Manajemen User - Permission;/manajemen-user/permission";
        public string Pengaturan_Delegasi = "H.01. Pengaturan - Delegasi;/pengaturan/delegasi";
        public string Pengaturan_Sekretaris = "H.02 Pengaturan - Sekretaris;/pengaturan/sekretaris";
        public string Pengaturan_Grup_Unit = "H.03 Pengaturan - Grup Unit;/pengaturan/grup-unit";
        public string Pengaturan_Trigger = "H.04. Pengaturan - Trigger;/pengaturan/trigger";
    }
}
