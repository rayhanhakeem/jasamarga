﻿using AutoMapper;
using Jasamarga.DA.UserManagement.Databases.Identity;
using Jasamarga.DA.UserManagement.Databases.Models;
using Jasamarga.DA.UserManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Utilities
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            //CreateMap<ActionAuthorization, ActionAuthorizationViewModels>().ReverseMap();
            CreateMap<ApplicationRoleActionAuthorizations, RoleViewModel>().ReverseMap();
            CreateMap<ApplicationUser, UserViewModel>().ReverseMap();
            CreateMap<ApplicationUser, UsersViewModel>().ReverseMap();
            CreateMap<ApplicationUser, AdminViewModel>().ReverseMap();
            CreateMap<Permission, PermissionViewModels>().ReverseMap();
            CreateMap<ApplicationRole, RoleViewModel>().ReverseMap();
            CreateMap<UserDevice, UserDevicesViewModels>().ReverseMap();
            CreateMap<UserRolesAdditional, UserRolesAdditionalViewModels>().ReverseMap();
            CreateMap<ApplicationUser, UserAdditionalViewModels>().ReverseMap();
            CreateMap<ApplicationUser, UserJMViewModel>().ReverseMap();
            CreateMap<ApplicationUser, UsersMemberViewModel>().ReverseMap();
        }
    }
}
