﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Utilities
{
    public static class CommonHelper
    {
        public static DateTime StartOfDay(this DateTime date)
        {
            var year = date.Year;
            var month = date.Month;
            var day = date.Day;

            return new DateTime(year, month, day, 0, 0, 0);
        }

        public static DateTime EndOfDay(this DateTime date)
        {
            var year = date.Year;
            var month = date.Month;
            var day = date.Day;

            return new DateTime(year, month, day, 23, 59, 59);
        }
    }
}
