﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Jasamarga.DA.UserManagement.Databases;
using Jasamarga.DA.UserManagement.Databases.Identity;
using Jasamarga.DA.UserManagement.Services.Contracts;
using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace Jasamarga.DA.UserManagement.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : BaseController
    {
        protected IAdminServices svc { get; set; }

        public AdminController(IAdminServices svc, ITokenServices tokenSvc) : base(tokenSvc)
        {
            this.svc = svc;
        }
        #region Admin
        [HttpGet]
        public ResponseViewModels<PaginatedList<AdminViewModel>> Index(int? page = 1, int? per_page = 10, string order_by = null, string order = null, string searchField = null, string query = null)
        {
            return svc.GetAllActiveAdmin(new AdminViewModel(), page, per_page, order_by, order, searchField, query);
        }

        [HttpPost]
        [Route("update")]
        public ResponseViewModels<AdminViewModel> Edit([FromBody] AdminViewModel vm)
        {

            return svc.EditAdmin(vm);
        }

        [HttpPost]
        [Route("delete/{id}")]
        public ResponseViewModels<AdminViewModel> Delete(string id)
        {
            return svc.DeleteAdmin(id);
        }

        [HttpGet]
        [Route("{id}")]
        public ResponseViewModels<AdminViewModel> DetailUser(string id)
        {
            return svc.GetAdmin(id);
        }
        #endregion
    }
}
