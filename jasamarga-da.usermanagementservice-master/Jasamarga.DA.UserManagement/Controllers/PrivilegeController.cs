﻿using Jasamarga.DA.UserManagement.Services.Contracts;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PrivilegeController : BaseController
    {
        protected IPrivelegedService svc { set; get; }
        public PrivilegeController(IPrivelegedService svc, ITokenServices tokenSvc) : base(tokenSvc)
        {
            this.svc = svc;
        }

        [HttpGet]
        public PrivilegedViewModels ListPriviliged()
        {
            return svc.ListPrivileged();
        }

        [HttpGet]
        [Route("ListPriviligedByRole")]
        public PrivilegedViewModels ListPriviligedByRole(string id)
        {
            return svc.ListPrivilegedByRoles(id);
        }

        [HttpPost]
        [Route("CheckPrivileged")]
        public ResponsePrivileged CheckPriviliged(string url)
        {
           return svc.Validation(url);
        }
    }
}
