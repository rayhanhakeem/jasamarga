﻿using Jasamarga.DA.UserManagement.Services.Contracts;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class UserDeviceController : BaseController
    {
        protected IUserDeviceServices svc { get; set; }

        public UserDeviceController(IUserDeviceServices svc, ITokenServices tokenSvc) : base(tokenSvc)
        {
            this.svc = svc;
        }

        [HttpGet]
        [Route("{deviceId}")]
        public ResponseViewModels<UserDevicesViewModels> Detail(string deviceId)
        {
            return svc.GetUserDevices(deviceId);
        }

        [HttpGet]
        public ResponseViewModels<List<UserDevicesViewModels>> List()
        {
            return svc.GetListUserDevices();
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("GetUserDeviceByUserId")]
        public ResponseViewModels<List<UserDevicesViewModels>> ListUserDeviceById(string userId)
        {
            return svc.GetListUserDevicesByUserId(userId);
        }
    }
}
