﻿using Jasamarga.DA.UserManagement.Databases;
using Jasamarga.DA.UserManagement.Services.Contracts;
using Jasamarga.DA.UserManagement.Services.Repositories;
using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Controllers
{
    public class DataController : Controller
    {
        private readonly AppSettings _appSettings;
        private ApplicationDbContext dbContext;
        protected IDummyServices svc { get; set; }
        public DataController(IOptions<AppSettings> appSettings, ApplicationDbContext dbContext, IDummyServices svc)
        {
            this._appSettings = appSettings.Value;
            this.dbContext = dbContext;
            this.svc = svc;
        }
        [HttpGet]
        [Route("seed/{seed_key}")]
        public SeedMessage Seed(string seed_key)
        {
            return svc.Seed(seed_key, _appSettings.SeedKey);
        }

        [HttpGet]
        [Route("updatealluser")]
        public ResponseViewModels<bool> UpdateAllUser()
        {
            return svc.UpdateUser();
        }

        [HttpGet]
        [Route("processing-unit")]
        public ResponseViewModels<List<ProcessingUnitViewModel>> ProcessingUnit(
           string OrderBy = null,
           string Order = null,
           bool? IncludeDeleted = null,
           string firstChar = null)
        {
            var filter = new ProcessingUnitFilter
            {
                OrderBy = OrderBy,
                Order = Order,
                IncludeDeleted = IncludeDeleted,
                FirstChar = firstChar
            };

            return svc.GetAll(new ProcessingUnitViewModel(), filter);
        }
    }
}
