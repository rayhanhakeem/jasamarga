﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jasamarga.DA.UserManagement.Services.Contracts;
using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Jasamarga.DA.UserManagement.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : BaseController
    {
        protected IRoleServices _svc { get; set; }
        protected IHelperServices _hlpSvc { get; set; }

        public RoleController(IRoleServices svc, ITokenServices tokenSvc, IHelperServices hlpSvc) : base(tokenSvc)
        {
            this._svc = svc;
            this._hlpSvc = hlpSvc;
        }

        #region Role

        [HttpGet]
        public ResponseViewModels<PaginatedList<RoleViewModel>> Index(int? page = 1, int? per_page = 10, string order_by = null, string order = null, string searchField = null, string query = null, bool? IncludeDeleted = null)
        {
            return _svc.GetAllRole(new RoleViewModel(), page, per_page, order_by, order, searchField, query, IncludeDeleted);
        }

        [HttpPost]
        public async Task<ActionResult<ResponseViewModels<RoleViewModel>>> Create([FromBody] RoleViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return await _svc.InsertRole(vm);
        }

        [HttpGet]
        [Route("{id}")]
        public ResponseViewModels<RoleViewModel> Detail(string id)
        {
            return _svc.GetRoleViewModel(id);
        }

        [HttpPost]
        [Route("update")]
        public ActionResult<ResponseViewModels<RoleViewModel>> Edit([FromBody] RoleViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return _svc.EditRole(vm);
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public ResponseViewModels<RoleViewModel> Delete(string id)
        {
            return _svc.DeleteRole(id);
        }

        [HttpPost]
        [Route("restore/{id}")]
        public ResponseViewModels<RoleViewModel> Restore(string id)
        {
            return _svc.RestoreRole(id);
        }

        [HttpGet("ChangeRoles/{Id}")]
        public Task<ResponseViewModels<UserViewModel>> ChangeRoles(string Id)
        {
            return _svc.SwitchRoles(Id);
        }

        [HttpPost("ChangeRolesV2")]
        public TokenResponse ChangeRolesV2([FromBody] SwitchRoles id)
        {
            return _svc.SwitchRolesV2(id.Id);
        }

        [HttpPost]
        [Route("AddRolesFromDelegation")]
        public Task<ResponseViewModels<UserRolesAdditionalViewModels>> AddRolesFromDelegation(string Id)
        {
          return _svc.RolesFromDelegation(Id);
        }

        [HttpPost]
        [Route("AddRolesFromSecretary")]
        public ResponseViewModels<UserRolesAdditionalViewModels> AddRolesFromSecretary(string Id, string userPejabatId, string secretaryId, bool isActive)
        {
          return  _svc.RolesFromSecretary(Id, userPejabatId, secretaryId, isActive);
        }
        [HttpDelete]
        [Route("DeleteUserRolesAdditional")]
        public ResponseViewModels<UserRolesAdditionalViewModels> DeleteUserRolesAdditional(string Id)
        {
           return _svc.DeleteUserRolesAdditional(Id);
        }

        [HttpPost]
        [Route("RestoreUserRolesAdditional")]
        public ResponseViewModels<UserRolesAdditionalViewModels> RestoreUserRolesAdditional(string Id)
        {
            return _svc.RestoreUserRolesAdditional(Id);
        }
        
        //[HttpPost]
        //[Route("InputJabatanBaru")]
        //public void jabatanbaru(double? orgId, string orgName, double? parentOrgId, string positionName)
        //{
        //   _hlpSvc.InsertPosition(orgId, orgName, parentOrgId, positionName);
            
        //}
        //[HttpGet]
        //[Route("GetJabatanBaru")]
        //public void Getjabatanbaru(string positionName)
        //{
        //    _hlpSvc.GetPosition(positionName);

        //}
        #endregion

        public class SwitchRoles
        {
            public string Id { get; set; }
        }
    }
}
