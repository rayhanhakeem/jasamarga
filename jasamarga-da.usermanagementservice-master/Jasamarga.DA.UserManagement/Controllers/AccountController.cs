﻿using Jasamarga.DA.UserManagement.Databases;
using Jasamarga.DA.UserManagement.Databases.Identity;
using Jasamarga.DA.UserManagement.Services.Contracts;
using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Wangkanai.Detection.Services;

namespace Jasamarga.DA.UserManagement.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : BaseController
    {
        protected IAccountServices svc { get; set; }
        protected IAuthenticateServices authSvc { get; set; }
        protected IHelperServices helperSvc { get; set; }
        protected ITokenServices tokenSvc { get; set; }
        private readonly IDetectionService _detectionService;
        protected ICaptchaVerificationServices captchaSvc { get; set; }
        private UserManager<ApplicationUser> userManager;
        private RoleManager<ApplicationRole> roleManager;
        private ApplicationDbContext context;
        private SignInManager<ApplicationUser> signInManager;
        private readonly AppSettings _appSettings;
        private const string DisplayNameAttribute = "DisplayName";
        private const string SAMAccountNameAttribute = "SAMAccountName";
        private static HttpClient client = new HttpClient();
        public AccountController(
            IAccountServices svc,
            IAuthenticateServices authSvc,
            ICaptchaVerificationServices captchaSvc,
            UserManager<ApplicationUser> userManager,
            ApplicationDbContext context,
            RoleManager<ApplicationRole> roleManager,
            SignInManager<ApplicationUser> signInManager,
            IOptions<AppSettings> appSettings,
            ITokenServices tokenSvc,
            IHelperServices helperSvc,
            IDetectionService detectionService) : base(tokenSvc)
        {
            this.svc = svc;
            this.captchaSvc = captchaSvc;
            this.userManager = userManager;
            this.context = context;
            this.roleManager = roleManager;
            this.signInManager = signInManager;
            this._appSettings = appSettings.Value;
            this._detectionService = detectionService;
            this.tokenSvc = tokenSvc;
            this.authSvc = authSvc;
            this.helperSvc = helperSvc;
        }
        #region User
        [HttpGet]
        [Route("user")]
        public Task<ResponseViewModels<PaginatedUserList<UsersViewModel>>> Index(FilterParameterViewModels filter, bool? isActive = null, bool? includeParent = null, bool? includeDelegation = null, bool? includeGroup = null, string level = null, string unitKerjaId = null)
        {
            return svc.GetAllActiveUser(new UsersViewModel(), filter, isActive, includeParent, includeDelegation, includeGroup, level, unitKerjaId);
        }

        [HttpPost]
        [Route("update")]
        public ResponseViewModels<UserViewModel> EditUser([FromForm] UpdateUserViewModel vm)
        {
            return svc.EditUser(vm);
        }
        [HttpPost]
        [Route("updateV2")]
        public Task<ResponseViewModels<UserViewModel>> EditUserV2([FromForm] UpdateUserViewModelV2 vm)
        {
            return svc.EditUserV2(vm);
        }
        [HttpPost]
        [Route("register")]
        public Task<ResponseViewModels<RegisterViewModel>> RegisterUser([FromForm] RegisterViewModel vm)
        {
            return svc.RegisterUser(vm);
        }
        [HttpPost]
        [Route("registerV2")]
        public Task<ResponseViewModels<RegisterViewModelV2>> RegisterUserV2([FromForm] RegisterViewModelV2 vm)
        {
            return svc.RegisterUserV2(vm);
        }
        [HttpPost]
        [Route("CreateUserGroup")]
        public ResponseViewModels<UserViewModel> CreateUserGroup(string id, string userName, string personName)
        {
            return svc.CreateUserGroup(id, userName, personName);
        }
        [HttpDelete]
        [Route("DeleteUserGroup")]
        public ResponseViewModels<UserViewModel> DeleteUserGroup(string id)
        {
            return svc.DeleteUserGroup(id);
        }
        [HttpPost]
        [Route("RestoreUserGroup")]
        public ResponseViewModels<UserViewModel> RestoreUserGroup(string id)
        {
            return svc.RestoreUserGroup(id);
        }
        [HttpPost]
        [Route("UploadImageUser")]
        public ActionResult<ResponseViewModels<UserViewModel>> UploadImageUser([FromForm] UploadImageViewModels vm)
        {
            return svc.UploadImageUser(vm.ImageAttachment);
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("user/{id}")]
        public Task<ResponseViewModels<UserViewModel>> DetailUser(string id)
        {
            return svc.GetUser(id);
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("GetPersonName/{id}")]
        public UserViewModel GetPersonName(string id)
        {
            return helperSvc.GetPersonName(id);
        }

        [HttpGet("GetUserByToken")]
        public Task<ResponseViewModels<UserViewModel>> DetailUserByToken()
        {
            return svc.GetUserByToken();
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("user/GetListUserByADAccount")]
        public ResponseViewModels<List<UserJMViewModel>> ListUserByADAccount([FromBody] UserList vm)
        {
            return svc.GetListUserByADAccount(vm.Ids);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("user/GetListUserByADAccountV2")]
        public ResponseViewModels<List<UserJMViewModel>> ListUserByADAccountV2()
        {
            return svc.GetListUserByADAccountV2();
        }
        [AllowAnonymous]
        [HttpGet("GetUserByEmail/{email}")]
        public ResponseViewModels<UserViewModel> DetailUserByEmail(string email)
        {
            return helperSvc.GetUserByEmail(email);
        }

        [AllowAnonymous]
        [HttpDelete("DeleteNonLdap")]
        public ResponseViewModels<UserViewModel> DeleteUserNonLdap()
        {
            return svc.DeleteNonLdap();
        }

        [HttpDelete]
        [Route("{id}")]
        public ResponseViewModels<UserViewModel> DeleteUser(string id)
        {
            return svc.DeleteUser(id);
        }

        [HttpGet]
        [Route("PejabatForSecretary")]
        public Task<ResponseViewModels<PaginatedList<UsersViewModel>>> PejabatForSecretary(FilterParameterViewModels filter, string userId = null, string secretaryId = null)
        {
            return svc.PejabatSekretaris(new UsersViewModel(), filter, userId, secretaryId);
        }

        [HttpGet]
        [Route("GetListUserByLevel")]
        public List<UsersViewModel> GetListUserByLevel(FilterParameterViewModels filter)
        {
            return svc.GetListUserByLevelV2(filter);
        }

        [HttpGet]
        [Route("GetListUserByLevelV2")]
        public List<UsersViewModel> GetListUserByLevelV2(FilterParameterViewModels filter)
        {
            return svc.GetListUserByLevelV2(filter);
        }

        [HttpGet]
        [Route("GetListUserByLevelV3")]
        public List<UsersViewModel> GetListUserByLevelV3(FilterParameterViewModels filter)
        {
            return svc.GetListUserByLevelV2(filter);
        }

        [HttpGet]
        [Route("GetPengganti1")]
        public List<UsersViewModel> GetPengganti1(int level1, FilterParameterViewModels filter)
        {
            return svc.GetUserDelegate(level1, filter);
        }

        [HttpGet]
        [Route("GetPengganti2")]
        public List<UsersViewModel> GetPengganti2(int level1, int level2, FilterParameterViewModels filter)
        {
            return svc.GetUserDelegate2(level1, level2, filter);
        }
        //[AllowAnonymous]
        [HttpGet]
        [Route("GetUserByEmployeeNumber")]
        public UserViewModel GetUserByEmployeeNumber(string number)
        {
            return svc.GetEmployeeByEmployeeNumber(number);
        }

        [HttpGet]
        [Route("GetListGolongan")]
        public ResponseViewModels<List<GolonganViewModel>> Getgolongan()
        {
            return helperSvc.GetListGolongan();
        }

        [HttpPost]
        [Route("user/GetListUserByIds")]
        public Task<ResponseViewModels<List<UsersViewModel>>> ListUserById([FromBody] UserList vm)
        {
            return svc.GetListUserById(vm.Ids);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("user/GetListUserByPositionIds")]
        public Task<ResponseViewModels<List<UsersViewModel>>> ListUserByPositionId([FromBody] UserList vm)
        {
            return svc.GetListUserByPositionId(vm.Ids);
        }

        [HttpPost]
        [Route("user/GetListMemberGroupUserByIds")]
        public Task<ResponseViewModels<List<UsersMemberViewModel>>> GetListUserMemberById([FromBody] UserList vm)
        {
            return svc.GetListUserMemberById(vm.Ids);
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("user/GetListUserByUnitKerja")]
        public ResponseViewModels<List<UsersViewModel>> ListUserByUnitKerjaId(string unitKerja)
        {
            return svc.GetListUserByUnitKerja(unitKerja);
        }
        #endregion
        #region Login
        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            string deviceType = DeviceType();
            var userLogin = context.Users.FirstOrDefault(a => a.UserName == model.UserName && a.IsDeleted == false);
            //await userManager.FindByNameAsync(model.UserName);
            
            if (userLogin != null)
            {
                if (userLogin.RecentPositionId == 99999999)
                {
                    var nonActiveUser = new TokenResponse()
                    {
                        StatusCode = 400,
                        Message = "User sudah tidak aktif",
                        Token = null,
                        Type = null,
                        RefreshToken = null,
                        UserId = null
                    };
                    return BadRequest(nonActiveUser);
                }

                if (userLogin.IsDelegation == true || userLogin.ParentUserId != null)
                {
                    var notmainRole = new TokenResponse()
                    {
                        StatusCode = 400,
                        Message = "Silakan login menggunakan akun Induk",
                        Token = null,
                        Type = null,
                        RefreshToken = null,
                        UserId = null
                    };
                    return BadRequest(notmainRole);
                }

                if (await userManager.CheckPasswordAsync(userLogin, model.Password))
                {
                    if (userLogin.IsActive == false)
                    {
                        var userInactive = new TokenResponse()
                        {
                            StatusCode = 400,
                            Message = "Akun sudah tidak aktif. Silahkan hubungi admin teknis aplikasi",
                            Token = null,
                            Type = null,
                            RefreshToken = null,
                            UserId = null
                        };
                        return BadRequest(userInactive);
                    }
                    svc.UpdateUser(model.UserName);
                    var response = authSvc.Authenticate(model, ipAddress());
                    setTokenCookie(response.RefreshToken);
                    helperSvc.DefaultRolesV2(response.UserId);
                    if (model.DeviceId != null)
                    {
                        authSvc.SaveUserDevice(response.UserId, model.DeviceId, deviceType);
                    }
                    return Ok(response);
                }
            }
            //else
            //{
            var userInfo = ValidateUserCredentialsViaActiveDirectory(model.UserName, model.Password);
            if (userInfo != null)
            {
                string userName = model.UserName?.ToString();
                ApplicationUser user = context.Users.Where(t => t.UserName.ToLower().Equals(model.UserName.ToLower())).FirstOrDefault();
                if (user == null) // If the User DOES NOT exists in the ASP.NET Identity table (AspNetUsers)
                {
                    try
                    {
                        //create user data from jmcore
                        user = new ApplicationUser();
                        user.UserName = userInfo.GetDirectoryEntry().Properties["samaccountname"]?.Value?.ToString();
                        user.PersonName = userInfo.GetDirectoryEntry().Properties["displayname"]?.Value?.ToString();
                        user.Email = userInfo.GetDirectoryEntry().Properties["mail"]?.Value?.ToString();
                        svc.GetEmployee(user.PersonName, user.UserName, user.Email, model.Password);
                    }
                    catch (Exception e)
                    {

                    }
                }
                else
                {
                    try
                    {
                        //update password
                        await UpdatePassword(user.Id, model.Password);
                        svc.UpdateUser(model.UserName);
                    }
                    catch (Exception e)
                    {

                    }
                }
                //Sign in user ldap
                var userLoginLdap = await userManager.FindByNameAsync(user.UserName);
                if (userLoginLdap != null && await userManager.CheckPasswordAsync(userLoginLdap, model.Password))
                {
                    var response = authSvc.Authenticate(model, ipAddress());
                    setTokenCookie(response.RefreshToken);
                    helperSvc.DefaultRoles(response.UserId);
                    if (model.DeviceId != null)
                    {
                        authSvc.SaveUserDevice(response.UserId, model.DeviceId, deviceType);
                    }
                    return Ok(response);
                }
            }
            else
            {
                var response = new TokenResponse()
                {
                    StatusCode = 400,
                    Message = "Username dan password yang dimasukkan tidak cocok. Silakan coba lagi.",
                    Token = null,
                    Type = null,
                    RefreshToken = null,
                    UserId = null
                };
                return BadRequest(response);
            }
            //}
            var responseFail = new TokenResponse()
            {
                StatusCode = 400,
                Message = "Username dan password yang dimasukkan tidak cocok. Silakan coba lagi.",
                Token = null,
                Type = null,
                RefreshToken = null,
                UserId = null
            };
            return BadRequest(responseFail);
        }

        public SearchResult ValidateUserCredentialsViaActiveDirectory(string username, string password)
        {
            var directoryEntry = new DirectoryEntry(
                _appSettings.LDAPConnectionString,
                _appSettings.UserDomainName + "\\" +
                username,
                password,
                AuthenticationTypes.Secure
                );

            try
            {
                var directorySearcher = new DirectorySearcher(directoryEntry)
                {
                    Filter = string.Format("({0}={1})", SAMAccountNameAttribute, username)
                };
                try
                {
                    return directorySearcher.FindOne();
                }

                catch (DirectoryServicesCOMException dsce)
                {
                    if (dsce.Message.Contains("user name or password") || dsce.Message.Contains("user name or bad password"))
                    {
                        return null;
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        [AllowAnonymous]
        [HttpPost("UpdatePassword")]
        //[ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> UpdatePassword(string id, string password)
        {
            var user = await userManager.FindByIdAsync(id);
            var token = await userManager.GeneratePasswordResetTokenAsync(user);
            var result = await userManager.ResetPasswordAsync(user, token, password);
            return Ok(result);
        }
        [HttpPost("ResetPassword")]
        public async Task<ActionResult<ResponseViewModels<ResetPasswordViewModel>>> ResetPassword([FromBody] ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var reset = await svc.ResetPassword(model);
            return Ok(reset);
        }
        [HttpPost("ForgetPassword")]
        public ResponseViewModels<string> ForgetPassword(string email)
        {
            return svc.ForgetPassword(email);
        }

        [AllowAnonymous]
        [HttpPost("RefreshToken")]
        public IActionResult RefreshToken([FromBody] RefreshTokenViewModel vm)
        {
            //var refreshToken = Request.Cookies["refreshToken"];
            var response = authSvc.RefreshToken(vm.RefreshToken, ipAddress());
            if (response == null)
                return Unauthorized(new { message = "Invalid token" });
            setTokenCookie(response.RefreshToken);
            return Ok(response);
        }

        [HttpPost("RevokeToken")]
        public IActionResult RevokeToken()
        {
            var response = authSvc.Revoke();
            return Ok(response);
        }


        private void setTokenCookie(string token)
        {
            var cookieOptions = new CookieOptions
            {
                HttpOnly = true,
                Expires = DateTime.UtcNow.AddHours(_appSettings.ExpiresRefreshToken)
            };
            Response.Cookies.Append("refreshToken", token, cookieOptions);
        }

        private string ipAddress()
        {
            if (Request.Headers.ContainsKey("X-Forwarded-For"))
                return Request.Headers["X-Forwarded-For"];
            else
                return HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
        }

        private string DeviceType()
        {
            string device = _detectionService.Device.Type.ToString();
            return device;
        }

        #endregion

        #region captcha
        [AllowAnonymous]
        [HttpPost]
        [Route("VerificationCaptcha")]
        public dynamic verificationResponseAsync(string CaptchaResponse)
        {
            var requestIsValid = captchaSvc.IsCaptchaValid(CaptchaResponse);
            return requestIsValid;
        }
        #endregion

        #region unit
        [HttpGet]
        [Route("GetDataUnit")]
        public List<UnitViewModels> ListUnit()
        {
            return helperSvc.GetAllUnit();
        }
        #endregion

        [AllowAnonymous]
        [HttpPost]
        [Route("UpdateUser")]
        public ActionResult<bool> UpdateUser()
        {
            try
            {
                var model = context.Users.ToList();
                foreach (var item in model)
                {
                    item.CreatedDate = DateTime.Now;
                    item.UpdatedDate = DateTime.Now;
                    context.Users.Update(item);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                return BadRequest();
            }

            return Ok();
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("UpdateUserRolesAdditional")]
        public ResponseViewModels<bool> UpdateUserRolesAdditional()
        {
            return helperSvc.UpdateUserAdditionaMainRole();
        }
        public class UserList
        {
            public List<string> Ids { set; get; }
        }

        public class RefreshTokenViewModel
        {
            public string RefreshToken { set; get; }
        }
    }
}
