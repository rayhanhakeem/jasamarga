﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jasamarga.DA.UserManagement.Services.Contracts;
using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Jasamarga.DA.UserManagement.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class PermissionController : BaseController
    {
        protected IPermissionServices svc { set; get; }
        public PermissionController(IPermissionServices svc, ITokenServices tokenSvc) : base(tokenSvc)
        {
            this.svc = svc;
        }

        [HttpGet]
        public ResponseViewModels<PaginatedList<PermissionViewModels>> Index(int? page = 1, int? per_page = 10, string order_by = null, string order = null, string searchField = null, string query = null, bool? IncludeDeleted = null)
        {
            return svc.GetList(new PermissionViewModels(), page, per_page, order_by, order, searchField, query, IncludeDeleted);
        }

        [HttpPost]
        public ActionResult<ResponseViewModels<PermissionViewModels>> Create([FromBody] PermissionViewModels vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else
            {
                return svc.Store(vm);
            }
            
        }


        [HttpPost]
        [Route("update")]
        public ActionResult<ResponseViewModels<PermissionViewModels>> Update([FromBody] PermissionViewModels vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else
            {
                return svc.Update(vm);
            }
            
        }

        [HttpGet]
        [Route("{id}")]
        public ResponseViewModels<PermissionViewModels> Detail(string id)
        {
            return svc.Detail(id);
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public ResponseViewModels<PermissionViewModels> Delete(string id)
        {
            return svc.Delete(id);
        }
        [HttpPost]
        [Route("restore/{id}")]
        public ResponseViewModels<PermissionViewModels> Restore(string id)
        {
            return svc.Restore(id);
        }
    }
}
