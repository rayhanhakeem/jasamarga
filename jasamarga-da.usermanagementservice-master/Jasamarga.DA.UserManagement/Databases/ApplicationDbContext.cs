﻿using Jasamarga.DA.UserManagement.Databases.Identity;
using Jasamarga.DA.UserManagement.Databases.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Databases
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string,
       ApplicationUserClaim, ApplicationUserRole, ApplicationUserLogin,
        ApplicationRoleClaim, ApplicationUserToken>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<ApplicationUser>(b =>
            {
                // Each User can have many UserClaims
                b.HasMany(e => e.Claims)
                    .WithOne(e => e.User)
                    .HasForeignKey(uc => uc.UserId)
                    .IsRequired();

                // Each User can have many UserLogins
                b.HasMany(e => e.Logins)
                    .WithOne(e => e.User)
                    .HasForeignKey(ul => ul.UserId)
                    .IsRequired();

                // Each User can have many UserTokens
                b.HasMany(e => e.Tokens)
                    .WithOne(e => e.User)
                    .HasForeignKey(ut => ut.UserId)
                    .IsRequired();

                // Each User can have many entries in the UserRole join table
                b.HasMany(e => e.UserRoles)
                    .WithOne(e => e.User)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();

                b.HasMany(e => e.UserDevices)
                    .WithOne(e => e.User)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();

                //b.HasMany(e => e.UserRolesAdditionals)
                //    .WithOne(e => e.User)
                //    .HasForeignKey(ur => ur.LoginUserPersonId)
                //    .HasForeignKey(ur => ur.PejabatId )
                //    .IsRequired();

                b.ToTable("AspNetUsers");
            });

            builder.Entity<ApplicationRole>(b =>
            {
                // Each Role can have many entries in the UserRole join table
                b.HasMany(e => e.UserRoles)
                    .WithOne(e => e.Role)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();

                // Each Role can have many associated RoleClaims
                b.HasMany(e => e.RoleClaims)
                    .WithOne(e => e.Role)
                    .HasForeignKey(rc => rc.RoleId)
                    .IsRequired();


                b.ToTable("AspNetRoles");
            });


            builder.Entity<ApplicationUserRole>(b =>
            {
                // Primary key
                b.HasKey(r => new { r.UserId, r.RoleId });
                //b.HasMany(e => e.UserRolesAdditionals)
                //    .WithOne(e => e.UserRoles)
                //    .HasForeignKey(ur => ur.PejabatRoleId )
                //    .IsRequired();
                // Maps to the AspNetUserRoles table
                b.ToTable("AspNetUserRoles");
            });

            builder.Entity<ApplicationUserLogin>(b =>
            {
                // Composite primary key consisting of the LoginProvider and the key to use
                // with that provider
                b.HasKey(l => new { l.LoginProvider, l.ProviderKey, l.UserId });

                // Limit the size of the composite key columns due to common DB restrictions
                b.Property(l => l.LoginProvider).HasMaxLength(128);
                b.Property(l => l.ProviderKey).HasMaxLength(128);
                b.Property(l => l.UserId).HasMaxLength(128);

                // Maps to the AspNetUserLogins table
                b.ToTable("AspNetUserLogins");
            });

            builder.Entity<ApplicationUserToken>(b =>
            {
                // Composite primary key consisting of the UserId, LoginProvider and Name
                b.HasKey(t => new { t.UserId, t.LoginProvider, t.Name });

                // Limit the size of the composite key columns due to common DB restrictions
                b.Property(t => t.LoginProvider).HasMaxLength(128);
                b.Property(t => t.Name).HasMaxLength(128);

                // Maps to the AspNetUserTokens table
                b.ToTable("AspNetUserTokens");
            });

            builder.Entity<ApplicationRoleActionAuthorizations>(b =>
            {
                b.HasKey(t => new { t.ApplicationRoleId, t.PermissionId });
            });
        }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<ApplicationRoleActionAuthorizations> ApplicationRoleActionAuthorizations { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        public DbSet<UserDevice> UserDevices { get; set; }

        public DbSet<UserRolesAdditional> UserRolesAdditionals { get; set; }
    }
}
