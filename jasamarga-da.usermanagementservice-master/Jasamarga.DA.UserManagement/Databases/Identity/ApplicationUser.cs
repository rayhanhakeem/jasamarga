﻿using Jasamarga.DA.UserManagement.Databases.Models;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Databases.Identity
{
    public class ApplicationUser : IdentityUser, IBaseModel
    {
        public long? PersonNumber { get; set; }
        public string PersonName { get; set; }
        public string EmployeeNumber { get; set; }
        public DateTime? OriginalDateOfHire { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string TownOfBirth { get; set; }
        public string NationalIdentifier { get; set; }
        public string Sex { get; set; }
        public double? JobId { get; set; }
        public string JobName { get; set; }
        public string JobType { get; set; }
        public double? RecentPositionId { get; set; }
        public string PositionName { get; set; }
        public string PositionNameAlias { get; set; }
        public string PositionNameAtasan { get; set; }
        public string NomorSk { get; set; }
        public string TanggalSk { get; set; }
        public string TanggalEfektifSk { get; set; }
        public double? OrganizationId { get; set; }
        public string OrgName { get; set; }
        public double? OrgIdParent { get; set; }
        public string OrgParentName { get; set; }
        public double? OrgGrandParent { get; set; }
        public string OrgGrandParentName { get; set; }
        public string UnitKerja { get; set; }
        public double? LocationId { get; set; }
        public string LocationName { get; set; }
        public string Golongan { get; set; }
        public string Baris { get; set; }
        public string Ruang { get; set; }
        public string Npwp { get; set; }
        public string JenisNpwp { get; set; }
        public string StatusNpwp { get; set; }
        public string MaritalStatus { get; set; }
        public string Religion { get; set; }
        public string Password { get; set; }
        public double? Isldap { get; set; }
        public string StatusSat { get; set; }
        public string StatusSap { get; set; }
        public double? MainRole { get; set; }
        public long? UnitKerjaId { get; set; }
        public string PayScaleArea { get; set; }
        public string UrlImage { get; set; }
        public string UrlImageResize { get; set; }
        public string ImageFileName { get; set; }
        public string ImageFileNameResize { get; set; }
        public double? PersonNumberApprover { get; set; }
        public long? PersonnelNumberSap { get; set; }
        public short? EmployeeGroup { get; set; }
        public short? CntSubOrd { get; set; }
        public short? PersonalArea { get; set; }
        public int? PositionIdAtasan { get; set; }
        public int? PayrollGroupId { get; set; }
        public string EmployeeSubgroup { get; set; }
        public string BusinessArea { get; set; }
        public string PersonalSubArea { get; set; }
        public string CompanyCode { get; set; }
        public short? EmpType { get; set; }
        public string NppAbsen { get; set; }
        public string EmpStatus { get; set; }
        public string DataSource { get; set; }
        public int Level { get; set; }
        public string BODLevel { get; set; }
        public double? JmId { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        [DefaultValue(false)]
        public bool IsDeleted { get; set; }
        [DefaultValue(false)]
        public bool IsDraft { get; set; }
        [DefaultValue(true)]
        public bool IsActive { get; set; }
        [DefaultValue(false)]
        public bool IsGroup { get; set; }
        public string ParentUserId { get; set; }
        public bool? IsDelegation { get; set; }
        public DateTime? DelegationStart { get; set; }
        public DateTime? DelegationEnd { get; set; }
        public bool IsUnitKearsipan { get; set; }
        public void Init()
        {
            Id = Guid.NewGuid().ToString();
            CreatedDate = DateTime.Now;
        }

        public void Delete(string userName)
        {
            this.IsDeleted = true;
            this.UpdatedBy = userName;
            this.UpdatedDate = DateTime.Now;
        }

        public virtual ICollection<ApplicationUserClaim> Claims { get; set; }
        public virtual ICollection<ApplicationUserLogin> Logins { get; set; }
        public virtual ICollection<ApplicationUserToken> Tokens { get; set; }
        public virtual ICollection<ApplicationUserRole> UserRoles { get; set; }
        public virtual ICollection<UserDevice> UserDevices { get; set; }
        //public virtual ICollection<UserRolesAdditional> UserRolesAdditionals { get; set; }

        [JsonIgnore]
        public List<RefreshToken> RefreshTokens { get; set; }
    }

    public class ApplicationUserRole : IdentityUserRole<string>
    {
        public virtual ApplicationUser User { get; set; }
        public virtual ApplicationRole Role { get; set; }

        //public virtual ICollection<UserRolesAdditional> UserRolesAdditionals { get; set; }
    }

    public class ApplicationUserClaim : IdentityUserClaim<string>
    {
        public virtual ApplicationUser User { get; set; }
    }

    public class ApplicationUserLogin : IdentityUserLogin<string>
    {
        public virtual ApplicationUser User { get; set; }
    }

    public class ApplicationRoleClaim : IdentityRoleClaim<string>
    {
        public virtual ApplicationRole Role { get; set; }
    }

    public class ApplicationUserToken : IdentityUserToken<string>
    {
        public virtual ApplicationUser User { get; set; }
    }
}
