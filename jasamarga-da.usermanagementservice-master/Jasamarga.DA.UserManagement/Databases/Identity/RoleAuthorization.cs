﻿using Jasamarga.DA.UserManagement.Databases.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Databases.Identity
{
    public class ApplicationRole : IdentityRole, IBaseModel
    {
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        [DefaultValue(false)]
        public bool IsDeleted { get; set; }
        [DefaultValue(false)]
        public bool IsDraft { get; set; }
        [DefaultValue(true)]
        public bool IsActive { get; set; }


        public void Init()
        {
            Id = Guid.NewGuid().ToString();
            CreatedDate = DateTime.Now;
        }

        public void Delete(string userName)
        {
            this.IsDeleted = true;
            this.UpdatedBy = userName;
            this.UpdatedDate = DateTime.Now;
        }
        public void SetCreated()
        {
            Id = Guid.NewGuid().ToString();
            CreatedDate = DateTime.Now;
            this.CreatedDate = DateTime.Now;
        }

        public void SetUpdated()
        {
            UpdatedDate = DateTime.Now;
            this.UpdatedDate = DateTime.Now;
        }
        public ApplicationRole() : base() { }
        public ApplicationRole(string name) : base(name) { }

        public virtual ICollection<ApplicationUserRole> UserRoles { get; set; }
        public virtual ICollection<ApplicationRoleClaim> RoleClaims { get; set; }
        public virtual ICollection<ApplicationRoleActionAuthorizations> ApplicationRoleActionAuthorizations { get; set; }
    }

    public class ApplicationRoleActionAuthorizations
    {
        [Key]
        [Required]
        [MaxLength(128)]
        public string ApplicationRoleId { get; set; }
        [Key]
        [Required]
        [MaxLength(128)]
        public string PermissionId { get; set; }
        [DefaultValue(false)]
        public bool forView { get; set; }
        [DefaultValue(false)]
        public bool forCreate { get; set; }
        [DefaultValue(false)]
        public bool forEdit { get; set; }
        [DefaultValue(false)]
        public bool forDelete { get; set; }
        [DefaultValue(false)]
        public bool forRestore { get; set; }
        public ApplicationRole ApplicationRole { get; set; }
        public Permission Permission { get; set; }
    }
}
