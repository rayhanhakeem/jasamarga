﻿using Jasamarga.DA.UserManagement.Databases.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Databases.Models
{
    public class UserDevice : BaseModel
    {
        public string UserId { get; set; }
        public string DeviceId { get; set; }
        public string DeviceType { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
