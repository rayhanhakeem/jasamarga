﻿using Jasamarga.DA.UserManagement.Databases.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Databases.Models
{
    public class UserRolesAdditional : BaseModel
    {
        public string TypeId { get; set; }
        public string Type { get; set; }
        public string LoginUserPersonId { get; set; }
        public string LoginUserPersonName { get; set; }
        public string LoginUserUnitKerja { get; set; }
        public double? LoginUserUnitKerjaId { get; set; }
        public string PejabatRoleId { get; set; }
        public string PejabatRoleName { get; set; }
        public string PejabatId { get; set; }
        public string PejabatName { get; set; }
        public double? PejabatJabatanId { get; set; }
        public string PejabatJabatanName { get; set; }
        public string PejabatBODLEvel { get; set; }
        public bool isActive { get; set; }
        public bool SecretaryActive { get; set; }
        public DateTime? DelegationStartDate { get; set; }
        public DateTime? DelegationEndDate { get; set; }
        public int? Order { get; set; }
        public string ParentUserId { get; set; }
        public string DelegationUserId { get; set; }
        //public virtual ApplicationUser User { get; set; }
        //public virtual ApplicationUserRole UserRoles { get; set; }
        //public string Privileged { get; set; }
        //public List<string> PrivilegedList { get; set; }
    }
}
