﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Databases.Models
{
    public class BaseModel : IBaseModel
    {
        protected BaseModel()
        {
            Id = Guid.NewGuid().ToString();
            CreatedDate = DateTime.Now;
        }

        public void Init()
        {
            Id = Guid.NewGuid().ToString();
            CreatedDate = DateTime.Now;
        }

        public void Delete(string userName)
        {
            this.IsDeleted = true;
            this.UpdatedBy = userName;
            this.UpdatedDate = DateTime.Now;
        }

        [Key]
        [Required]
        [MaxLength(128)]
        public string Id { get; set; }
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }


        [DefaultValue(false)]
        public bool IsDeleted { get; set; }


        [DefaultValue(false)]
        public bool IsDraft { get; set; }

        public void SetCreated()
        {
            Id = Guid.NewGuid().ToString();
            CreatedDate = DateTime.Now;
            this.CreatedDate = DateTime.Now;
        }

        public void SetUpdated()
        {
            UpdatedDate = DateTime.Now;
            this.UpdatedDate = DateTime.Now;
        }
    }
}
