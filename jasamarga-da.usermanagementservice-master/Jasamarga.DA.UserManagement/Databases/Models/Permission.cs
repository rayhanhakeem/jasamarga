﻿using Jasamarga.DA.UserManagement.Databases.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.Databases.Models
{
    public class Permission : BaseModel
    {
        public string PermissionName { get; set; }
        public string Url { get; set; }
        public virtual ICollection<ApplicationRoleActionAuthorizations> ApplicationRoleActionAuthorizations { get; set; }
    }
}
