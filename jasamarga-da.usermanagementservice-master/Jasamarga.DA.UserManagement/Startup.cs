using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jasamarga.DA.UserManagement.AppStart;
using Jasamarga.DA.UserManagement.Databases;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using AutoMapper;
using Jasamarga.DA.UserManagement.Services.Contracts;
using Jasamarga.DA.UserManagement.Services.Repositories;
using Jasamarga.DA.UserManagement.Databases.Identity;
using Microsoft.AspNetCore.Identity;
using Jasamarga.DA.UserManagement.Utilities;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using NSwag;
using NSwag.Generation.Processors.Security;
using Microsoft.AspNetCore.Authorization;
using Jasamarga.DA.UserManagement.Services;
using Wangkanai.Detection.Services;
using Jasamarga.DA.UserManagement.Models;
using ElmahCore.Mvc;
using ElmahCore;

namespace Jasamarga.DA.UserManagement
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        readonly string AllowSpecificOrigins = "_allowSpecificOrigins";
        private AppSettings mapper { set; get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddDistributedMemoryCache();
            mapper = Configuration.GetSection("AppSettings").Get<AppSettings>();
            //services.AddOptions<CaptchaSettings>().BindConfiguration("Captcha");
            services.AddDetection();
            //services.AddHttpClient<ICaptchaValidator, GoogleReCaptchaValidator>();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddControllers()
                .AddNewtonsoftJson(options => {
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                })
                .ConfigureApiBehaviorOptions(options => {
                    options.SuppressConsumesConstraintForFormFileParameters = true;
                    options.SuppressInferBindingSourcesForParameters = true;
                    options.SuppressModelStateInvalidFilter = true;
                    options.SuppressMapClientErrors = true;
                    options.ClientErrorMapping[StatusCodes.Status404NotFound].Link =
                        "https://httpstatuses.com/404";
                });



            services.AddScoped(typeof(IBaseServices<,>), typeof(BaseServices<,>));
            //services.AddScoped<IActionAuthorizationServices, ActionAuthorizationServices>();
            services.AddScoped<IAccountServices, AccountServices>();
            services.AddScoped<IAdminServices, AdminServices>();
            services.AddScoped<IPermissionServices, PermissionServices>();
            services.AddScoped<IDummyServices, DummyServices>();
            services.AddScoped<ITokenServices, TokenServices>();
            services.AddScoped<IPrivelegedService, PrivilegedServices>();
            services.AddScoped<ICaptchaVerificationServices, CaptchaVerificationServices>();
            services.AddScoped<IDetectionService, DetectionService>();
            services.AddScoped<IAuthenticateServices, AuthenticateServices>();
            services.AddScoped<IUserDeviceServices, UserDeviceServices>();
            services.AddScoped<IHelperServices, HelperServices>();
            services.AddScoped<IRoleServices, RoleServices>();

            // For Entity Framework  
            services.AddDbContext<ApplicationDbContext>(options => options.UseNpgsql(Configuration.GetConnectionString("ConnStr")));
            services.AddDbContext<JMCoreContext>(options => options.UseNpgsql(Configuration.GetConnectionString("JMCoreConnStr")));

            // For Identity  
            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddCors(options =>
            {
                options.AddPolicy(
                    AllowSpecificOrigins,
                    builder =>
                    {
                        //if (mapper.Environtment.ToLower().Equals("development"))
                        //{
                        //    builder.AllowAnyOrigin();
                        //}
                        //else if (mapper.Environtment.ToLower().Equals("production"))
                        //{
                        //    var origins = mapper.AllowOrigins.Split(',');
                        //    builder.WithOrigins(origins);
                        //}
                        builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
                        //builder.AllowAnyHeader();
                        //builder.AllowAnyMethod();
                    }
               );
            });

            services.AddOpenApiDocument(document =>
            {
                document.AddSecurity("JWT", Enumerable.Empty<string>(), new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.ApiKey,
                    Name = "Authorization",
                    In = OpenApiSecurityApiKeyLocation.Header,
                    Description = "Type into the textbox: Bearer {your JWT token}."
                });

                document.OperationProcessors.Add(
                    new AspNetCoreOperationSecurityScopeProcessor("JWT"));
            });

            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);
            var capthcaSettingsSection = Configuration.GetSection("Captcha");
            services.Configure<CaptchaSettings>(capthcaSettingsSection);
            var ApiEndpointSection = Configuration.GetSection("ApiEndpoint");
            services.Configure<ApiEndpoint>(ApiEndpointSection);
            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            //var key = Encoding.ASCII.GetBytes(appSettings.Secret);

            // Adding Authentication  
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })

            // Adding Jwt Bearer  
            .AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(appSettings.Secret)),
                    RequireExpirationTime = false,
                    ClockSkew = TimeSpan.Zero
                };
            });


            //Adding Authorization
            services.AddAuthorization(options =>
            {
                var defaultAuthorizationPolicyBuilder = new AuthorizationPolicyBuilder(
                    JwtBearerDefaults.AuthenticationScheme);

                defaultAuthorizationPolicyBuilder =
                    defaultAuthorizationPolicyBuilder.RequireAuthenticatedUser();

                options.DefaultPolicy = defaultAuthorizationPolicyBuilder.Build();
            });
            // Adding Swagger
            //services.AddSwaggerDocument();
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromSeconds(10);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 1;
                options.Password.RequireLowercase = false;
                //options.Password.RequireNonLetterOrDigit = true;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
            });

            //adding elmah
            services.AddElmah<XmlFileErrorLog>(options =>
            {
                options.LogPath = "~/logs";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(AllowSpecificOrigins);

            app.UseAuthorization();
            app.UseStaticFiles();
            app.UseStatusCodePages();
            app.UseSession();

            app.UseElmah();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            
            //seed admin user
            //Initialize.CreateRoleandUser(app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope().ServiceProvider);

            app.UseOpenApi();
            app.UseSwaggerUi3();
        }
    }
}
