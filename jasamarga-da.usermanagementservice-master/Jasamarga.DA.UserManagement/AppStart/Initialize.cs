﻿using Jasamarga.DA.UserManagement.Databases;
using Jasamarga.DA.UserManagement.Databases.Identity;
using Jasamarga.DA.UserManagement.Databases.Models;
using Jasamarga.DA.UserManagement.Services.Contracts;
using Jasamarga.DA.UserManagement.Utilities;
using Jasamarga.DA.UserManagement.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jasamarga.DA.UserManagement.AppStart
{
    public class Initialize
    {
        public static void CreateRoleandUser(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<ApplicationDbContext>();
            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            var roleManager = serviceProvider.GetRequiredService<RoleManager<ApplicationRole>>();


            var actions = new List<PermissionViewModels>();
            var appPermission = new AppPermissions();

            foreach (var prop in typeof(AppPermissions).GetFields())
            {
                if (prop.IsPublic)
                {
                    var item = prop.GetValue(appPermission) as string;
                    string[] value = item.Split(';');
                    var permission = new PermissionViewModels();
                    permission.PermissionName = value[0];
                    permission.Url = value[1];
                    actions.Add(permission);
                }
            }

            foreach (var item in actions)
            {
                var exist = context.Permissions.Where(a => a.PermissionName.ToLower() == item.PermissionName.ToLower()).FirstOrDefault();
                if (exist == null)
                {
                    var permission = new Permission();
                    permission.PermissionName = item.PermissionName;
                    permission.Url = item.Url;
                    permission.Init();
                    permission.UpdatedDate = DateTime.Now;
                    context.Permissions.Add(permission);
                }
            }
            context.SaveChanges();


            if (!roleManager.RoleExistsAsync("Public").Result)
            {
                var role = new ApplicationRole();
                role.Id = "f4a3bfb3-511c-40f3-8e26-b873561d4d81";
                role.CreatedDate = DateTime.Now;
                role.UpdatedDate = DateTime.Now;
                role.Name = "Public";
                IdentityResult roleResult = roleManager.CreateAsync(role).Result;
                string[] auth =
                {
                    "A. Pencarian Arsip",
                    "B. Dashboard",
                    "D.02. Dokumen - Arsip Inaktif",
                    "D.03. Dokumen - Permintaan Akses",
                    "C.01. Naskah Dinas - Tugas",
                    "D.04. Dokumen - Pemberkasan",
                    "C.02. Naskah Dinas - Kotak Masuk",
                    "C.03. Naskah Dinas - Terkirim",
                    "C.04. Naskah Dinas - Draft,",
                    "C.05. Naskah Dinas - Kelola Label",
                    "D.01. Dokumen - Arsip Aktif",
                    "E.02. Pelaporan - Naskah Keluar",
                    "E.01. Pelaporan - Naskah Masuk",
                    "E.4 Pelaporan - Arsip",
                    "E.0 Pelaporan - Naskah Dinas"
                };
                for (int i = 0; i < auth.Count(); i++)
                {
                    var rolePublic = context.Roles.Where(a => a.Name == "Public").FirstOrDefault();
                    var permission = context.Permissions.Where(t => t.PermissionName == auth[i]).FirstOrDefault();
                    if (permission != null)
                    {
                        ApplicationRoleActionAuthorizations actionAuthorizations = new ApplicationRoleActionAuthorizations()
                        {
                            ApplicationRoleId = rolePublic.Id,
                            PermissionId = permission.Id,
                            forCreate = true,
                            forEdit = true,
                            forView = true,
                            forDelete = true,
                            forRestore = true
                        };
                        context.Add(actionAuthorizations);
                    }
                }
                context.SaveChanges();
            }
            if (!roleManager.RoleExistsAsync("Admin").Result)
            {
                var role = new ApplicationRole();
                role.CreatedDate = DateTime.Now;
                role.UpdatedDate = DateTime.Now;
                role.Name = "Admin";
                role.Id = "201b0a2d-30ce-424a-baed-efac1f306951";
                IdentityResult roleResult = roleManager.CreateAsync(role).Result;

                var user = new ApplicationUser();
                user.Id = "9b8302b5-a111-466e-83c8-2bd1e852e58a";
                user.PersonName = "Admin";
                user.Email = "admin@jasamarga.co.id";
                user.UserName = user.Email;
                user.IsActive = true;
                user.RoleId = role.Id;
                user.RoleName = role.Name;
                user.CreatedDate = DateTime.Now;
                user.UpdatedDate = DateTime.Now;
                string userPWD = "Aspapp@789";
                IdentityResult result = userManager.CreateAsync(user, userPWD).Result;
                IdentityResult roles = userManager.AddToRoleAsync(user, "Admin").Result;

                var userRoles = new UserRolesAdditional()
                {
                    Id = Guid.NewGuid().ToString(),
                    Type = "User Main Role",
                    TypeId = user.Id,
                    LoginUserPersonId = user.Id,
                    LoginUserUnitKerja = user.UnitKerja,
                    LoginUserUnitKerjaId = user.UnitKerjaId,
                    PejabatId = user.Id,
                    PejabatBODLEvel = user.BODLevel,
                    PejabatName = user.PersonName,
                    PejabatJabatanId = user.RecentPositionId,
                    PejabatJabatanName = user.PositionName,
                    PejabatRoleId = user.RoleId,
                    PejabatRoleName = user.RoleName,
                    SecretaryActive = false,
                    isActive = false,
                    DelegationEndDate = null,
                    DelegationStartDate = null,
                    CreatedDate = DateTime.Now
                };
                context.UserRolesAdditionals.Add(userRoles);
                context.SaveChanges();

                var auth = context.Permissions.ToList();
                foreach (var item in auth)
                {
                    var permission = context.Permissions.Where(t => t.PermissionName == item.PermissionName).FirstOrDefault();
                    var roleAdmin = context.Roles.Where(a => a.Name == "Admin").FirstOrDefault();

                    if (permission != null)
                    {
                        ApplicationRoleActionAuthorizations actionAuthorizations = new ApplicationRoleActionAuthorizations()
                        {
                            ApplicationRoleId = roleAdmin.Id,
                            PermissionId = permission.Id,
                            forCreate = true,
                            forEdit = true,
                            forView = true,
                            forDelete = true,
                            forRestore = true
                        };
                        context.Add(actionAuthorizations);
                    }
                    context.SaveChanges();
                }
            }

        }
    }
}
