﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Jasamarga.DA.UserManagement.Migrations
{
    public partial class update_model_userRoleadditional_ : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "UserRolesAdditionals",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TypeId",
                table: "UserRolesAdditionals",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Type",
                table: "UserRolesAdditionals");

            migrationBuilder.DropColumn(
                name: "TypeId",
                table: "UserRolesAdditionals");
        }
    }
}
