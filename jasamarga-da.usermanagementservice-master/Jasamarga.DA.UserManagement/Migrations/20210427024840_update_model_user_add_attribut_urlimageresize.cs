﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Jasamarga.DA.UserManagement.Migrations
{
    public partial class update_model_user_add_attribut_urlimageresize : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UrlImageResize",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UrlImageResize",
                table: "AspNetUsers");
        }
    }
}
