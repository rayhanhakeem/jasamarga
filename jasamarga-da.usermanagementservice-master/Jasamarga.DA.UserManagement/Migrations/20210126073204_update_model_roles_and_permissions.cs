﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Jasamarga.DA.UserManagement.Migrations
{
    public partial class update_model_roles_and_permissions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationRoleActionAuthorizations_ActionAuthorization_Act~",
                table: "ApplicationRoleActionAuthorizations");

            migrationBuilder.DropTable(
                name: "ActionAuthorization");

            migrationBuilder.DropIndex(
                name: "IX_ApplicationRoleActionAuthorizations_ActionAuthorizationId",
                table: "ApplicationRoleActionAuthorizations");

            migrationBuilder.AddColumn<string>(
                name: "PermissionId",
                table: "ApplicationRoleActionAuthorizations",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationRoleActionAuthorizations_PermissionId",
                table: "ApplicationRoleActionAuthorizations",
                column: "PermissionId");

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationRoleActionAuthorizations_Permissions_PermissionId",
                table: "ApplicationRoleActionAuthorizations",
                column: "PermissionId",
                principalTable: "Permissions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationRoleActionAuthorizations_Permissions_PermissionId",
                table: "ApplicationRoleActionAuthorizations");

            migrationBuilder.DropIndex(
                name: "IX_ApplicationRoleActionAuthorizations_PermissionId",
                table: "ApplicationRoleActionAuthorizations");

            migrationBuilder.DropColumn(
                name: "PermissionId",
                table: "ApplicationRoleActionAuthorizations");

            migrationBuilder.CreateTable(
                name: "ActionAuthorization",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    ActionName = table.Column<string>(type: "text", nullable: true),
                    CreatedBy = table.Column<string>(type: "text", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    IsDraft = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedBy = table.Column<string>(type: "text", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActionAuthorization", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationRoleActionAuthorizations_ActionAuthorizationId",
                table: "ApplicationRoleActionAuthorizations",
                column: "ActionAuthorizationId");

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationRoleActionAuthorizations_ActionAuthorization_Act~",
                table: "ApplicationRoleActionAuthorizations",
                column: "ActionAuthorizationId",
                principalTable: "ActionAuthorization",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
