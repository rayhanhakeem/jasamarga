﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Jasamarga.DA.UserManagement.Migrations
{
    public partial class update_model_user_add_parentUserId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ParentUserId",
                table: "UserRolesAdditionals",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ParentUserId",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ParentUserId",
                table: "UserRolesAdditionals");

            migrationBuilder.DropColumn(
                name: "ParentUserId",
                table: "AspNetUsers");
        }
    }
}
