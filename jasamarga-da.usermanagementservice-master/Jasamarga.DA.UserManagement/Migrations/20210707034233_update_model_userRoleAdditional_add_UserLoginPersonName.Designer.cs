﻿// <auto-generated />
using System;
using Jasamarga.DA.UserManagement.Databases;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Jasamarga.DA.UserManagement.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20210707034233_update_model_userRoleAdditional_add_UserLoginPersonName")]
    partial class update_model_userRoleAdditional_add_UserLoginPersonName
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn)
                .HasAnnotation("ProductVersion", "3.1.8")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationRole", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("text");

                    b.Property<string>("ConcurrencyStamp")
                        .HasColumnType("text");

                    b.Property<string>("CreatedBy")
                        .HasColumnType("text");

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<bool>("IsActive")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDraft")
                        .HasColumnType("boolean");

                    b.Property<string>("Name")
                        .HasColumnType("text");

                    b.Property<string>("NormalizedName")
                        .HasColumnType("text");

                    b.Property<string>("UpdatedBy")
                        .HasColumnType("text");

                    b.Property<DateTime?>("UpdatedDate")
                        .HasColumnType("timestamp without time zone");

                    b.HasKey("Id");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationRoleActionAuthorizations", b =>
                {
                    b.Property<string>("ApplicationRoleId")
                        .HasColumnType("character varying(128)")
                        .HasMaxLength(128);

                    b.Property<string>("PermissionId")
                        .HasColumnType("character varying(128)")
                        .HasMaxLength(128);

                    b.Property<bool>("forCreate")
                        .HasColumnType("boolean");

                    b.Property<bool>("forDelete")
                        .HasColumnType("boolean");

                    b.Property<bool>("forEdit")
                        .HasColumnType("boolean");

                    b.Property<bool>("forRestore")
                        .HasColumnType("boolean");

                    b.Property<bool>("forView")
                        .HasColumnType("boolean");

                    b.HasKey("ApplicationRoleId", "PermissionId");

                    b.HasIndex("PermissionId");

                    b.ToTable("ApplicationRoleActionAuthorizations");
                });

            modelBuilder.Entity("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationRoleClaim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("ClaimType")
                        .HasColumnType("text");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("text");

                    b.Property<string>("RoleId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("RoleClaims");
                });

            modelBuilder.Entity("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationUser", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("text");

                    b.Property<int>("AccessFailedCount")
                        .HasColumnType("integer");

                    b.Property<string>("BODLevel")
                        .HasColumnType("text");

                    b.Property<string>("Baris")
                        .HasColumnType("text");

                    b.Property<string>("BusinessArea")
                        .HasColumnType("text");

                    b.Property<short?>("CntSubOrd")
                        .HasColumnType("smallint");

                    b.Property<string>("CompanyCode")
                        .HasColumnType("text");

                    b.Property<string>("ConcurrencyStamp")
                        .HasColumnType("text");

                    b.Property<string>("CreatedBy")
                        .HasColumnType("text");

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("DataSource")
                        .HasColumnType("text");

                    b.Property<DateTime?>("DateOfBirth")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("Email")
                        .HasColumnType("text");

                    b.Property<bool>("EmailConfirmed")
                        .HasColumnType("boolean");

                    b.Property<string>("EmpStatus")
                        .HasColumnType("text");

                    b.Property<short?>("EmpType")
                        .HasColumnType("smallint");

                    b.Property<short?>("EmployeeGroup")
                        .HasColumnType("smallint");

                    b.Property<string>("EmployeeNumber")
                        .HasColumnType("text");

                    b.Property<string>("EmployeeSubgroup")
                        .HasColumnType("text");

                    b.Property<string>("Golongan")
                        .HasColumnType("text");

                    b.Property<string>("ImageFileName")
                        .HasColumnType("text");

                    b.Property<string>("ImageFileNameResize")
                        .HasColumnType("text");

                    b.Property<bool>("IsActive")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDraft")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsGroup")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsUnitKearsipan")
                        .HasColumnType("boolean");

                    b.Property<double?>("Isldap")
                        .HasColumnType("double precision");

                    b.Property<string>("JenisNpwp")
                        .HasColumnType("text");

                    b.Property<double?>("JmId")
                        .HasColumnType("double precision");

                    b.Property<double?>("JobId")
                        .HasColumnType("double precision");

                    b.Property<string>("JobName")
                        .HasColumnType("text");

                    b.Property<string>("JobType")
                        .HasColumnType("text");

                    b.Property<int>("Level")
                        .HasColumnType("integer");

                    b.Property<double?>("LocationId")
                        .HasColumnType("double precision");

                    b.Property<string>("LocationName")
                        .HasColumnType("text");

                    b.Property<bool>("LockoutEnabled")
                        .HasColumnType("boolean");

                    b.Property<DateTimeOffset?>("LockoutEnd")
                        .HasColumnType("timestamp with time zone");

                    b.Property<double?>("MainRole")
                        .HasColumnType("double precision");

                    b.Property<string>("MaritalStatus")
                        .HasColumnType("text");

                    b.Property<string>("NationalIdentifier")
                        .HasColumnType("text");

                    b.Property<string>("NomorSk")
                        .HasColumnType("text");

                    b.Property<string>("NormalizedEmail")
                        .HasColumnType("text");

                    b.Property<string>("NormalizedUserName")
                        .HasColumnType("text");

                    b.Property<string>("NppAbsen")
                        .HasColumnType("text");

                    b.Property<string>("Npwp")
                        .HasColumnType("text");

                    b.Property<double?>("OrgGrandParent")
                        .HasColumnType("double precision");

                    b.Property<string>("OrgGrandParentName")
                        .HasColumnType("text");

                    b.Property<double?>("OrgIdParent")
                        .HasColumnType("double precision");

                    b.Property<string>("OrgName")
                        .HasColumnType("text");

                    b.Property<string>("OrgParentName")
                        .HasColumnType("text");

                    b.Property<double?>("OrganizationId")
                        .HasColumnType("double precision");

                    b.Property<DateTime?>("OriginalDateOfHire")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("ParentUserId")
                        .HasColumnType("text");

                    b.Property<string>("Password")
                        .HasColumnType("text");

                    b.Property<string>("PasswordHash")
                        .HasColumnType("text");

                    b.Property<string>("PayScaleArea")
                        .HasColumnType("text");

                    b.Property<int?>("PayrollGroupId")
                        .HasColumnType("integer");

                    b.Property<string>("PersonName")
                        .HasColumnType("text");

                    b.Property<long?>("PersonNumber")
                        .HasColumnType("bigint");

                    b.Property<double?>("PersonNumberApprover")
                        .HasColumnType("double precision");

                    b.Property<short?>("PersonalArea")
                        .HasColumnType("smallint");

                    b.Property<string>("PersonalSubArea")
                        .HasColumnType("text");

                    b.Property<long?>("PersonnelNumberSap")
                        .HasColumnType("bigint");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("text");

                    b.Property<bool>("PhoneNumberConfirmed")
                        .HasColumnType("boolean");

                    b.Property<int?>("PositionIdAtasan")
                        .HasColumnType("integer");

                    b.Property<string>("PositionName")
                        .HasColumnType("text");

                    b.Property<string>("PositionNameAlias")
                        .HasColumnType("text");

                    b.Property<string>("PositionNameAtasan")
                        .HasColumnType("text");

                    b.Property<double?>("RecentPositionId")
                        .HasColumnType("double precision");

                    b.Property<string>("Religion")
                        .HasColumnType("text");

                    b.Property<string>("RoleId")
                        .HasColumnType("text");

                    b.Property<string>("RoleName")
                        .HasColumnType("text");

                    b.Property<string>("Ruang")
                        .HasColumnType("text");

                    b.Property<string>("SecurityStamp")
                        .HasColumnType("text");

                    b.Property<string>("Sex")
                        .HasColumnType("text");

                    b.Property<string>("StatusNpwp")
                        .HasColumnType("text");

                    b.Property<string>("StatusSap")
                        .HasColumnType("text");

                    b.Property<string>("StatusSat")
                        .HasColumnType("text");

                    b.Property<string>("TanggalEfektifSk")
                        .HasColumnType("text");

                    b.Property<string>("TanggalSk")
                        .HasColumnType("text");

                    b.Property<string>("TownOfBirth")
                        .HasColumnType("text");

                    b.Property<bool>("TwoFactorEnabled")
                        .HasColumnType("boolean");

                    b.Property<string>("UnitKerja")
                        .HasColumnType("text");

                    b.Property<long?>("UnitKerjaId")
                        .HasColumnType("bigint");

                    b.Property<string>("UpdatedBy")
                        .HasColumnType("text");

                    b.Property<DateTime?>("UpdatedDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("UrlImage")
                        .HasColumnType("text");

                    b.Property<string>("UrlImageResize")
                        .HasColumnType("text");

                    b.Property<string>("UserName")
                        .HasColumnType("text");

                    b.Property<DateTime?>("ValidFrom")
                        .HasColumnType("timestamp without time zone");

                    b.Property<DateTime?>("ValidTo")
                        .HasColumnType("timestamp without time zone");

                    b.HasKey("Id");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationUserClaim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("ClaimType")
                        .HasColumnType("text");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("text");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("UserClaims");
                });

            modelBuilder.Entity("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationUserLogin", b =>
                {
                    b.Property<string>("LoginProvider")
                        .HasColumnType("character varying(128)")
                        .HasMaxLength(128);

                    b.Property<string>("ProviderKey")
                        .HasColumnType("character varying(128)")
                        .HasMaxLength(128);

                    b.Property<string>("UserId")
                        .HasColumnType("character varying(128)")
                        .HasMaxLength(128);

                    b.Property<string>("ProviderDisplayName")
                        .HasColumnType("text");

                    b.HasKey("LoginProvider", "ProviderKey", "UserId");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationUserRole", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("text");

                    b.Property<string>("RoleId")
                        .HasColumnType("text");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationUserToken", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("text");

                    b.Property<string>("LoginProvider")
                        .HasColumnType("character varying(128)")
                        .HasMaxLength(128);

                    b.Property<string>("Name")
                        .HasColumnType("character varying(128)")
                        .HasMaxLength(128);

                    b.Property<string>("Value")
                        .HasColumnType("text");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("Jasamarga.DA.UserManagement.Databases.Models.Permission", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("character varying(128)")
                        .HasMaxLength(128);

                    b.Property<string>("CreatedBy")
                        .HasColumnType("text");

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDraft")
                        .HasColumnType("boolean");

                    b.Property<string>("PermissionName")
                        .HasColumnType("text");

                    b.Property<string>("UpdatedBy")
                        .HasColumnType("text");

                    b.Property<DateTime?>("UpdatedDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("Url")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("Permissions");
                });

            modelBuilder.Entity("Jasamarga.DA.UserManagement.Databases.Models.UserDevice", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("character varying(128)")
                        .HasMaxLength(128);

                    b.Property<string>("CreatedBy")
                        .HasColumnType("text");

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("DeviceId")
                        .HasColumnType("text");

                    b.Property<string>("DeviceType")
                        .HasColumnType("text");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDraft")
                        .HasColumnType("boolean");

                    b.Property<string>("UpdatedBy")
                        .HasColumnType("text");

                    b.Property<DateTime?>("UpdatedDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("UserDevices");
                });

            modelBuilder.Entity("Jasamarga.DA.UserManagement.Databases.Models.UserRolesAdditional", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("character varying(128)")
                        .HasMaxLength(128);

                    b.Property<string>("CreatedBy")
                        .HasColumnType("text");

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<DateTime?>("DelegationEndDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<DateTime?>("DelegationStartDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDraft")
                        .HasColumnType("boolean");

                    b.Property<string>("LoginUserPersonId")
                        .HasColumnType("text");

                    b.Property<string>("LoginUserPersonName")
                        .HasColumnType("text");

                    b.Property<string>("LoginUserUnitKerja")
                        .HasColumnType("text");

                    b.Property<double?>("LoginUserUnitKerjaId")
                        .HasColumnType("double precision");

                    b.Property<int?>("Order")
                        .HasColumnType("integer");

                    b.Property<string>("ParentUserId")
                        .HasColumnType("text");

                    b.Property<string>("PejabatBODLEvel")
                        .HasColumnType("text");

                    b.Property<string>("PejabatId")
                        .HasColumnType("text");

                    b.Property<double?>("PejabatJabatanId")
                        .HasColumnType("double precision");

                    b.Property<string>("PejabatJabatanName")
                        .HasColumnType("text");

                    b.Property<string>("PejabatName")
                        .HasColumnType("text");

                    b.Property<string>("PejabatRoleId")
                        .HasColumnType("text");

                    b.Property<string>("PejabatRoleName")
                        .HasColumnType("text");

                    b.Property<bool>("SecretaryActive")
                        .HasColumnType("boolean");

                    b.Property<string>("Type")
                        .HasColumnType("text");

                    b.Property<string>("TypeId")
                        .HasColumnType("text");

                    b.Property<string>("UpdatedBy")
                        .HasColumnType("text");

                    b.Property<DateTime?>("UpdatedDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<bool>("isActive")
                        .HasColumnType("boolean");

                    b.HasKey("Id");

                    b.ToTable("UserRolesAdditionals");
                });

            modelBuilder.Entity("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationRoleActionAuthorizations", b =>
                {
                    b.HasOne("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationRole", "ApplicationRole")
                        .WithMany("ApplicationRoleActionAuthorizations")
                        .HasForeignKey("ApplicationRoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Jasamarga.DA.UserManagement.Databases.Models.Permission", "Permission")
                        .WithMany("ApplicationRoleActionAuthorizations")
                        .HasForeignKey("PermissionId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationRoleClaim", b =>
                {
                    b.HasOne("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationRole", "Role")
                        .WithMany("RoleClaims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationUser", b =>
                {
                    b.OwnsMany("Jasamarga.DA.UserManagement.Databases.Identity.RefreshToken", "RefreshTokens", b1 =>
                        {
                            b1.Property<string>("Id")
                                .HasColumnType("text");

                            b1.Property<string>("ApplicationUserId")
                                .IsRequired()
                                .HasColumnType("text");

                            b1.Property<string>("CreatedBy")
                                .HasColumnType("text");

                            b1.Property<string>("CreatedByIp")
                                .HasColumnType("text");

                            b1.Property<DateTime>("CreatedDate")
                                .HasColumnType("timestamp without time zone");

                            b1.Property<DateTime>("Expires")
                                .HasColumnType("timestamp without time zone");

                            b1.Property<bool>("IsDeleted")
                                .HasColumnType("boolean");

                            b1.Property<bool>("IsDraft")
                                .HasColumnType("boolean");

                            b1.Property<string>("ReplacedByToken")
                                .HasColumnType("text");

                            b1.Property<DateTime?>("Revoked")
                                .HasColumnType("timestamp without time zone");

                            b1.Property<string>("RevokedByIp")
                                .HasColumnType("text");

                            b1.Property<string>("Token")
                                .HasColumnType("text");

                            b1.Property<string>("UpdatedBy")
                                .HasColumnType("text");

                            b1.Property<DateTime?>("UpdatedDate")
                                .HasColumnType("timestamp without time zone");

                            b1.HasKey("Id");

                            b1.HasIndex("ApplicationUserId");

                            b1.ToTable("RefreshTokens");

                            b1.WithOwner()
                                .HasForeignKey("ApplicationUserId");
                        });
                });

            modelBuilder.Entity("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationUserClaim", b =>
                {
                    b.HasOne("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationUser", "User")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationUserLogin", b =>
                {
                    b.HasOne("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationUser", "User")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationUserRole", b =>
                {
                    b.HasOne("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationRole", "Role")
                        .WithMany("UserRoles")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationUser", "User")
                        .WithMany("UserRoles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationUserToken", b =>
                {
                    b.HasOne("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationUser", "User")
                        .WithMany("Tokens")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Jasamarga.DA.UserManagement.Databases.Models.UserDevice", b =>
                {
                    b.HasOne("Jasamarga.DA.UserManagement.Databases.Identity.ApplicationUser", "User")
                        .WithMany("UserDevices")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
