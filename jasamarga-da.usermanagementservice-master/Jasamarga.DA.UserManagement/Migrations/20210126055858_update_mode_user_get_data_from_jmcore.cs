﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Jasamarga.DA.UserManagement.Migrations
{
    public partial class update_mode_user_get_data_from_jmcore : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FullName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PositionId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "WorkUnitId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "WorkUnitName",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<string>(
                name: "Baris",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BusinessArea",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "CntSubOrd",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyCode",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DataSource",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateOfBirth",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EmpStatus",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "EmpType",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "EmployeeGroup",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EmployeeNumber",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EmployeeSubgroup",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Golongan",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Isldap",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "JenisNpwp",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "JobId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "JobName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "JobType",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "LocationId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LocationName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "MainRole",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MaritalStatus",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NationalIdentifier",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NomorSk",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NppAbsen",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Npwp",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "OrgGrandParent",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OrgGrandParentName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "OrgIdParent",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OrgName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OrgParentName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "OrganizationId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "OriginalDateOfHire",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PayScaleArea",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PayrollGroupId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PersonName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "PersonNumber",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "PersonNumberApprover",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "PersonalArea",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PersonalSubArea",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "PersonnelNumberSap",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PositionIdAtasan",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PositionNameAlias",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PositionNameAtasan",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "RecentPositionId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Religion",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Ruang",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Sex",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StatusNpwp",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StatusSap",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StatusSat",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TanggalEfektifSk",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TanggalSk",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TownOfBirth",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UnitKerja",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UnitKerjaId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UrlImage",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ValidFrom",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ValidTo",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Baris",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "BusinessArea",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "CntSubOrd",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "CompanyCode",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "DataSource",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "DateOfBirth",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "EmpStatus",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "EmpType",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "EmployeeGroup",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "EmployeeNumber",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "EmployeeSubgroup",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Golongan",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Isldap",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "JenisNpwp",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "JobId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "JobName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "JobType",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "LocationId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "LocationName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "MainRole",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "MaritalStatus",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "NationalIdentifier",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "NomorSk",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "NppAbsen",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Npwp",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OrgGrandParent",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OrgGrandParentName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OrgIdParent",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OrgName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OrgParentName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OrganizationId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OriginalDateOfHire",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Password",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PayScaleArea",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PayrollGroupId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PersonName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PersonNumber",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PersonNumberApprover",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PersonalArea",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PersonalSubArea",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PersonnelNumberSap",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PositionIdAtasan",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PositionNameAlias",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PositionNameAtasan",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "RecentPositionId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Religion",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Ruang",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Sex",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "StatusNpwp",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "StatusSap",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "StatusSat",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "TanggalEfektifSk",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "TanggalSk",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "TownOfBirth",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "UnitKerja",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "UnitKerjaId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "UrlImage",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ValidFrom",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ValidTo",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<string>(
                name: "FullName",
                table: "AspNetUsers",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PositionId",
                table: "AspNetUsers",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "AspNetUsers",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WorkUnitId",
                table: "AspNetUsers",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WorkUnitName",
                table: "AspNetUsers",
                type: "text",
                nullable: true);
        }
    }
}
