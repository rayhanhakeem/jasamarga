﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Jasamarga.DA.UserManagement.Migrations
{
    public partial class add_model_userroleAdditional_init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserRolesAdditionals",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 128, nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<string>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsDraft = table.Column<bool>(nullable: false),
                    LoginUserPersonId = table.Column<string>(nullable: true),
                    LoginUserUnitKerja = table.Column<string>(nullable: true),
                    LoginUserUnitKerjaId = table.Column<double>(nullable: true),
                    PejabatRoleId = table.Column<string>(nullable: true),
                    PejabatRoleName = table.Column<string>(nullable: true),
                    PejabatId = table.Column<string>(nullable: true),
                    PejabatName = table.Column<string>(nullable: true),
                    PejabatJabatanId = table.Column<double>(nullable: true),
                    PejabatJabatanName = table.Column<string>(nullable: true),
                    PejabatBODLEvel = table.Column<string>(nullable: true),
                    isActive = table.Column<bool>(nullable: false),
                    DelegationStartDate = table.Column<DateTime>(nullable: true),
                    DelegationEndDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRolesAdditionals", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserRolesAdditionals");
        }
    }
}
