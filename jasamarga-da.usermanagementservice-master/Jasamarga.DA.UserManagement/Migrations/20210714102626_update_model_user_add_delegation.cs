﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Jasamarga.DA.UserManagement.Migrations
{
    public partial class update_model_user_add_delegation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DelegationEnd",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DelegationStart",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelegation",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DelegationEnd",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "DelegationStart",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "IsDelegation",
                table: "AspNetUsers");
        }
    }
}
