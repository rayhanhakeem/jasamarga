﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Jasamarga.DA.UserManagement.Migrations
{
    public partial class update_model_role_action_authorization_change_name_column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationRoleActionAuthorizations_Permissions_PermissionId",
                table: "ApplicationRoleActionAuthorizations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ApplicationRoleActionAuthorizations",
                table: "ApplicationRoleActionAuthorizations");

            migrationBuilder.DropColumn(
                name: "ActionAuthorizationId",
                table: "ApplicationRoleActionAuthorizations");

            migrationBuilder.AlterColumn<string>(
                name: "PermissionId",
                table: "ApplicationRoleActionAuthorizations",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character varying(128)",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ApplicationRoleActionAuthorizations",
                table: "ApplicationRoleActionAuthorizations",
                columns: new[] { "ApplicationRoleId", "PermissionId" });

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationRoleActionAuthorizations_Permissions_PermissionId",
                table: "ApplicationRoleActionAuthorizations",
                column: "PermissionId",
                principalTable: "Permissions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationRoleActionAuthorizations_Permissions_PermissionId",
                table: "ApplicationRoleActionAuthorizations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ApplicationRoleActionAuthorizations",
                table: "ApplicationRoleActionAuthorizations");

            migrationBuilder.AlterColumn<string>(
                name: "PermissionId",
                table: "ApplicationRoleActionAuthorizations",
                type: "character varying(128)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 128);

            migrationBuilder.AddColumn<string>(
                name: "ActionAuthorizationId",
                table: "ApplicationRoleActionAuthorizations",
                type: "character varying(128)",
                maxLength: 128,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ApplicationRoleActionAuthorizations",
                table: "ApplicationRoleActionAuthorizations",
                columns: new[] { "ApplicationRoleId", "ActionAuthorizationId" });

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationRoleActionAuthorizations_Permissions_PermissionId",
                table: "ApplicationRoleActionAuthorizations",
                column: "PermissionId",
                principalTable: "Permissions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
